//
//  fs_mutation.hpp
//  flowsculptga
//
//  Created by Dan Stoecklein on 2/1/19.
//

#ifndef fs_mutation_hpp
#define fs_mutation_hpp

#include <math.h>

#include "ga.h"
#include "fs_chromosome.h"

double PowerMutation(const double& x, const double& x_min, const double& x_max);
float PowerMutation(const float& x, const float& x_min, const float& x_max);
int PowerMutation(const int x, const int x_min, const int x_max);

#endif /* fs_mutation_hpp */
