//
//  fs_utilities.h
//  flowsculptga
//
//  Created by Dan Stoecklein on 1/14/19.
//

#ifndef fs_utilities_h
#define fs_utilities_h

#include <numeric>
#include <vector>
#include <sstream>
#include <iomanip>
#include <assert.h>
#include <unordered_map>
#include <algorithm>

#include "ga.h"

//////////// Helper Structs / Classes ////////////

/// Typedef for runtime settings
typedef std::unordered_map<std::string, std::string> FsSettings;

/// Typedefs for a advection data.
typedef std::vector<int> IndexMap;
typedef std::vector<float> AdvectionMap;

/// Typedef for a channel cross section.
typedef std::vector<double> CrossSection;

/// Typedef for an inlet design
typedef std::pair<std::vector<double>, std::vector<int>> InletDesign;

typedef GA::Configuration FsConfig;

/// Struct to hold information on material for analysis and sorting
struct MaterialInfo {
  int material;
  int count;
  
  bool operator<( const MaterialInfo& test ) const
  {return count > test.count;}
};

typedef struct {
  std::string result_dir;
  std::string result_suffix;
  std::string gen_file_prefix;
  std::string fitness_file_prefix;
  std::string map_folder;
  std::string target_file;
} FsPathData;

/// This is a basic chromosome element. It holds the
/// pillar's data and allows for optional index index_override.
class RealPillar{
public:
  /// y-position in the channel.
  float yw;
  
  /// diameter of the pillar.
  float dw;
  
  /// height of the pillar.
  float hw;
  
  /// Index index_override.
  int idx;
  
  /// flag to index_override.
  bool index_override;
  
  RealPillar() : yw(0.0f), dw(0.0f), hw(0.0f), idx(0), index_override(false) { }
  RealPillar(float _yw, float _dw, float _hw) : yw(_yw), dw(_dw), hw(_hw), idx(0), index_override(false) { }
  RealPillar(int _idx) : idx(_idx), index_override(true) {
    /// Convert index to pillar geometry
    float idx_f = static_cast<float>(idx + 1);
    yw = (idx_f - (ceil(idx_f / 8.0) - 1) * 8.0) / 8.0 - 0.5;
    if (idx <= 32)
    {
      dw = 0.25 + ceil(idx_f / 8.0) / 8.0;
    }
    else
    {
      dw = 0.25 + ceil((idx_f - 32.0) / 8.0) / 8.0;
    }
    hw = idx_f <= 32 ? 1.0 : 0.5;
  }
  
  /// Method used to convert yw-dw representation to our discretized pillar space.
  int Interpret() const {
    if (index_override)
    {
      return idx;
    }
    /// Convert pillar geometry to index
    return (round((dw - 0.25f) / 0.125f) - 1) * 8.0f
    + (round((yw + 0.50f) / 0.125f) - 1)
    + (round(1.0f - hw) * 32.0f);
  }
  
  bool IsValid() {
    // Full pillar, more than 3/4 of channel blocked
    if (hw >= 0.5 && dw > 0.75) {
      return false;
    }
    return true;
  }
};

/// The inlet generator is responsible for creating the inlets for use in the
/// forward model.
///
/// N.B. Historically this used to be a class that caches inlet designs when the
///      number of possible inlets was discrete. Since moving to inlets with
///      real valued components, a cache is no longer useful.
class InletGenerator {
public:
  InletGenerator() { }
  
  CrossSection GetInlet(int imgw, int imgh, const InletDesign& inseq);
};

//////////// Runtime estimate functions ////////////
std::chrono::time_point<std::chrono::high_resolution_clock> FSTimer();
int FSRuntime_ms(std::chrono::time_point<std::chrono::high_resolution_clock> t_1, std::chrono::time_point<std::chrono::high_resolution_clock> t_2);

/// Use linear regression to estimate runtime
void ComputeSlopeIntercept(const std::vector<double>& x, const std::vector<double>& y, double& slope, double& intercept);

/// Convert seconds to minutes/hours/days
std::string RuntimeMod(double time);

template<class T>
bool IsMember(std::vector<T> v, T x) {
  if(std::find(v.begin(), v.end(), x) != v.end()) {
    return true;
  } else {
    return false;
  }
}
#endif /* fs_utilities_h */
