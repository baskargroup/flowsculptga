//
//  fs_fitness.h
//  flowsculptga
//
//  Created by Dan Stoecklein on 1/23/19.
//

#ifndef fs_fitness_h
#define fs_fitness_h

/// Prototype for fitness function.
typedef double(*FitnessFunction)(GA::Configuration&, void *, CrossSection&);

/// Supported fitness functions
#define FITNESS_FUNCTION(name) \
double name(GA::Configuration& config, void * thread_data, CrossSection& outlet)

/// ------------------------------- V Look here V -------------------------------
/// This is where fitness functions are declared.
FITNESS_FUNCTION(PMRFitness);
FITNESS_FUNCTION(OCV_L2Norm);
FITNESS_FUNCTION(Corr2Fitness);
FITNESS_FUNCTION(Corr2MaskFitness);
FITNESS_FUNCTION(FFTCorr2Fitness);
FITNESS_FUNCTION(EMDRcorrocvFitness);

/// TODO: New fitness function should be declared here and must have the call
///       pattern as above. They should be implemented in fitness.cpp.

/// -----------------------------------------------------------------------------

typedef struct {
  char * name;
  FitnessFunction func;
} FitnessFunctionEntry;

#define REGISTER_FITNESS_FUNCTION(name) \
{ (char *)"" # name, name }

extern FitnessFunctionEntry FitnessFunctionList[];

#endif /* fs_fitness_h */
