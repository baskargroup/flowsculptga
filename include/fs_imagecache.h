#ifndef fs_imagecache_h
#define fs_imagecache_h

#include "ga.h"
#include "fs_utilities.h"
#include "fs_fft.h"
#include "ocv_utilities.h"

/// Stores the input image (converted to species). This is resized
/// as necessary for supporting different resolutions and aspect ratios.
typedef struct {
  int imgw;
  int imgh;
  int num_material;
  bool num_material_override;
  bool symmetric;
  std::vector<int> material_seen;
  std::vector<MaterialInfo> material_info;
  std::vector<double> data;
} InputImageData;

/// ImageCache handles the
/// initial loading of the target image. It will store the
/// original image at full res. When a chromosome is evaluated,
/// it will ask for the target image at some resolution and
/// with or without FFT applied to it. This class provides
/// a mechanism to produce the target image for any desired
/// resolution and with/out FFT applied. This class will cache
/// generated targets in memory for fast access.
class ImageCache {
private:
  ///
  GA::Configuration * config_;
  
  /// Original image loaded in memory
  InputImageData image_data_;
  
  /// Whether or not the GA was configured to use half pillars
  bool half_pillars_;
  
  /// Lock mechanism for access to the cache
  std::mutex cache_lock_;
  
  /// Cache for the various resolutions. Each entry in
  /// the map is a (imgw, CrossSection) pair.
  std::map<std::pair<int, int>, CrossSection> target_cache_;
  
  /// Same as above but these targets have FFT applied
  /// to them.
  std::map<std::pair<int, int>, CrossSection> target_fft_cache_;
  
  /// Same as above but these targets have an OpenCV format
  std::map<std::pair<int, int>, CrossSectionMAT> target_ocv_cache_;
  
  /// Internal method to generate an FFT target
  CrossSection GenerateTargetFFT(int imgw, int imgh);
  
  /// Internal method to generate an OpenCV target
  CrossSectionMAT GenerateTargetOCV(int imgw, int imgh);
  
public:
  ImageCache() {};
  int SetImageCache(std::string image_file, bool _half_pillars,
                    GA::Configuration * config);
  
  int NumMaterial() const {
    return image_data_.num_material;
  }
  
  const CrossSection& GetTarget(int imgw, int imgh);
  const CrossSection& GetTargetFFT(int imgw, int imgh);
  const CrossSectionMAT& GetTargetOCV(int imgw, int imgh);
};

/// Loads the target image from the filesystem.
int LoadTarget(std::string filename, InputImageData&);
CrossSection Target2CrossSection(InputImageData& imgData, GA::Configuration * config, int imgw, int imgh);

#endif /* fs_imagecache_h */
