//
//  fs_setup.h
//  flowsculptga
//
//  Created by Dan Stoecklein on 4/10/19.
//

#ifndef fs_setup_h
#define fs_setup_h

#include "fs_utilities.h"
#include "fs_imagecache.h"


/// Setup functions
void FsTeardown(FsConfig& config);

/// Reads the config file and returns a GA::Configuration filled out with relevant settings
/// as well as some book-keeping information
int GetBaseConfiguration(
                         const char *filename,
                         FsPathData &path_data,
                         FsConfig &config,
                         InletDesign &inlet_design,
                         ImageCache &image_cache,
                         std::vector<RealPillar> &seed_pillar_seq
                         );

/// Not sure if this needs to be exposed anymore.
void SetGASettings(FsConfig& config);

#endif /* fs_setup_h */
