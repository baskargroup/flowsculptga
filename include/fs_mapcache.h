#ifndef fs_mapcache_h
#define fs_mapcache_h

#include <map>
#include <iostream>
#include <iomanip>
#include <vector>
#include <math.h>

#include "zstr.hpp"

#include "fs_utilities.h"
#include "fs_interpolate.h"
#include "fs_image.h"

/// The MapCache handles loading of map bundles. Callers ask
/// for a map (by index) with a certain Reynold's number and
/// resolution. This class will take of loading bundles from
/// storage into memory where applicable and return constant
/// reference to the desired map. This class is thread safe.
class MapCache {
private:
  std::string map_folder_;
  int re_num_;
  int load_imgw_;
  std::map<std::pair<int, int>, std::vector<IndexMap> > map_table_;
  std::mutex cache_lock_;
  bool half_pillars_;
  bool base_80_;
  
  void LoadBase80Maps(zstr::ifstream* ifs, const int load_imgh, const int imgw, const int imgh, const double ar, const std::vector<int>& diffusion_zone, std::vector<IndexMap>& maps);
  void LoadMaps(int imgw, int imgh);
  
public:
//  MapCache(std::string map_folder, bool half_pillars, int re_num = 20) : map_folder_(map_folder), re_num_(re_num), map_table_(), cache_lock_() { }
  MapCache() {};
  void SetMapCache(std::string map_folder, bool half_pillars, bool base_80 = true, int re_num = 20, int load_imgw = 800) {
    map_folder_ = map_folder;
    re_num_ = re_num;
    half_pillars_ = half_pillars;
    base_80_ = base_80;
    load_imgw_ = load_imgw;
  }
  
  const IndexMap& GetMap(int imgw, int imgh, int idx);
};

/// Loads the maps for the given imgw to the given maps container from the filesystem.
void LoadMaps(std::string mapfolder, int imgw, int imgh, int re_num, bool half_pillars, std::vector<IndexMap>& maps);

#endif /* fs_mapcache_h */
