//
//  fs_image.h
//  flowsculptga
//
//  Created by Dan Stoecklein on 1/24/19.
//

#ifndef fs_image_h
#define fs_image_h

#include "fs_utilities.h"

void SaveImage(std::string filename, CrossSection cs, int imgw,
               int imgh, bool half_pillars);

#endif /* fs_image_h */
