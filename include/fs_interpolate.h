#pragma once

#include <stdio.h>
#include <limits>
#include <vector>

struct Q_Boundary {
    const double xmin;
    const double xmax;
    const double ymin;
    const double ymax;
};

/**
 * Function to generate an array of linearly changing values
 * @param first value at start of array
 * @param last value at end of array
 * @param len size of array
 * @return linear array
 */
std::vector<double> linspace(double first, double last, int len);

/**
 * Creates a uniform X, Y grid within an nx_coarse*ny_coarse space of resolution
 * nx_fine * ny_fine
 * @param[out] X x values of fine resolution grid
 * @param[out] Y y values of fine resolution grid
 * @param nx_coarse size of original grid in x direction
 * @param ny_coarse size of original grid in y direction
 * @param nx_fine size of fine grid in x direction
 * @param ny_fine size of fine grid in y direction
 */
void MeshGrid(std::vector<double>& X, std::vector<double>& Y,
              const int nx_coarse, const int ny_coarse,
              const int nx_fine, const int ny_fine);

/**
 * Linearly interpolates a 1D vector
 * @param coarse_data Original data for interpolation
 * @param nx_coarse size of original grid in x direction
 * @param ny_coarse size of original grid in y direction
 * @param nx_fine size of fine grid in x direction
 * @param ny_fine size of fine grid in y direction
 * @param q_bound quiver dimensions
 * @return interpolated 1D vector
 */
std::vector<double> Interp2DFunc(std::vector<double> coarse_data,
                                 const int nx_coarse, const int ny_coarse,
                                 const int nx_fine, const int ny_fine,
                                 Q_Boundary q_bound);

std::vector<float> Interp2DFuncF(std::vector<double> coarse_data,
                                 const int nx_coarse, const int ny_coarse,
                                 const int nx_fine, const int ny_fine,
                                 Q_Boundary q_bound);
/**
 * Linearly interpolates a 1D vector
 * @param coarse_data Original data for interpolation
 * @param nx_coarse size of original grid in x direction
 * @param ny_coarse size of original grid in y direction
 * @param nx_fine size of fine grid in x direction
 * @param ny_fine size of fine grid in y direction
 * @param q_bound quiver dimensions
 * @return interpolated 1D vector
 */
void interp_func(std::vector<double> &B, std::vector<double> A,
                 std::vector<double> X, std::vector<double> Y,
                 const int num_points, const int w, const int h,
                 const int col, const double oobv);

void interp_func(std::vector<float> &B, std::vector<double> A,
                 std::vector<double> X, std::vector<double> Y,
                 const int num_points, const int w, const int h,
                 const int col, const double oobv);

std::vector<std::vector<double>>
QuiverInterpolate(std::vector<std::vector<double>> vData,
                  const int nx_coarse, const int ny_coarse,
                  const int nx_fine, const int ny_fine, Q_Boundary q_bound);

std::vector<float> AdvectionInterpolate(std::vector<float> adata,
                     const int coarse_nx, const int coarse_ny,
                     const int fine_nx, const int fine_ny,
                     Q_Boundary q_bound);

template <class T>
std::vector<T> InterpolateNearestNeighbor(const std::vector<T> data, int imgw_old, int imgh_old, int imgw_new, int imgh_new) {
  std::vector<T> result(imgw_new * imgh_new);
  int index = 0;
  for (int row = 0; row < imgh_new; row++) {
    for (int col = 0; col < imgw_new; col++) {
      int orig_row = (row * imgh_old) / imgh_new;
      int orig_col = (col * imgw_old) / imgw_new;
      int orig_index = orig_row * imgw_old + orig_col;
      result[index++] = data[orig_index];
    }
  }
  return result;
}
