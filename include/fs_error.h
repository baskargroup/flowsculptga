//
//  fs_error.hpp
//  flowsculptga
//
//  Created by Dan Stoecklein on 4/10/19.
//

#ifndef fs_error_hpp
#define fs_error_hpp

#include <iostream>
#include <vector>
#include <string>
#include <sstream>

void FSCHKERR(int fs_error);

#endif /* fs_error_hpp */
