
#ifndef __flowsculptga__
#define __flowsculptga__

#include <mutex>
#include <iostream>
#include <vector>
#include <time.h>
#include <chrono>
#include <cstdint>
#include <cstring>
#include <string>
#include <sstream>
#include <unordered_map>
#include <exception>
#include <memory>
#include <math.h>
#include <map>
#include <algorithm>

#include "ga.h"
#include "fs_setup.h"
#include "fs_chromosome.h"
#include "fs_crossover.h"
#include "fs_mutation.h"
#include "fs_utilities.h"
#include "fs_mapcache.h"

class FlowSculptGa {
private:
  FsConfig global_config_;
  InletDesign inlet_design_;
  FsPathData path_data_;
  InletGenerator inlet_generator_;
  
  GA::Evaluation::CreateNewThreadData create_thread_data_ = [](int i) -> void* {return reinterpret_cast<void *> ((unsigned long long)i);
    
  };
  
  MapCache map_cache_;
  ImageCache image_cache_;
  FFTPlanManager plan_manager_;
  
  /// Runtime variables
  int num_pillars_;
  int current_rep_;
  
  /// GA settings
  int min_pillars_;
  int max_pillars_;
  int reps_;
  int popsize_;
  int ga_counter_;
  int num_GAs_;
  
  FsChromosome* global_top_;
  FsChromosome* current_top_;
  
  std::vector<FsChromosome*> per_pillar_top_;
  std::vector<std::vector<double>> top_scores_;
  std::vector<std::vector<int>> num_gen_;
  std::vector<double> ga_runtime_;
  std::vector<double> ga_pillars_;

  // Seed pillar sequence
  std::vector<RealPillar> seed_pillar_seq_;
  
public:
  void PrepareGa(const char*);
  void CheckTop(FsChromosome*, int, bool&);
  void UpdateProgress(GA::Solver*, bool&);
  void PrintUpdate(GA::Solver*, int, int);
  void PrintResults();
  void SaveGenerationData(std::vector<std::vector<double>>, int, int);
  void SaveRepetitionData(GA::Solver *s, FsChromosome *current_top);
  void GaSweep();
  std::string ComputeRemainingTime(int, int);
};

//////////// Additional Helper Classes ////////////

//////////// Required functions for GA ////////////

//////////// Other function prototypes ////////////

/// Forward model for computing resulting outlet from a pillar sequence
CrossSection ForwardModel(GA::Configuration * config, const InletDesign& inlet, const std::vector<RealPillar>& seq);

void SaveImage(std::string filename, CrossSection cs, int imgw, int imgh,
               bool half_pillars);

/// Gets called after each GA step. 
void StepCallback(GA::Configuration& config);
void MultiresSwitch(GA::Configuration& config, int new_imgw, int new_imgh);

/// Utils
void SplitLineInt(const std::string& line, std::vector<int>& list, char sep = ' ');
void SplitLineDouble(const std::string& line, std::vector<double>& list, char sep = ' ');


#endif
