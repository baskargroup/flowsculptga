//
//  fs_crossover.h
//  flowsculptga
//
//  Created by Dan Stoecklein on 2/1/19.
//

#ifndef fs_crossover_h
#define fs_crossover_h

#include "ga.h"
#include "fs_chromosome.h"
#include "fs_utilities.h"

/// Required crossover method.
void FsChromosomeCrossover(const GA::Configuration& config, GA::Chromosome * child,
                           const GA::Chromosome * parent1, const GA::Chromosome * parent2);

float LaplaceOperator(const float& x1, const float& x2, const float&, const float&);

/// Overload for integer values
int LaplaceOperator(const int& x1, const int& x2, const int&, const int&);

/// Overload for double values
double LaplaceOperator(const double& x1, const double& x2, const double&, const double&);

float LaplaceOperator(const float& x1, const float& x2);

/// Overload for integer values
int LaplaceOperator(const int& x1, const int& x2);

/// Overload for double values
double LaplaceOperator(const double& x1, const double& x2);

void CrossoverLaplace(FsChromosome * pc1, FsChromosome * pc2, GA::Chromosome * child,
                      InletDesign& inlet, std::vector<RealPillar>& seq,
                      const GA::Configuration& config);

void CrossoverUniform(FsChromosome * pc1, FsChromosome * pc2, GA::Chromosome * child,
                      InletDesign& inlet, std::vector<RealPillar>& seq,
                      const GA::Configuration& config);

#endif /* fs_crossover_h */
