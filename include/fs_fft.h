#ifndef fs_fft_h
#define fs_fft_h

#include <fftw3.h>
#include "fs_utilities.h"

/// Helper struct to store allocated data for an FFTW plan
struct FsFftwPlan {
  int imgw;
  int imgh;
  double * in;
  fftw_complex * out;
  fftw_plan p;
};

/// This class provides a way for easy application of FFTW plans.
/// It creates and maintains a plan for each evaluation thread
/// and allows that thread to easily run its plan through calling
/// RunPlan with its ID.
///
/// The plan manager will ensure that the plan for each thread
/// has a resolution that matches the current evaluation. It will
/// reallocate the plan when the resolution changes.
class FFTPlanManager {
private:
  std::vector<FsFftwPlan> plan_list_;
  std::mutex fftw_mutex_;
  
public:
  FFTPlanManager() : fftw_mutex_() {};
  void SetFFTPlanManager(int num_threads);
  
  CrossSection RunPlan(int guid, int imgw, int imgh, CrossSection& outlet);
  
  ~FFTPlanManager();
};

/// Used for calculating the square magnitude of a fftw complex number
inline double mag_squared(fftw_complex c) {
  return c[0] * c[0] + c[1] * c[1];
}

#endif /* fs_fft_h */
