//
//  fs_forward_model.h
//  flowsculptga
//
//  Created by Dan Stoecklein on 1/25/19.
//

#ifndef fs_forward_model_h
#define fs_forward_model_h

#include "ga.h"
#include "fs_utilities.h"
#include "fs_flowsculptga.h"

CrossSection ForwardModel(
	GA::Configuration * config,
	const InletDesign& inlet,
	const std::vector<RealPillar>& seq,
	int imgh,
	int imgw);

#endif /* fs_forward_model_h */
