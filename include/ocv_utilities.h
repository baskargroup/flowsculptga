//
//  ocv_utilities.h
//  flowsculptga
//
//  Created by Dan Stoecklein on 5/8/19.
//

#ifndef ocv_utilities_h
#define ocv_utilities_h

#include "opencv2/core/core_c.h"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#undef EMPTY

#include "fs_utilities.h"

/// Typedef for a channel cross section.
typedef cv::Mat CrossSectionMAT;

CrossSectionMAT ConvertToMAT(const CrossSection& outlet, const int& imgw, const int& imgh);

#endif /* ocv_utilities_h */
