#ifndef fs_chromosome_h
#define fs_chromosome_h

#include "fs_utilities.h"
#include "fs_mapcache.h"
#include "fs_image.h"

#define E 1E-6

#define DW_MIN 0.3125
#define DW_MAX 1.0625

#define DW_MIN_FP 0.3125
#define DW_MAX_FP 0.8125

#define YW_MIN 0.4375
#define YW_MAX 0.5625

#define HW_MIN 0.0
#define HW_MAX 1.0

#define HW_MIN_FP 0.5
#define HW_MAX_FP 1.0

//////////// The Chromosome Representation ////////////

/// This is the chromosome representation for the GA.
/// It provides all the necessary methods needed for
/// the GA.
class FsChromosome : public GA::Chromosome {
private:
  /// Inlet sequence for this chromosome.
  InletDesign inlet_;
  
  /// Pillar sequence for this chromosome.
  std::vector<RealPillar> seq_;

  /// Advection dimensions for forward model
  int imgh_;
  int imgw_;
  
protected:
  /// Required evaluation methods by GA
  virtual double Evaluate();
  
public:
  /// Pillar geometry
  bool half_pillars_;
  float dw_min_;
  float dw_max_;
  float yw_min_;
  float yw_max_;
  float hw_max_;
  float hw_min_;
  FsChromosome() : Chromosome(), inlet_(), seq_() {
    // TODO: The following is a patch for an uninitialized
    //       value in gafr. gafr needs to be updated to to
    //       remove this bug and this line can go away.
    this->SetThreadData(nullptr);
  }
  
  /// Required clone method by GA
  virtual GA::Chromosome * Clone() {
    FsChromosome * clone = new FsChromosome();
    CloneBase(clone);
    clone->SetInlet(inlet_);
    clone->SetPillars(seq_);
    clone->SetDim(imgh_, imgw_);
    clone->SetPillarGeo(half_pillars_);
    
    return clone;
  }
  
  /// Helper method used to adjust inlet if it
  /// doesn't meet constraints.
  void FixInlet();
  
  /// Required generate method by GA.
  virtual void Generate(bool pre_computed_seed = false);
  
  /// Required mutate method by GA.
  virtual void Mutate();

  /// Required print stateent by GA
  virtual void PrintData();
  
  /// SetInlet used to force this chromosome to assume an inlet (for cloning).
  void SetInlet(InletDesign& newInlet) {
    inlet_ = newInlet;
    FixInlet();
  }
  
  void SetInlet(InletDesign& newInlet, bool useStaticInlet) {
    inlet_ = newInlet;
    if (!useStaticInlet) {
      FixInlet();
    }
  }

  void SetDim(int imgh, int imgw) {
    imgh_ = imgh;
    imgw_ = imgw;
  }
  
  void SetPillarGeo(bool half_pillars) {
    half_pillars_ = half_pillars;
    if (half_pillars) {
      dw_max_ = DW_MAX - E;
      dw_min_ = DW_MIN + E;
      
      hw_min_ = HW_MIN + E;
      hw_max_ = HW_MAX - E;
    } else {
      dw_max_ = DW_MAX_FP - E;
      dw_min_ = DW_MIN_FP + E;
      
      hw_min_ = HW_MIN_FP + E;
      hw_max_ = HW_MAX_FP;
    }
    
    yw_min_ = -1*YW_MIN + E;
    yw_max_ = YW_MAX - E;
  }
  
  const InletDesign& GetInlet() const { return inlet_; }
  const std::vector<RealPillar>& GetPillars() const { return seq_; }
  
  /// Create pillar with random dw, yw, hw for pillar <index> in a sequence
  RealPillar GenerateRandomPillar(int index);
  
  /// Create new pillar diameter within allowed geometry
  inline double NewPillarDw();
  
  /// SetPillar used to force this chromosome to assume a pillar sequence (for cloning).
  void SetPillars(std::vector<RealPillar>& new_seq) { seq_ = new_seq; }
  
  /// Gets the ith pillar in the sequence
  RealPillar GetPillar(int i) { return seq_[i]; }
  std::vector<RealPillar> GetSeq() { return seq_; }
  
  /// Gets this chromosome's pillar sequence (indices) as a string
  std::string GetPillarSeqIdx();
  /// Prints this chromosome's data as JSON parse-able text.
  void PrintJson();
  
  virtual ~FsChromosome() { }
};

void SaveResult(std::string filename, GA::Configuration * config, FsChromosome& fsch);

/// Required allocator method.
GA::Chromosome * FsChromosomeAllocate();

void MakePillarValid(const bool half_pillars, const float& hw, float& dw);

#endif /* fs_chromosome_h */
