### windows.mk for use with Windows NMAKE system
#
# Only the following lines should change in most cases:

OPENCV_DIR=.\thirdparty\libs\opencv
FFTW_DIR=.\thirdparty\libs\fftw
ZLIB_DIR=.\thirdparty\libs\zlib

#####################################
# DO NOT MODIFY ANYTHING BELOW HERE
# (Unless you know what you're doing)

OBJ=obj/fs_crossover.obj obj/fs_mutation.obj obj/main.obj obj/fs_chromosome.obj obj/fs_fft.obj obj/fs_multires.obj obj/fs_inletgenerator.obj obj/fs_setup.obj obj/fs_fitness.obj obj/fs_imagecache.obj obj/fs_mapcache.obj obj/fs_image.obj obj/fs_utilities.obj obj/fs_interpolate.obj obj/fs_forward_model.obj obj/fs_flowsculptga.obj
INCFLAGS=/Igafr\include /Iinclude /I$(FFTW_DIR) /I$(OPENCV_DIR)\build\include /I$(ZLIB_DIR)
LINKFLAGS=/LIBPATH:$(FFTW_DIR) /LIBPATH:$(ZLIB_DIR) /LIBPATH:.\gafr\bin /LIBPATH:$(OPENCV_DIR)\build\x64\vc14\lib /MACHINE:X64
LIBS=libgafr.lib libfftw3-3.lib opencv_world320.lib zlib.lib
CXXFLAGS=/nologo /EHsc /std:c++17 $(OFLAG)
CFLAGS=/nologo /EHsc $(OFLAG)

all: release

release: pre-build
    @cd gafr
    @nmake /nologo /f windows.mk
    @cd ..
    @nmake /nologo /f windows.mk bin/flowsculptga.exe OFLAG=/O2 

debug: pre-build
    @cd gafr
    @nmake /nologo /f windows.mk debug
    @cd ..
    @nmake /nologo /f windows.mk bin/flowsculptga.exe DEBUG=debug OFLAG=/Zi DFLAGS="/DDEBUG /FS /Fdbin\libgafr.pdb"

.PHONY: pre-build

pre-build:
    @if NOT EXIST "bin" mkdir "bin"
    @if NOT EXIST "obj" mkdir "obj"
    @copy $(FFTW_DIR)\libfftw3-3.dll bin
    @copy $(OPENCV_DIR)\build\x64\vc14\bin\opencv_world320.dll bin

bin/flowsculptga.exe: $(OBJ)
    @cl $(CXXFLAGS) $(INCFLAGS) $(DFLAGS) /MP /Febin\flowsculptga.exe $(OBJ) $(LIBS) /link $(LINKFLAGS)

{src}.cpp{obj}.obj::
    @cl $(CXXFLAGS) $(INCFLAGS) $(DFLAGS) /MP /Foobj\ /c $<

clean:
    @del /Q obj\*
    @del /Q bin\*
    @cd gafr
    @nmake /nologo /f windows.mk clean
    @cd ..

