FlowSculpt GA
======

Genetic algorithm optimization for micropillar sequence design.

## What does this do?
This repository contains all the source code and data necessary to run a GA to design micropillar sequences for a desired outlet fluid flow shape.

It contains a submodule reference (See below) to gafr, a C++ based framework for Genetic Algorithm optimization.

## Getting started
### 1. Dependancies (Third-party libraries)
FlowSculptGA was built to be as dependancy-free as possible.  That said, we have made use of third-party libraries to efficiently extend functionality:

* FFTW (www.fftw.org)
* OpenCV (www.opencv.org)
* zlib (www.zlib.net)

These can be installed on your own, or you can let FlowSculpt install them for you (see step 3).

### 2. Cloning the repository
The first step is to clone this repository off BitBucket:
~~~
git clone https://bitbucket.org/baskargroup/flowsculptga.git
~~~

### 3. Building FlowSculptGA

#### Dependencies
If you already have the FFTW, OpenCV, and zlib libraries installed, you can modify the appropriate `make` file, uncommenting the locations at the top of the file, and replacing them with the installation directories for the libraries:

* Windows -> windows.mk
* MacOS -> linux.mk
* Linux -> linux.mk 

Otherwise, FlowSculpt contains platform agnostic scripts to install these libraries.  Simply navigate to the `third party` folder, and execute the appropriate script:

* Windows -> setup_windows.ps1
* MacOS -> setup_mac.sh
* Linux -> setup_linux.sh

#### Linux / Make
To build on Linux or Unix based systems with GNU Make, type in bash:
~~~
$ make -f linux.mk 
~~~

#### Windows / NMake
To build on Windows, type in PowerShell*:
~~~
PS> nmake /f windows.mk
~~~

\* Note: this assumes your PowerShell environment is set up to access Visual Studio command line tools. If you haven't done this, you can instead open Visual Studio and run these commands from the `Visual Studio Command Prompt` located in the `Tools` menu.

### 4. Running FlowSculptGA via CLI
FlowSculpt uses a user-modifiable configuration file for each search.  To run FlowSculpt from the command line, give the location of the configuration file:
~~~
flowsculptga[.exe] ga.config
~~~

The configuration file must contain the following parameters, with their default values shown here:
```
half_pillars: false
mbundle_folder: mbundles
imgw: 801
imgh: 101
renum: 20
target: test/example/target_example.png
minpillars: 5
maxpillars: 10
reps: 2
num_inlets: 5
#fixed_inlet: 00100
fitness_function: Corr2Fitness
PopulationSize: 100
MaxGeneraions: 200
MutationRate: 0.05
numpar: 4
result_directory: .
result_suffix: 0
per_pillar_results: true
```
### 5. Running FlowSculpt via GUI
The GUI version of FlowSculpt is available at www.flowsculpt.org

* A target flow shape can be drawn in the design canvas in the top part of the GUI, or loaded from file as an image.  For now,
the target image must by of 801 x 101 pixels
* Once a shape is drawn, click "set design canvas as target" to lock-on the drawn image as the GA target
* Set GA parameters as desired.  Recommended parameters are:
	* Pillars start: 1
	* Pillars stop: 10
	* GA reps: 10
	* GA population: 100
* If you want to fix the inlet flow configuration, check "Fix inlet design", and enter a design in the "inlet design" text box below.
The inlet flow configuration is limited to 5, 7, and 9 channels.  For example, [0,0,1,0,0] would have 5 channels, with the first and 
last two channels being "empty", and the middle channel containing the tracked fluid.


## Who to contact
* Michael Davies
* Daniel Stoecklein
* Baskar Ganapathysubramanian