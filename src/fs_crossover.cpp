//
//  fs_crossover.cpp
//  flowsculptga
//
//  Created by Dan Stoecklein on 2/1/19.
//

#include "fs_crossover.h"

#define LAPLACE_B 0.15
#define LAPLACE_B_INT 0.35

using namespace GA;

void FsChromosomeCrossover(
                           const GA::Configuration& config,
                           GA::Chromosome * child,
                           const GA::Chromosome * parent1,
                           const GA::Chromosome * parent2) 
{
  InletDesign inlet;
  std::vector<RealPillar> seq;
  
  FsChromosome * pc1 = (FsChromosome*)parent1;
  FsChromosome * pc2 = (FsChromosome*)parent2;

  // Check if the selected chromosomes are the same.  If so,
  // create a copy of the chromosome
  if (pc1->GetId() == pc2->GetId()) 
  {
    std::vector<RealPillar> seq = pc1->GetSeq();
    InletDesign inlet = pc1->GetInlet();
    
    ((FsChromosome*)child)->SetInlet(inlet);
    ((FsChromosome*)child)->FixInlet();
    ((FsChromosome*)child)->SetPillars(seq);
    ((FsChromosome*)child)->SetDim(config.GetSetting("imgh").AsInt, config.GetSetting("imgw").AsInt);
    ((FsChromosome*)child)->SetPillarGeo(config.GetSetting("half_pillars").AsBool);
  } 
  else 
  {
    CrossoverLaplace(pc1, pc2, child, inlet, seq, config);
  }
}

float LaplaceOperator(const float& x1, const float& x2, const float& xmin, const float& xmax) 
{
  double beta;
  
  double r = GA::Random::GetRandom();
  double u = GA::Random::GetRandom();
  
  double a = 0;
  double b = LAPLACE_B;
  
  if (r >= 0.5) 
  {
    beta = a - b * log(u);
  } 
  else 
  {
    beta  = a + b * log(u);
  }
  
  double interval = fabs(x1 - x2);
  
  float x_out;
  float modifier = beta * interval;
  
  if (r >= 0.5) 
  {
    x_out = x1 + modifier;
  } 
  else 
  {
    x_out = x2 + modifier;
  }
  
  /// Clamp values
  x_out = fmax(x_out, xmin);
  x_out = fmin(x_out, xmax);
  
  return x_out;
}

/// Overload for integer values
int LaplaceOperator(const int& x1, const int& x2, const int& xmin, const int& xmax) 
{
  double beta;
  
  double r = GA::Random::GetRandom();
  double u = GA::Random::GetRandom();
  
  double a = 0;
  double b = LAPLACE_B_INT;
  
  if (r >= 0.5) 
  {
    beta = a - b * log(u);
  } 
  else 
  {
    beta  = a + b * log(u);
  }
  
  double interval = fabs(x1 - x2);
  int x_out;
  
  int modifier = int(beta * interval);
  
  if (r >= 0.5) 
  {
    x_out = x1 + modifier;
  } 
  else 
  {
    x_out = x2 + modifier;
  }
  
  /// Clamp values
  x_out = fmax(x_out, xmin);
  x_out = fmin(x_out, xmax);
  
  return x_out;
}

/// Overload for double values
double LaplaceOperator(const double& x1, const double& x2, const double& xmin, const double& xmax) 
{
  double beta;
  
  double a = 0;
  double b = LAPLACE_B;
  
  double r = GA::Random::GetRandom();
  double u = GA::Random::GetRandom();
  
  if (r >= 0.5) 
  {
    beta = a - b * log(u);
  } 
  else 
  {
    beta  = a + b * log(u);
  }
  
  double interval = fabs(x1 - x2);
  double x_out;
  
  double modifier = beta * interval;
  
  if (r >= 0.5) 
  {
    x_out = x1 + modifier;
  } 
  else 
  {
    x_out = x2 + modifier;
  }
  
  /// Clamp values
  x_out = fmax(x_out, xmin);
  x_out = fmin(x_out, xmax);
  
  return x_out;
}

void CrossoverLaplace(FsChromosome * pc1, FsChromosome * pc2, GA::Chromosome * child,
                      InletDesign& inlet, std::vector<RealPillar>& seq,
                      const GA::Configuration& config) 
{
  // Algorithm for Laplace crossover

  // Retreive flow sculpting and flow shape image parameters  
  int num_pillars = config.GetSetting("numPillars").AsInt;
  int num_material = config.GetSetting("num_material").AsInt;
  bool fixed_inlet = config.GetSetting("fixed_inlet").AsBool;
  bool half_pillars = config.GetSetting("half_pillars").AsBool;
  int num_inlets = config.GetSetting("num_inlets").AsInt;
  bool uniform_pillar_width = config.GetSetting("uniform_pillar_width").AsBool;
  int imgh = config.GetSetting("imgh").AsInt;
  int imgw = config.GetSetting("imgw").AsInt;
  //int generation_num = config.GetSetting("GenerationNum").AsInt;
  
  // Set channel width limits
  double min_cw = config.GetSetting("min_channel_width").AsDouble;
  double max_cw = 1.0 - (num_inlets - 1) * min_cw;
  
  // Crossover inlet flow pattern
  if (!fixed_inlet) 
  {
    for (int i = 0; i < num_inlets; i++) 
    {
      if (i < num_inlets - 1) 
      {
        inlet.first.push_back(LaplaceOperator(pc1->GetInlet().first[i], pc2->GetInlet().first[i], min_cw, max_cw));
      }
      inlet.second.push_back(LaplaceOperator(pc1->GetInlet().second[i], pc2->GetInlet().second[i], 0, num_material));
    }
  }
  
  // Crossover pillar geometries
  for (int i = 0; i < num_pillars; i++) 
  {
    float yw, dw, hw;

    yw = LaplaceOperator(pc1->GetPillar(i).yw, pc2->GetPillar(i).yw, pc1->yw_min_, pc1->yw_max_);
    
    // If all pillars must be of a uniform size, choose the first pillar's diameter as the reference
    if (uniform_pillar_width && i > 0) 
    {
      dw = seq[0].dw;
    } 
    else 
    {
      dw = LaplaceOperator(pc1->GetPillar(i).dw, pc2->GetPillar(i).dw, pc1->dw_min_, pc1->dw_max_);
    }
    
    // If half-pillars are allowed, crossover pillar height
    if (half_pillars) 
    {
      hw = LaplaceOperator(pc1->GetPillar(i).hw, pc2->GetPillar(i).hw, pc1->hw_min_, pc1->hw_max_);
    }
    else {
      hw = 1.0f;
    }
    
    RealPillar rp(yw, dw, hw);
    
    
    // Perform a check to make sure the resulting pillar geometry is valid
    if (!rp.IsValid()) 
    {
    // N.B. At this stage, the only thing that could make a pillar invalid
    //      is if it became a full pillar while keeping dw > 0.75. To correct
    //      this case, we will simply change it back to a half-pillar.
      // TODO: In the case where half pillars are disabled we need to
      //       make sure not to accidentally create one. This logic should be
      //       be changed such that it doesn't just ignore invalid pillars
      //       in the full-pillar case.
      MakePillarValid(half_pillars, rp.hw, rp.dw);
    }
    
    // Add pillar to sequence
    seq.push_back(rp);
  }

  ((FsChromosome*)child)->SetInlet(inlet, fixed_inlet);
  ((FsChromosome*)child)->FixInlet();
  ((FsChromosome*)child)->SetPillars(seq);
  ((FsChromosome*)child)->SetDim(imgh, imgw);
  ((FsChromosome*)child)->SetPillarGeo(half_pillars);
}

void CrossoverUniform(FsChromosome * pc1, FsChromosome * pc2, GA::Chromosome * child,
                      InletDesign& inlet, std::vector<RealPillar>& seq,
                      const GA::Configuration& config) 
{
  // Algorithm for uniform crossover
  int num_pillars = config.GetSetting("numPillars").AsInt;
  bool fixed_inlet = config.GetSetting("fixed_inlet").AsBool;
  bool half_pillars = config.GetSetting("half_pillars").AsBool;
  int num_inlets = config.GetSetting("num_inlets").AsInt;
  bool uniform_pillar_width = config.GetSetting("uniform_pillar_width").AsBool;
  
  int imgh = config.GetSetting("imgh").AsInt;
  int imgw = config.GetSetting("imgw").AsInt;
  
  if(!fixed_inlet) 
  {
    for (int i = 0; i < num_inlets; i++) 
    {
      if (i < num_inlets - 1) 
      {
        if (GA::Random::GetRandom() >= 0.5) 
        {
          inlet.first.push_back(pc1->GetInlet().first[i]);
        } 
        else 
        {
          inlet.first.push_back(pc2->GetInlet().first[i]);
        }
      }
      
      if (GA::Random::GetRandom() >= 0.5) 
      {
        inlet.second.push_back(pc1->GetInlet().second[i]);
      } 
      else 
      {
        inlet.second.push_back(pc2->GetInlet().second[i]);
      }
    }
  }
  
  for (int i = 0; i < num_pillars; i++) 
  {
    float yw, dw, hw;
    
    if (GA::Random::GetRandom() >= 0.5) 
    {
      yw = pc1->GetPillar(i).yw;
    } 
    else 
    {
      yw = pc2->GetPillar(i).yw;
    }
    
    if (uniform_pillar_width && i > 0) 
    {
      dw = seq[0].dw;
    } 
    else {
      if (GA::Random::GetRandom() >= 0.5) 
      {
        dw = pc1->GetPillar(i).dw;
      } 
      else 
      {
        dw = pc2->GetPillar(i).dw;
      }
    }
    
    if (half_pillars) 
    {
      if (GA::Random::GetRandom() >= 0.5) 
      {
        hw = pc1->GetPillar(i).hw;
      }
      else {
        hw = pc2->GetPillar(i).hw;
      }
    }
    else {
      hw = 1.0f;
    }
    
    RealPillar rp(yw, dw, hw);
    
    // N.B. At this stage, the only thing that could make a pillar invalid
    //      is if it became a full pillar while keeping dw > 0.75. To correct
    //      this case, we will simply change it back to a half-pillar.
    
    if (!rp.IsValid()) 
    {
      MakePillarValid(half_pillars, rp.hw, rp.dw);
    }
    
    seq.push_back(rp);
  }
  
  ((FsChromosome*)child)->SetInlet(inlet, fixed_inlet);
  ((FsChromosome*)child)->FixInlet();
  ((FsChromosome*)child)->SetPillars(seq);
  ((FsChromosome*)child)->SetDim(imgh, imgw);
  ((FsChromosome*)child)->SetPillarGeo(half_pillars);
}
