#include "fs_interpolate.h"

std::vector<double> linspace(double first, double last, int len) {
  std::vector<double> result(len);
  double step = (last-first) / (len - 1);
  for (int i=0; i<len; i++) { result[i] = first + i*step; }
  return result;
}

void MeshGrid(std::vector<double>& X, std::vector<double>& Y,
              const int nx_coarse, const int ny_coarse,
              const int nx_fine, const int ny_fine) {
  
  std::vector<double> x_linspace = linspace(1.0,
                                            static_cast<double>(nx_coarse),
                                            nx_fine);
  std::vector<double> y_linspace = linspace(1.0,
                                            static_cast<double>(ny_coarse),
                                            ny_fine);
  
  for (int n=0; n<nx_fine*ny_fine; n++) {
    int i = (n)%(nx_fine);
    int j = (n)/(nx_fine);
    X[n] = x_linspace[i];
    Y[n] = y_linspace[j];
  }
}

void interp_func(std::vector<double> &B, std::vector<double> A,
                 std::vector<double> X, std::vector<double> Y,
                 const int num_points, const int w, const int h,
                 const int col, const double oobv)
{
  int end = num_points * col;
  int step = h * w;
  
  double dw = (double)w;
  double dh = (double)h;
  
  // For each of the interpolation points
  int i, j, k, x, y;
  double u, v, out;
  // #pragma omp parallel for if (num_points > 300) num_threads(omp_get_num_procs()) default(shared) private(i,j,k,u,v,x,y,out)
  for (i = 0; i < num_points; i++) {
    
    if (X[i] >= 1 && Y[i] >= 1) {
      if (X[i] < dw) {
        if (Y[i] < dh) {
          // Linearly interpolate
          x = (int)X[i];
          y = (int)Y[i];
          u = X[i] - x;
          v = Y[i] - y;
          k = h * (x - 1) + y - 1;
          for (j = i; j < end; j += num_points, k += step) {
            out = A[k] + (A[k+h] - A[k]) * u;
            out += ((A[k+1] - out) + (A[k+h+1] - A[k+1]) * u) * v;
            B[j] = out;
          }
        } else if (Y[i] == dh) {
          // The Y coordinate is on the boundary
          // Avoid reading outside the buffer to avoid crashes
          // Linearly interpolate along X
          x = (int)X[i];
          u = X[i] - x;
          k = h * x - 1;
          for (j = i; j < end; j += num_points, k += step)
            B[j] = (A[k] + (A[k+h] - A[k]) * u);
        } else {
          // Out of bounds
          for (j = i; j < end; j += num_points)
            B[j] = oobv;
        }
      } else if (X[i] == dw) {
        if (Y[i] < dh) {
          // The X coordinate is on the boundary
          // Avoid reading outside the buffer to avoid crashes
          // Linearly interpolate along Y
          y = (int)Y[i];
          v = Y[i] - y;
          k = h * (w - 1) + y - 1;
          for (j = i; j < end; j += num_points, k += step)
            B[j] = A[k] + (A[k+1] - A[k]) * v;
        } else if (Y[i] == dh) {
          // The X and Y coordinates are on the boundary
          // Avoid reading outside the buffer to avoid crashes
          // Output the last value in the array
          k = h * w - 1;
          for (j = i; j < end; j += num_points, k += step)
            B[j] = A[k];
        } else {
          // Out of bounds
          for (j = i; j < end; j += num_points)
            B[j] = oobv;
        }
      } else {
        // Out of bounds
        for (j = i; j < end; j += num_points)
          B[j] = oobv;
      }
    } else {
      // Out of bounds
      for (j = i; j < end; j += num_points)
        B[j] = oobv;
    }
  }
  return;
}

void interp_func(std::vector<float> &B, std::vector<double> A,
                 std::vector<double> X, std::vector<double> Y,
                 const int num_points, const int w, const int h,
                 const int col, const double oobv)
{
  int end = num_points * col;
  int step = h * w;
  
  double dw = (double)w;
  double dh = (double)h;
  
  // For each of the interpolation points
  int i, j, k, x, y;
  double u, v, out;
  // #pragma omp parallel for if (num_points > 300) num_threads(omp_get_num_procs()) default(shared) private(i,j,k,u,v,x,y,out)
  for (i = 0; i < num_points; i++) {
    
    if (X[i] >= 1 && Y[i] >= 1) {
      if (X[i] < dw) {
        if (Y[i] < dh) {
          // Linearly interpolate
          x = (int)X[i];
          y = (int)Y[i];
          u = X[i] - x;
          v = Y[i] - y;
          k = h * (x - 1) + y - 1;
          for (j = i; j < end; j += num_points, k += step) {
            out = A[k] + (A[k+h] - A[k]) * u;
            out += ((A[k+1] - out) + (A[k+h+1] - A[k+1]) * u) * v;
            B[j] = (float)out;
          }
        } else if (Y[i] == dh) {
          // The Y coordinate is on the boundary
          // Avoid reading outside the buffer to avoid crashes
          // Linearly interpolate along X
          x = (int)X[i];
          u = X[i] - x;
          k = h * x - 1;
          for (j = i; j < end; j += num_points, k += step)
            B[j] = (float)(A[k] + (A[k+h] - A[k]) * u);
        } else {
          // Out of bounds
          for (j = i; j < end; j += num_points)
            B[j] = (float)oobv;
        }
      } else if (X[i] == dw) {
        if (Y[i] < dh) {
          // The X coordinate is on the boundary
          // Avoid reading outside the buffer to avoid crashes
          // Linearly interpolate along Y
          y = (int)Y[i];
          v = Y[i] - y;
          k = h * (w - 1) + y - 1;
          for (j = i; j < end; j += num_points, k += step)
            B[j] = (float)(A[k] + (A[k+1] - A[k]) * v);
        } else if (Y[i] == dh) {
          // The X and Y coordinates are on the boundary
          // Avoid reading outside the buffer to avoid crashes
          // Output the last value in the array
          k = h * w - 1;
          for (j = i; j < end; j += num_points, k += step)
            B[j] = (float)A[k];
        } else {
          // Out of bounds
          for (j = i; j < end; j += num_points)
            B[j] = (float)oobv;
        }
      } else {
        // Out of bounds
        for (j = i; j < end; j += num_points)
          B[j] = (float)oobv;
      }
    } else {
      // Out of bounds
      for (j = i; j < end; j += num_points)
        B[j] = (float)oobv;
    }
  }
  return;
}

std::vector<double> Interp2DFunc(std::vector<double> coarse_data,
                                 const int coarse_nx, const int coarse_ny,
                                 const int fine_nx, const int fine_ny,
                                 Q_Boundary q_bound) {
  
  const int col = 2;
  const double oobv = 4.0;
  const int num_points = fine_nx * fine_ny;
  
  std::vector<double> X(fine_nx * fine_ny);
  std::vector<double> Y(fine_nx * fine_ny);
  std::vector<double> B(num_points*2);
  
  MeshGrid(X, Y, coarse_nx, coarse_ny, fine_nx, fine_ny);
  
  interp_func(B, coarse_data, X, Y, num_points, coarse_nx, coarse_ny, col, oobv);
  
  return B;
}

std::vector<float> Interp2DFuncF(std::vector<double> coarse_data,
                                 const int coarse_nx, const int coarse_ny,
                                 const int fine_nx, const int fine_ny,
                                 Q_Boundary q_bound) {
  
  const int col = 2;
  const double oobv = 4.0;
  const int num_points = fine_nx * fine_ny;
  
  std::vector<double> X(fine_nx * fine_ny);
  std::vector<double> Y(fine_nx * fine_ny);
  std::vector<float> B(num_points*2);
  
  MeshGrid(X, Y, coarse_nx, coarse_ny, fine_nx, fine_ny);
  
  interp_func(B, coarse_data, X, Y, num_points, coarse_nx, coarse_ny, col, oobv);
  
  return B;
}

std::vector<std::vector<double>> QuiverInterpolate(std::vector<std::vector<double>> vData, const int coarse_nx,
                                                   const int coarse_ny, const int fine_nx, const int fine_ny,
                                                   Q_Boundary q_bound) {
  
  std::vector<double> dx_data(coarse_nx * coarse_ny * 2);
  
  // Convert to column major for some reason
  int counter = 0;
  for (int i=0; i<coarse_nx; i++) {
    for (int j=0; j<coarse_ny; j++) {
      dx_data[counter] = vData[j * coarse_nx + i][2];
      dx_data[counter + coarse_nx * coarse_ny] = vData[j * coarse_nx + i][3];
      counter++;
    }
  }
  
  std::vector<double> dx_i = Interp2DFunc(dx_data, coarse_nx, coarse_ny,
                                          fine_nx, fine_ny, q_bound);
  
  // Assemble final interpolated quiver
  std::vector<double> fine_grid1 = linspace(q_bound.xmin, q_bound.xmax, fine_nx);
  std::vector<double> fine_grid2 = linspace(q_bound.ymin, q_bound.ymax, fine_ny);
  std::vector<std::vector<double>> vData_interp;
  for (int n = 0; n < fine_nx * fine_ny; n++) {
    int i = n % fine_nx;
    int j = n / fine_nx;
    std::vector<double> row(4);
    row[0] = fine_grid1[i];
    row[1] = fine_grid2[j];
    row[2] = dx_i[n];
    row[3] = dx_i[n + fine_nx*fine_ny];
    vData_interp.push_back(row);
  }
  return vData_interp;
}

std::vector<float> AdvectionInterpolate(std::vector<float> adata, const int coarse_nx,
                                        const int coarse_ny, const int fine_nx, const int fine_ny,
                                        Q_Boundary q_bound) {
  
  std::vector<double> dx_data(coarse_nx * coarse_ny * 2);
  
  // Convert to column major for some reason
  int counter = 0;
  for (int i=0; i<coarse_nx; i++) {
    for (int j=0; j<coarse_ny; j++) {
      // dx
      dx_data[counter] = (double)adata[j*coarse_nx+i];
      // dy
      dx_data[counter+coarse_nx*coarse_ny] = (double)adata[coarse_nx*coarse_ny+j*coarse_nx+i];
      counter++;
    }
  }
  
  std::vector<float> dx_i = Interp2DFuncF(dx_data, coarse_nx, coarse_ny,
                                          fine_nx, fine_ny, q_bound);
  
  return dx_i;
}
