//
//  fs_mutation.cpp
//  flowsculptga
//
//  Created by Dan Stoecklein on 2/1/19.
//

#include "fs_mutation.h"

#define POWER_P 10

float PowerMutation(const float& x, const float& x_min, const float& x_max) 
{
  double p = POWER_P;
  
  double u = GA::Random::GetRandom();
  double r = GA::Random::GetRandom();
  double s = pow(u, p);
  float t = (x - x_min) / (x_max - x_min);
  
  float mutation;
  
  if (t < r) 
  {
    mutation = -1 * s * (x - x_min);
  } 
  else 
  {
    mutation = s * (x_max - x);
  }
  
  return (x + mutation);
}

double PowerMutation(const double& x, const double& x_min, const double& x_max) 
{
  double p = POWER_P;
  
  double u = GA::Random::GetRandom();
  double r = GA::Random::GetRandom();
  double s = pow(u, p);
  double t = (x - x_min) / (x_max - x_min);
  
  double mutation;
  
  if (t < r) 
  {
    mutation = -1 * s * (x - x_min);
  } 
  else 
  {
    mutation = s * (x_max - x);
  }
  
  return (x + mutation);
}

int PowerMutation(const int x, const int x_min, const int x_max) 
{
  double p = 4;
  double u = GA::Random::GetRandom();
  double r = GA::Random::GetRandom();
  double s = pow(u, p);
  
  double t = double(x - (x_min-1)) / double(x_max - (x_min-1));
  
  double mutation;
  
  if (t < r) 
  {
    mutation = round(-1 * s * (x - x_min));
  } 
  else 
  {
    mutation = round(s * (x_max - x));
  }
  
  return (int)(x + mutation);
}

void FsChromosome::Mutate() {
  int num_pillars = config->GetSetting("numPillars").AsInt;
  int num_inlets = config->GetSetting("num_inlets").AsInt;
  bool fixed_inlet = config->GetSetting("fixed_inlet").AsBool;
  int num_material = config->GetSetting("num_material").AsInt;
  bool half_pillars = config->GetSetting("half_pillars").AsBool;
  double min_cw = config->GetSetting("min_channel_width").AsDouble;
  
  bool uniform_pillar_width = config->GetSetting("uniform_pillar_width").AsBool;
  
  for (int i = 0; i < num_pillars; i++) 
  {
    RealPillar rp = seq_[i];
    
    // Mutate diameter
    if (uniform_pillar_width && i > 0) 
    {
      rp.dw = seq_[0].dw;
      seq_[i].dw = seq_[0].dw;
    } 
    else 
    {
      rp.dw = PowerMutation(rp.dw, dw_min_, dw_max_);
    }
    
    // Mutate location
    rp.yw = PowerMutation(rp.yw, yw_min_, yw_max_);
    
    if (half_pillars) 
    {
      // Mutate height
      rp.hw = PowerMutation(rp.hw, hw_min_, hw_max_);
      
    } 
    else 
    {
      // Don't bring half-pillars into full-pillar situations
      rp.hw = 1.0f;
    }
    
    if (!rp.IsValid()) 
    {
      MakePillarValid(half_pillars, rp.hw, rp.dw);
    }
    seq_[i] = rp;
  }
  
  if (!fixed_inlet) 
  {
    // This algorithm keeps inlets ordered during mutation
    double channel_sum = 0.0;
    
    for (int i = 0; i < inlet_.first.size(); i++) 
    {
      channel_sum += inlet_.first[i];
    }
    
    for (int i = 0; i < num_inlets; i++) 
    {
      if (i < num_inlets - 1) 
      {
        channel_sum -= inlet_.first[i];

        inlet_.first[i] = PowerMutation(inlet_.first[i], min_cw, 1.0f-min_cw);
        
        if (channel_sum + inlet_.first[i] + min_cw > 1.0) 
        {
          inlet_.first[i] = 1.0 - channel_sum - min_cw;
        } 
        else if (inlet_.first[i] < min_cw) 
        {
          inlet_.first[i] = min_cw;
        }
        channel_sum += inlet_.first[i];
        
        assert(channel_sum + min_cw <= 1.0);
      }
      inlet_.second[i] = PowerMutation(inlet_.second[i], 0, num_material);
    }
  }
}
