#include "fs_flowsculptga.h"
#include "fs_image.h"
#include "fs_interpolate.h"
#include "fs_forward_model.h"

#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_RESIZE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION

#include "stb_image.h"
#include "stb_image_resize.h"
#include "stb_image_write.h"

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string.h>

#define MAX_MATERIAL 5

void SplitLineDouble(const std::string& line, std::vector<double>& list, char sep) 
{
  int len = line.length();
  char * copy = new char[line.length() + 1];
  strcpy(copy, line.c_str());

  char * iter = copy;

  while (*iter) 
  {
    if (*iter == sep) 
    {
      *iter = '\0';
    }
    iter++;
  }

  iter = copy;
  while (iter < copy + len) 
  {
    list.push_back(std::stod(iter));
    iter += strlen(iter) + 1;
  }

  delete[] copy;
}

void SplitLineInt(const std::string& line, std::vector<int>& list, char sep) 
{
  int len = line.length();
  char * copy = new char[line.length() + 1];
  strcpy(copy, line.c_str());

  char * iter = copy;

  while (*iter) 
  {
    if (*iter == sep) 
    {
      *iter = '\0';
    }
    iter++;
  }

  iter = copy;
  while (iter < copy + len) 
  {
    list.push_back(std::stoi(iter));
    iter += strlen(iter) + 1;
  }

  delete[] copy;
}

inline int material_to_color(int d) 
{
  //
  // N.B. For some reason the byte order for stb is reversed
  //      for colors. So we return ABGR instead of RGBA for 
  //      color.
  //

  switch (d) 
  {
    case 0: return 0xFFFFFFFF; // White
    case 1: return 0xFFC39F5F; // Blue
    case 2: return 0xFF5E88FF; // Orange
    case 3: return 0xFF5E9E00; // Green
    case 4: return 0xFF42E4F0; // Yellow 
    case 5: return 0xFF005ED5; // Red
  }

  return 0xFF000000;
}

inline int red_value(int abgr) 
{
  return abgr & 0x000000FF;
}

inline int green_value(int abgr) 
{
  return (abgr >> 8) & 0x000000FF;
}

inline int blue_value(int abgr) 
{
  return (abgr >> 16) & 0x000000FF;
}

inline int rgb_difference(int abgr, int R, int B, int G) 
{
  return abs(R - red_value(abgr))
    + abs(G - green_value(abgr))
    + abs(B - blue_value(abgr));
}

inline int fuzzy_color_to_material(int R, int G, int B) 
{
  int min_diff = rgb_difference(material_to_color(0), R, G, B);
  int material = 0;

  for (int i = 1; i < MAX_MATERIAL; i++) 
  {
    int diff = rgb_difference(material_to_color(i), R, G, B);
    if (diff < min_diff) 
    {
      min_diff = diff;
      material = i;
    }
  }
  //std::cerr << "material assigned = " << material << std::endl;
  return material;
}

inline int color_to_material(int R, int G, int B) 
{
  // N.B. Flip to BGR here to keep consistent ordering.
  unsigned int color_code = 0xFF000000 | (B << 16) | (G << 8) | R;

  // std::cerr << std::hex << color_code << std::dec << std::endl;

  switch (color_code) 
  {
    case 0xFFC39F5F: return 1; // Blue
    case 0xFF5E88FF: return 2; // Orange
    case 0xFF5E9E00: return 3; // Green
    case 0xFF42E4F0: return 4; // Yellow
    case 0xFF005ED5: return 5; // Red
  }

  return -1;
}

int GetMin(std::vector<int> arr) 
{
  int min = arr[0];
  for (int i = 1; i < arr.size(); i++) 
  {
    if (arr[i] < min) 
    {
      min = arr[i];
    }
  }
  return min;
}

int GetMax(std::vector<int> arr) 
{
  int max = arr[0];
  for (int i = 1; i < arr.size(); i++) 
  {
    if (arr[i] > max) 
    {
      max = arr[i];
    }
  }
  return max;
}

bool CheckConsecutive(std::vector<int> material_seen, int num_material) 
{
  //
  // This function is used to ensure that the material being targeted
  // are consecutively ordered (i.e., 1,2,3, rather than 1,4,5), as
  // the chromosome anticipates that multiple material are consecutively
  // indexed starting from 1.
  //
  int min = GetMin(material_seen);

  if (min > 1) 
  {
    return false;
  }

  bool sorted = std::is_sorted(material_seen.begin(), material_seen.end());

  int max = GetMax(material_seen);

  if (max - min  + 1 == num_material) 
  {
    int i;
    for(i = 0; i < num_material; i++) 
    {
      int j;

      if (material_seen[i] < 0) 
      {
        j = -material_seen[i] - min;
      } 
      else 
      {
        j = material_seen[i] - min;
      }

      if (material_seen[j] > 0) 
      {
        material_seen[j] = -material_seen[j];
      } 
      else 
      {
        return false;
      }
    }
    if (sorted) 
    {
      return true;
    }
    return false;
  }
  return false;
}

void Reordermaterial(InputImageData& image_data) 
{
  ///
  /// If flow materials are unordered (e.g., (1,3,5),
  /// this function will reorder them (1,2,3)
  ///
  
  std::map<int, int> material_pairs;
  std::vector<double> data_tmp;
  
  // Get map to re-order material
  // This is indexed from 1 to treat 0 as background material
  for (int i=1; i<=image_data.material_info.size(); i++) 
  {
    int material = image_data.material_info[i-1].material;
    material_pairs[material] = i;
  }
  
  for (int i=0; i<image_data.imgh*image_data.imgw; i++) 
  {
    if (image_data.data[i] == 0) 
    {
      continue;
    } 
    else 
    {
      image_data.data[i] = (double)material_pairs[image_data.data[i]];
    }
  }
}

void AnalyzeMaterials(std::vector<int>& material_seen,
                   std::vector<MaterialInfo>& material_info,
                   InputImageData& image_data) 
{
  // Sort material based on count for re-ordering
  std::sort(material_info.begin(), material_info.end());
  
  assert(material_seen.size() > 0);
  
  image_data.num_material = material_seen.size();
  image_data.material_seen = material_seen;
  image_data.material_info = material_info;
  
  // If material do not consecutive 1-2-3..., re-order
  // e.g., if material_seen = {2,5}, make {1,2}
  if (!CheckConsecutive(material_seen, material_seen.size())) 
  {
    Reordermaterial(image_data);
  }
  
#ifdef DEBUG
  std::cerr << "Number of material: " << material_seen.size() << std::endl;
  
  for (int s : material_seen) 
  {
    std::cerr << s << ", ";
  }
  
  std::cerr << std::endl;
#endif
  
}

int LoadTargetStbi(std::string filename, InputImageData &image_data) 
{

#ifdef DEBUG
  std::cerr << "Loading \"" << filename << "\"" << std::endl;
#endif

  int bpp;
  unsigned char * stbi_data = stbi_load(filename.c_str(), &image_data.imgw, &image_data.imgh, &bpp, 0);

  if (!stbi_data) 
  {
    std::cerr << "ERROR: Could not load target flow shape image at " << filename.c_str() << "; exiting." << std::endl;
    exit(-1);
  }

  int imgw = image_data.imgw;
  int imgh = image_data.imgh;

  int index = 0;

  image_data.data.resize(imgw*imgh, 0.0);

  std::vector<int> material_seen;
  std::vector<MaterialInfo> material_info;

  //
  // N.B. Stbi inverts the vertical dimension, so this loop goes in reverse to correct for this.
  //

  for (int row = imgh - 1; row >= 0; row--) 
  {
    for (int col = 0; col < imgw; col++) 
    {
      unsigned char R = stbi_data[(row * imgw + col) * bpp + 0];
      unsigned char G = stbi_data[(row * imgw + col) * bpp + 1];
      unsigned char B = stbi_data[(row * imgw + col) * bpp + 2];
      unsigned char A = 0xFF;
      int material = 0;

      if (bpp == 4) 
      {
        A = stbi_data[(row * imgw + col) * bpp + 3];
      }
      
      // Treat mostly transparent colors as background. This only applies
      // when an alpha channel exists.

      if (A < 0x7F) 
      {
        index++;
        continue;
      }

      // Pure white is treated as background.
      if ((R == 0xFF) && (G == 0xFF) && (B == 0xFF)) 
      {
        index++;
        continue;
      }

      // Any other color is foreground.
      material = color_to_material(R, G, B);

      if (material < 0) 
      {
        // material = 0;
        material = fuzzy_color_to_material(R, G, B);
        if (material == 0) 
        {
          // Whoops, 0 assigned somehow, should not count as material
          index++;
          continue;
        }
      }

      // Detect new material, or increment the count of known material
      auto it = std::find(material_seen.begin(), material_seen.end(), material);
      if (it == material_seen.end()) 
      {
        // New material detected
        material_seen.push_back(material);

        // Store material data
        MaterialInfo tmp;
        tmp.material = material;
        tmp.count = 1;
        material_info.push_back(tmp);
      } 
      else 
      {
        // material already seen, increment count
        auto count_index = std::distance(material_seen.begin(), it);
        material_info[count_index].count++;
      }

      image_data.data[index++] = (double)material;
    }
  }

  if (material_seen.size() == 0) 
  {
    stbi_image_free(stbi_data);
    return -1;
  }
  
  AnalyzeMaterials(material_seen, material_info, image_data);

  stbi_image_free(stbi_data);
  return 1;
}

int LoadTargetCSV(std::string filename, InputImageData& image_data) 
{
  std::ifstream ifs(filename);
  std::string line;

  assert(ifs.is_open());

  std::vector<int> material_seen;
  std::vector<MaterialInfo> material_info;

  //try {
    std::getline(ifs, line);
    SplitLineDouble(line, image_data.data, ',');
    image_data.imgw = image_data.data.size();
    image_data.imgh = 1;

    while (std::getline(ifs, line)) 
    {
      SplitLineDouble(line, image_data.data, ',');
      image_data.imgh++;
    }
  //} catch (std::exception & e) {
  //  std::cerr << "Error loading image." << std::endl;
   // std::cerr << e.what() << std::endl;
  //}

  // Analyze material within CSV file. For now, we trust the material are valid
  // (i.e., material indices from 1 to MAX_MATERIAL
  for (double material : image_data.data) 
  {
    // Detect new material, or increment the count of known material
    if (material == 0) 
    {
      // We currently count 0 not as a material, but background
      continue;
    }
    auto it = std::find(material_seen.begin(), material_seen.end(), material);
    if (it == material_seen.end()) 
    {
      // New material detected
      material_seen.push_back(material);
      // std::cerr << "Material " << material << " found" << std::endl;

      // Store material info
      MaterialInfo tmp;
      tmp.material = material;
      tmp.count = 1;
      material_info.push_back(tmp);
    } 
    else 
    {
      // material already seen, increment count
      auto count_index = std::distance(material_seen.begin(), it);
      material_info[count_index].count++;
    }
  }

  // The loaded image is upside down - needs to be flipped
  std::vector<double> tmp_data;
  for (int r = 0; r < image_data.imgh; r++) 
  {
    for (int c = 0; c < image_data.imgw; c++) 
    {
      int idx = (image_data.imgh - r - 1) * image_data.imgw + c;
      tmp_data.push_back(image_data.data[idx]);
    }
  }

  image_data.data = tmp_data;

  if (material_seen.size() == 0) 
  {
    std::cerr << "exiting at loader" << std::endl;
    return -1;
  }

  AnalyzeMaterials(material_seen, material_info, image_data);

  return 1;
}

bool EndsWith(const std::string& str, const std::string& suffix) 
{
  return (str.size() >= suffix.size()) && 
    (str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0);
}

int LoadTarget(std::string filename, InputImageData& image_data) 
{
  /// Material override is switched to true if the number of materials
  /// specified by the user out numbers the materials detected. Thus,
  /// it is initialized as false
  image_data.num_material_override = false;
  try 
  {
  if (EndsWith(filename, std::string("csv"))) 
  {
    if (!LoadTargetCSV(filename, image_data)) 
    {
      return 0;
    } 
    else 
    {
      return 1;
    }
  } 
  else 
  {
    if (!LoadTargetStbi(filename, image_data)) 
    {
      return 0;
    } 
    else 
    {
      return 1;
    }
  }
  } 
  catch (std::exception &e) 
  {
    std::cout << "Error: could not load image." << std::endl;
    std::cout << e.what() << std::endl;
  }
  return 0;
}

CrossSection Target2CrossSection(InputImageData& image_data, GA::Configuration* config,
    int imgw, int imgh) 
{
  CrossSection result(imgw*imgh);

  // Check if number of material in target is different than number of material in
  // ga.config.  If there are fewer material in the target, there's nothing to be
  // done - we cannot conjure material in place, and we might want to clamp the
  // configuration's number of material here if so.  If there are more material
  // detected than specified in the search, then the smaller-volume material
  // will be dropped from the target (for now).
  int num_material_setting = config->GetSetting("num_material").AsInt;
  if ((image_data.material_seen.size() > num_material_setting) && (num_material_setting != 0)) 
  {
    for (int i = 0; i<image_data.data.size(); i++) 
    {
      if (image_data.data[i] > num_material_setting) 
      {
        image_data.data[i] = 0;
      }
    }
    /// Reset image_data.num_material without modifying other material_info
    image_data.num_material_override = true;
    image_data.num_material = num_material_setting;
  }
  
  /// If the loaded image does not match FlowSculpt's advection data
  /// dimensions, we resize them here

  if (imgw != image_data.imgw || imgh != image_data.imgh) 
  {
    result = InterpolateNearestNeighbor(image_data.data, image_data.imgw, image_data.imgh, imgw, imgh);
  }
  else 
  {
    result = image_data.data;
  }
  
  /// Save a copy of what FlowSculpt will actually search for
  std::stringstream filename;
  filename << "target_" << imgw << "_" << imgh << ".png";
  SaveImage(filename.str(), result, imgw, imgh, true);

  return result;
}

void SaveImage(std::string filename, CrossSection cs, int imgw,
    int imgh, bool half_pillars) 
{
  int r, c, idx;
  int half_height = 0;
  
  std::vector<int> img_data;
  half_height = imgh / 2;

  //
  // The following mess packs the outlet cross section into a row-major array where
  // index 0 (location 0,0) corresponds to the top left pixel in the image. Each value
  // in the cross section is interpreted as a color: 0 = background; 1 = foreground.
  //
  // In addition, this takes care of mirroring the cross section accross the horizontal
  // axis for the case that half pillars are disabled.
  //

  for (r = 0; r < imgh; r++) 
  {
    for (c = 0; c < imgw; c++) 
    {
      if (half_pillars) 
      {
        idx = (imgh - r - 1) * imgw + c;
      }
      else 
      {
        if (r < half_height) 
        {
          idx = r * imgw + c;
        }
        else 
        {
          idx = (imgh - r - 1) * imgw + c;
        }
      }

      img_data.push_back(material_to_color((int)cs[idx]));
    }
  }

  stbi_write_png(filename.c_str(), imgw, imgh, 4, img_data.data(), imgw * sizeof(int));
}

void SaveResult(std::string filename, GA::Configuration * config, FsChromosome& fsch) 
{
  int r, c, idx;
  int imgw = config->GetSetting("imgw").AsInt;
  int imgh = config->GetSetting("imgh").AsInt;

  int imgw_load = config->GetSetting("load_imgw").AsInt;
  int imgh_load = imgw_load * (double(imgh)/double(imgw));

  int half_height = 0;
  bool half_pillars = config->GetSetting("half_pillars").AsBool;
  CrossSection cs = std::move(ForwardModel(config, fsch.GetInlet(), fsch.GetPillars(), 
    imgh_load, imgw_load));
  
  std::vector<int> img_data;
  half_height = imgh_load / 2;

  //
  // The following mess packs the outlet cross section into a row-major array where
  // index 0 (location 0,0) corresponds to the top left pixel in the image. Each value
  // in the cross section is interpreted as a color: 0 = background; 1 = foreground.
  //
  // In addition, this takes care of mirroring the cross section accross the horizontal
  // axis for the case that half pillars are disabled.
  //

  for (r = 0; r < imgh_load; r++) 
  {
    for (c = 0; c < imgw_load; c++) 
    {
      if (half_pillars) 
      {
        idx = (imgh_load - r - 1) * imgw_load + c;
      }
      else 
      {
        if (r < half_height) 
        {
          idx = r * imgw_load + c;
        }
        else 
        {
          idx = (imgh_load - r - 1) * imgw_load + c;
        }
      }

      img_data.push_back(material_to_color((int)cs[idx]));
    }
  }

  stbi_write_png(filename.c_str(), imgw_load, imgh_load, 4, img_data.data(), imgw_load * sizeof(int));
}


