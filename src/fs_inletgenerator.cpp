#include "fs_flowsculptga.h"

#include <iostream>
#include <vector>

CrossSection FormInlet(const InletDesign& id, int imgw, int imgh) 
{

    //
    // Inlet Design:
    //
    // id.first = [a, b, c, d]
    // id.second = [i, j, k, l, m]
    //
    //         0.0      a      a+b    a+b+c  a+b+c+d           1.0
    // Widths : |<- a ->|<- b ->|<- c ->|<- d ->|<- 1-a-b-c-d ->|
    // material:     i       j       k       l           m
    // Index  :     0       1       2       3           4
    //
//  InletDesign id;
//  id.first = {0.2, 0.1, 0.1, 0.1};
//  id.second = {1, 0, 0, 0, 0};

    CrossSection cs;
  
    for (int r = 0; r < imgh; r++) 
    {
        int material_index = 0;
        double threshold = id.first[0];

        for (int c = 0; c < imgw; c++) 
        {
            if (((double) c) / ((double) imgw) > threshold) 
            {
                if (material_index + 1 < id.first.size()) 
                {
                    threshold += id.first[material_index + 1];
                }
                else 
                {
                    threshold = 1.0;
                }

                material_index++;
            }
            
            cs.push_back((double) id.second[material_index]);
        }
    }

    //
    // Set intlet index 0 to 0 for no-slip boundary conditions
    //

    cs[0] = 0;

    return cs;
}

CrossSection InletGenerator::GetInlet(int imgw, int imgh, const InletDesign& inseq) 
{
    return FormInlet(inseq, imgw, imgh);
}
