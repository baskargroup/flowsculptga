//
//  fs_forward_model.cpp
//  flowsculptga
//
//  Created by Dan Stoecklein on 1/25/19.
//
#include "fs_forward_model.h"

CrossSection ForwardModel(GA::Configuration * config,
    const InletDesign& inlet,
    const std::vector<RealPillar>& seq,
    int imgh,
    int imgw) 
{

  /// Set up preliminary variables
  InletGenerator& inlet_generator = * (InletGenerator *) config->GetSetting("inlet_generator").AsPointer;
  MapCache& map_cache = * (MapCache *) config->GetSetting("MapCache").AsPointer;
  bool half_pillars = config->GetSetting("half_pillars").AsBool;

  // Need the full-resolution flow image height to recall index maps
  int full_imgh = imgh;

  // If using half pillars, computation will use only lower-half of flow image
  if (!half_pillars) 
  {
    imgh = imgh / 2;
  }

  /// Forward model
  // The flow image will alternate between outlet and temp, sampling from each
  // other for even or odd indices of pillars to avoid memory swaps
  CrossSection outlet = inlet_generator.GetInlet(imgw, imgh, inlet);
  CrossSection temp(imgw * imgh);

  for (unsigned int i = 0; i < seq.size(); i++) 
  {
    // Real-valued pillar diameter, location, and height are converted to the
    // index map's index
    int pillar_index = seq[i].Interpret();
    const IndexMap& m = map_cache.GetMap(imgw, full_imgh, pillar_index);

    if (i % 2) 
    {
      // Odd pillar, sample from temp
      for (int j = 0; j < imgw * imgh; j++) 
      {
        outlet[j] = temp[m[j]];
      }
    } 
    else 
    {
      // Even pillar, sample from outlet
      for (int j = 0; j < imgw * imgh; j++) 
      {
        temp[j] = outlet[m[j]];
      }
    }
  }

  // Return whichever vector holds the last-updated flow shape
  if (seq.size() % 2) 
  {
    return temp;
  } 
  else 
  {
    return outlet;
  }

  /// Old approach:
  // for (unsigned int i = 0; i < seq.size(); i++) 
  // {
  //   int pillar_idx = seq[i].Interpret();
  //   const IndexMap& m = map_cache.GetMap(imgw, full_imgh, pillar_idx);
  //   for (int j = 0; j < imgw * imgh; j++) 
  //   {
  //     temp[j] = outlet[m[j]];
  //   }
  //   outlet = temp;
  // }
  // return outlet;
}
