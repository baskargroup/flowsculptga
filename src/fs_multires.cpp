#include "fs_flowsculptga.h"

using namespace GA;

void StepCallback(Configuration& config) {
    int curgen = config.GetSetting("GenerationNum").AsInt;

    if (curgen == 50) {
        config.SetSetting("imgw", 800);
        config.SetSetting("imgh", 200);
    }
}
