#include "fs_fft.h"
#include "fs_imagecache.h"
#include <vector>

void FFTPlanManager::SetFFTPlanManager(int num_threads) {
  plan_list_.resize(num_threads);
    for (int i = 0; i < num_threads; i++) {
        plan_list_[i].imgw = 0;
        plan_list_[i].imgh = 0;
        plan_list_[i].in = nullptr;
        plan_list_[i].out = nullptr;
    }
}

CrossSection FFTPlanManager::RunPlan(int guid, int imgw, int imgh, CrossSection& outlet) {

    if (plan_list_[guid].imgw != imgw || plan_list_[guid].imgh != imgh) {
        std::unique_lock<std::mutex> lock(fftw_mutex_);
        if (plan_list_[guid].in) delete[] plan_list_[guid].in;
        if (plan_list_[guid].out) fftw_free(plan_list_[guid].out);

        // imgw == 0 indicates the plan hasn't been setup yet at all.
        if (plan_list_[guid].imgw != 0) fftw_destroy_plan(plan_list_[guid].p);

        plan_list_[guid].in = new double[imgw * imgh];
        plan_list_[guid].out = (fftw_complex *) fftw_malloc(imgh * (imgw/2 + 1) * sizeof(fftw_complex));
        plan_list_[guid].p = fftw_plan_dft_r2c_2d(imgh, imgw, plan_list_[guid].in, plan_list_[guid].out, FFTW_ESTIMATE);

        plan_list_[guid].imgw = imgw;
        plan_list_[guid].imgh = imgh;
        lock.unlock();
    }

    fftw_execute_dft_r2c(plan_list_[guid].p, outlet.data(), plan_list_[guid].out);
    
    CrossSection outlet_ft;

    for (int i = 0; i < imgh * (imgw/2 + 1); i++) {
        outlet_ft.push_back(mag_squared(plan_list_[guid].out[i]));
    }

    return outlet_ft;
}

FFTPlanManager::~FFTPlanManager() {
    for (int i = 0; i < plan_list_.size(); i++) {
        if (plan_list_[i].in) delete[] plan_list_[i].in;
        if (plan_list_[i].out) fftw_free(plan_list_[i].out);
        if (plan_list_[i].imgw != 0) fftw_destroy_plan(plan_list_[i].p);
    }
}
