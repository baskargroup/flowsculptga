#include "fs_flowsculptga.h"
#include "fs_fitness.h"

#include <algorithm>
#include <sstream>
#include <string>

//#define DEBUG

FitnessFunctionEntry FitnessFunctionList[] = {
  REGISTER_FITNESS_FUNCTION(PMRFitness),
  REGISTER_FITNESS_FUNCTION(OCV_L2Norm),
  REGISTER_FITNESS_FUNCTION(Corr2Fitness),
  REGISTER_FITNESS_FUNCTION(Corr2MaskFitness),
  REGISTER_FITNESS_FUNCTION(FFTCorr2Fitness),
  /// TODO: Also register your fitness function here:
  // REGISTER_FITNESS_FUNCTION(MyAwesomeFitnessFunction)
  
  { (char *)"", 0 } // N.B. This is used to terminate the list so we can determine the size at runtime.
};

using namespace GA;

template<typename T>
T GetValueSafe(std::unordered_map<std::string, std::string>& settings,
               std::string name, std::function<T(const std::string&)> converter,
               bool optional, T default_value) {
  T value;
  try
  {
    // TODO: This whole function could probably be revamped but it works well enough for now.
    if (settings.count(name) == 0)
    {
      throw std::runtime_error("Key not found");
    }
    
    value = converter(settings[name]);
    
#ifdef DEBUG
    std::cerr << "Variable \"" << name << "\" = \"" << value << "\"" << std::endl;
#endif
    
  }
  catch(std::exception& e)
  {
    if (optional)
    {
#ifdef DEBUG
      std::cerr << "Warning: \"" << name << "\" not specified, defaulting to \"" << default_value << "\"." << std::endl;
#endif
      value = default_value;
    }
    else
    {
      std::cerr << "Failed to retrieve setting for \"" << name << "\" (" << e.what() << ")" << std::endl;
      exit(-1);
    }
  }
  return value;
}

std::string GetStringValue(std::unordered_map<std::string, std::string>& settings, std::string name, bool optional, std::string default_value) 
{
  try
  {
    
#ifdef DEBUG
    std::cerr << "Variable \"" << name << "\" = \"" << settings[name] << "\"" << std::endl;
#endif
    
    return settings[name];
  }
  catch(std::exception& e)
  {
    if (optional)
    {
#ifdef DEBUG
      std::cerr << "Warning: \"" << name << "\" not specified, defaulting to \"" << default_value << "\"." << std::endl;
#endif
      return default_value;
    }
    else
    {
      std::cerr << "Failed to retrieve setting for \"" << name << "\" (" << e.what() << ")" << std::endl;
      exit(-1);
    }
  }
}

void LoadConfigFile(const char * filename, FsSettings& settings) 
{
  std::ifstream ifs(filename);
  std::string line;
  
  while (std::getline(ifs, line))
  {
    line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
    if (line.length() == 0 || line[0] == '#') {
      continue;
    }
    
    std::istringstream iss(line);
    std::string key, value;
    
    std::getline(iss, key, ':');
    std::getline(iss, value);
    
    settings[key] = value;
  }
}

InletDesign ParseInletDesign(const std::string& widths, const std::string& material) 
{
  InletDesign id;
  SplitLineDouble(widths, id.first, ',');
  SplitLineInt(material, id.second, ',');
  
  return id;
}

std::vector<int> ParsePillarSeq(const std::string& pillar_seq_str) {
  std::vector<int> pillar_seq;
  SplitLineInt(pillar_seq_str, pillar_seq, ',');
  return pillar_seq;
}

auto intConverter = [] (const std::string& str) 
{
  return std::stoi(str.c_str());
};

auto boolConverter = [] (const std::string& str) 
{
  if ((str == "true") || (str == "yes")) return true;
  return false;
};

auto doubleConverter = [] (const std::string& str) 
{
  return std::atof(str.c_str());
};

void SetBaseConfiguration(FsConfig& config, FsSettings& settings,
                          FsPathData& path_data)
{
  // GA settings
  config.SetSetting("PopulationSize", GetValueSafe<int>(settings, "PopulationSize", intConverter, true, 100));
  config.SetSetting("MaxGenerations", GetValueSafe<int>(settings, "MaxGenerations", intConverter, true, 1000));
  config.SetSetting("MutationRate", GetValueSafe<double>(settings, "MutationRate", doubleConverter, true, 0.2));
  config.SetSetting("NumThreads", GetValueSafe<int>(settings, "numpar", intConverter, true, 1));
  
  /// Flow sculpting settings
  config.SetSetting("min_channel_width", GetValueSafe<double>(settings, "min_channel_width", doubleConverter, true, 0.05));
  config.SetSetting("imgw", GetValueSafe<int>(settings, "imgw", intConverter, true, 800));
  config.SetSetting("imgh", GetValueSafe<int>(settings, "imgh", intConverter, true, 200));
  config.SetSetting("renum", GetValueSafe<int>(settings, "renum", intConverter, true, 20));
  config.SetSetting("num_inlets", GetValueSafe<int>(settings, "num_inlets", intConverter, true, 5));
  config.SetSetting("half_pillars", GetValueSafe<bool>(settings, "half_pillars", boolConverter, true, false));
  config.SetSetting("fixed_inlet", GetValueSafe<bool>(settings, "fixed_inlet", boolConverter, true, false));
  config.SetSetting("uniform_pillar_width", GetValueSafe<bool>(settings, "uniform_pillar_width", boolConverter, true, false));
  
  /// Pillar sequence limits and number of GA repetitions
  config.SetSetting("minpillars", GetValueSafe<int>(settings, "minpillars", intConverter, true, 5));
  config.SetSetting("maxpillars", GetValueSafe<int>(settings, "maxpillars", intConverter, true, 10));
  config.SetSetting("reps", GetValueSafe<int>(settings, "reps", intConverter, true, 1));
  
  
  /// FlowSculpt output settings
  config.SetSetting("per_pillar_results", GetValueSafe<bool>(settings, "per_pillar_results", boolConverter, true, false));
  config.SetSetting("print_config", GetValueSafe<bool>(settings, "print_config", boolConverter, true, false));
  config.SetSetting("save_repetition_data", GetValueSafe<bool>(settings, "save_repetition_data", boolConverter, true, false));
  config.SetSetting("save_repetition_images", GetValueSafe<bool>(settings, "save_repetition_images", boolConverter, true, false));
  
  /// Hard-coded advection map resolution
  config.SetSetting("load_imgw", 800);
  
  /// Filesystem / path data for index maps, target flow iamge, results and data
  path_data.map_folder = GetStringValue(settings, "mbundle_folder", false, "");
  path_data.target_file = GetStringValue(settings, "target", false, "");
  path_data.result_dir = GetStringValue(settings, "result_directory", false, "");
  path_data.result_suffix = GetStringValue(settings, "result_suffix", false, "");
  path_data.gen_file_prefix = GetStringValue(settings, "gen_file_prefix", false, "");
  path_data.fitness_file_prefix = GetStringValue(settings, "repetition_file_prefix", true, "repetition_data");
  
  /// Seed the population with a pillar sequence and inlet
  config.SetSetting("use_seed_chromosome", GetValueSafe<bool>(settings, "use_seed_chromosome", boolConverter, true, false));
}

/// Function to parse which fitness function has been chosen, 
/// and set up or alter FlowSculpt parameters as necessary
void ParseFitnessFunction(const std::string fitness_function, FsConfig& config, FsSettings& settings) 
{
  FitnessFunction ff = nullptr;
  for (int i = 0; FitnessFunctionList[i].func; i++)
  {
    if (fitness_function == FitnessFunctionList[i].name)
    {
      ff = FitnessFunctionList[i].func;
      break;
    }
  }
  
  if (fitness_function == "FFTCorr2Fitness")
  {
    /// Need to convert image sizes and everything down to some base-2 design
    /// to prevent periodicity
    /// TODO: This
    
    /// Compute new base-2 image size
    int imgw_tmp = config.GetSetting("imgw").AsInt;
    int imgh_tmp = config.GetSetting("imgh").AsInt;
    double ar = double(imgh_tmp) / double(imgw_tmp);
    
    /// 512 is the base-2 resolution, might increase this value
    /// in the future.
    const int imgw_fftw = 512;
    
    config.SetSetting("imgw", imgw_fftw);
    config.SetSetting("imgh", int(imgw_fftw * ar));
  }
  else if ((config.GetSetting("imgh").AsInt == 400) || (config.GetSetting("imgh").AsInt == 800))
  {
    int imgw_tmp = config.GetSetting("imgw").AsInt;
    int imgh_tmp = config.GetSetting("imgh").AsInt;
    double ar = double(imgh_tmp) / double(imgw_tmp);
    const int imgw_fftw = 600;
    
    config.SetSetting("imgw", imgw_fftw);
    config.SetSetting("imgh", int(imgw_fftw * ar));
    
  }
  
  if (fitness_function == "Corr2MaskFitness")
  {
    config.SetSetting("mask", GetValueSafe<double>(settings, "mask", doubleConverter, false, 0.0));
  }
  
  if (ff == nullptr)
  {
    std::cerr << "Warning: Desired fitness function not found. Defaulting to Corr2Fitness" << std::endl;
    ff = Corr2Fitness;
  }
  
  config.SetSetting("fitnessFunction", (void *) ff);
  
}

int GetBaseConfiguration(
                         const char *filename,
                         FsPathData &path_data,
                         FsConfig &config,
                         InletDesign &inlet_design,
                         ImageCache &image_cache,
                         std::vector<RealPillar> &seed_pillar_seq
                         )
{
  FsSettings settings;
  
  LoadConfigFile(filename, settings);
  
  /// Load basic settings
  SetBaseConfiguration(config, settings, path_data);
  std::string fitness_function = GetStringValue(settings, "fitness_function", false, "");
  
  /// Parse settings for various runtime conditions and options
  ParseFitnessFunction(fitness_function, config, settings);
  
  if (path_data.gen_file_prefix != "")
  {
    config.SetSetting("StoreGenerationFitness", true);
  }
  else
  {
    config.SetSetting("StoreGenerationFitness", false);
  }
  
  // Parse inlet conditions
  if ((config.GetSetting("fixed_inlet").AsBool) ||
      (settings.count("channel_widths") > 0) ||
      (settings.count("channel_material") > 0))
  {
    std::string widths = GetStringValue(settings, "channel_widths", false, "");
    std::string material = GetStringValue(settings, "channel_material", false, "");
    inlet_design = ParseInletDesign(widths, material);
    if (config.GetSetting("fixed_inlet").AsBool == false)
    {
      config.SetSetting("fixed_inlet", true);
    }
  }
  
  // Check RNG seed setting, if any
  if (settings.count("rng_seed") > 0) {
    config.SetSetting("static_rng_seed", true);
    config.SetSetting("rng_seed", GetValueSafe<int>(settings, "rng_seed", intConverter, false, 0));
  }
  else
  {
    config.SetSetting("static_rng_seed", false);
  }
  
  // Check if high-resoution (flow image) search requested
  config.SetSetting("high_res", GetValueSafe<bool>(settings, "high_res", boolConverter, true, false));
  if (config.GetSetting("high_res").AsBool)
  {
    int imgw_tmp = config.GetSetting("imgw").AsInt;
    int imgh_tmp = config.GetSetting("imgh").AsInt;
    config.SetSetting("imgw", imgw_tmp*2);
    config.SetSetting("imgh", imgh_tmp*2);
  }
  
  /// Set up image_cache and handle num_material
  if (settings.count("num_material") > 0)
  {
    config.SetSetting("num_material", GetValueSafe<int>(settings, "num_material", intConverter, false, 1));
    if (!image_cache.SetImageCache(path_data.target_file, config.GetSetting("half_pillars").AsBool, &config))
    {
      return 2;
    }
  }
  else
  {
    if (!image_cache.SetImageCache(path_data.target_file, config.GetSetting("half_pillars").AsBool, &config))
    {
      return 2;
    }
    config.SetSetting("num_material", image_cache.NumMaterial());
  }
  
  /// Set up target image to finalize image_cache.NumMaterial()
  int imgw = config.GetSetting("imgw").AsInt;
  int imgh = config.GetSetting("imgh").AsInt;
  image_cache.GetTarget(imgw, imgh);
  if ((image_cache.NumMaterial() > 1) && (fitness_function == "FFTCorr2Fitness"))
  {
    return 1;
  }
  
  if (config.GetSetting("use_seed_chromosome").AsBool)
  {
    
    std::string pillar_seq_str = GetStringValue(settings, "seed_pillar_sequence", false, "");
    std::vector<int> pillar_seq = ParsePillarSeq(pillar_seq_str);
    
    if (pillar_seq.size() == 0)
    {
      std::cerr << "No seed_pillar_sequence specified, not using seeded chromosome" << std::endl;
      config.SetSetting("use_seed_chromosome", false);
      return 0;
    }
    
    for (int i = 0; i < pillar_seq.size(); i++)
    {
      seed_pillar_seq.push_back(RealPillar(pillar_seq[i]));
    }
  }
  
  return 0;
}

// Set GA framework settings
void SetGASettings(FsConfig& config) 
{  
  config.SetSetting("SelectionMethod", (void *) Selection::TournamentSelect);
  config.SetSetting("RankedFitness", true);
  
  //
  // N.B. We only need to specify ParallelEvaluate if we have more than one
  //      thread to run on.
  //
  
  if (config.GetSetting("NumThreads").AsInt > 1)
  {
    config.SetSetting("EvaluationMethod", (void *) Evaluation::ParallelEvaluate);
  }
  
  if (config.GetSetting("use_seed_chromosome").AsBool)
  {
    config.SetSetting("SeedChromosome", true);
  }
  //    config.SetSetting("StepCallback", (void *) StepCallback);
}
