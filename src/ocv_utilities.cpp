//
//  ocv_utilities.cpp
//  flowsculptga
//
//  Created by Dan Stoecklein on 5/8/19.
//

#include "ocv_utilities.h"

CrossSectionMAT ConvertToMAT(const CrossSection& outlet, const int& imgw, const int& imgh) {
  uchar pv[outlet.size()];

  for (unsigned int i = 0; i < outlet.size(); i++) {
    pv[i] = (uchar) outlet.at(i) * 255;
  }
  
  cv::Size s;
  s.width = imgw;
  s.height = imgh;
  CrossSectionMAT outlet_MAT(s, CV_8UC1);
  memcpy(outlet_MAT.data, &pv, outlet.size());
  return outlet_MAT;
}
