#include "fs_mapcache.h"

IndexMap AdvectionToIndex(const AdvectionMap& advection_map,
    int Ny, int Nz, const std::vector<int>& diffusion_zone) 
{
  IndexMap index_map(Ny * Nz);
  const float scaled_dx = float(Ny);

  int idx = 0;
  float dy, dz;
  int y1, z1, y2, z2;

  for (int cell = 0; cell < Ny * Nz; cell++) 
  {
    // Find cell coordinates
    y1 = cell % Ny;
    z1 = cell / Ny;

    // Get displacement vector for this cell
    dy = round(advection_map[cell] * scaled_dx);
    dz = round(advection_map[cell + Ny * Nz] * scaled_dx);

    // Resultant coordinates
    y2 = y1 + dy;
    z2 = z1 + dz;

    // Enforce boundary condition
    if (y2 < 0) y2 = 0;
    if (z2 < 0) z2 = 0;
    if (y2 > Ny) y2 = Ny-1;
    if (z2 > Nz) z2 = Nz-1;

    // Cell number of resultant
    index_map[idx++] = (int)(z2 * Ny + y2);
  }

  // Diffusive blur thresholding
  if (diffusion_zone.size() > 0) 
  {
    for (auto index : diffusion_zone) 
    {
      index_map[index] = 0;
    }
  }

  return index_map;
}

std::vector<int> ComputeNewDiffusionZone(const std::vector<int> diffusion_zone_0, int imgh, int imgw, int load_imgh, int load_imgw)
{
  /// Fill out diffusion regions in temporary vector of load dimensions
  std::vector<int> temp_flow_image_load(load_imgh * load_imgw);
  for (int index : diffusion_zone_0) 
  {
    temp_flow_image_load[index] = 1; // Indicate diffusion region with 1
  }
  
  /// Interpolate onto a temporary vector of new imgh x imgw dimensions
  std::vector<int> temp_flow_image = InterpolateNearestNeighbor(temp_flow_image_load, load_imgw, load_imgh, imgw, imgh);
  
  /// Find new diffusion indices
  std::vector<int> diffusion_zone_1;
  for (int index=0; index<temp_flow_image.size(); index++) 
  {
    if (temp_flow_image[index]) 
    {
      diffusion_zone_1.push_back(index);
    }
  }
  
  return diffusion_zone_1;
}

std::vector<int> LoadDiffusionZone(std::string map_folder, int imgh, int imgw, int load_imgw, int load_imgh) 
{
  double ar_diffusion = double(imgh) / double(imgw);
  
  std::stringstream zone_fname;
  zone_fname << map_folder << "/diffusion_zone_h" << std::setfill('0')
    << std::setw(3) << int(ar_diffusion * 100) << "_" << load_imgw << ".bin.gz";
  
  zstr::ifstream ifs(zone_fname.str(), std::ios::binary | std::ios::in);

  if (ifs.good()) 
  {
    int file_length;
    ifs.read((char *) &file_length, sizeof(int));
    std::vector<int> diffusion_zone(file_length);
    ifs.read((char *) diffusion_zone.data(), file_length * sizeof(int));
    
    /// Check if image resolution does not match advection data resolution
    /// If needed, compute new diffusion data
    if (load_imgw * load_imgh != imgw * imgh) 
    {
      return ComputeNewDiffusionZone(diffusion_zone, imgh, imgw, load_imgh, load_imgw);
    } 
    else 
    {
      return diffusion_zone;
    }
  } 
  else 
  {
    std::cerr << "Could not load diffusive zone." << std::endl;
    std::vector<int> temp;
    return temp;
  }
}

const IndexMap &MapCache::GetMap(int imgw, int imgh, int idx) 
{
  std::unique_lock<std::mutex> lock(cache_lock_);
  auto reskey = std::make_pair(imgw, imgh);
  if (map_table_.count(reskey) == 0) 
  {
    LoadMaps(imgw, imgh);
  }
  
  return map_table_[reskey][idx];
}

void MapCache::LoadBase80Maps(zstr::ifstream* ifs, const int load_imgh, const int imgw, const int imgh, const double ar, const std::vector<int>& diffusion_zone, std::vector<IndexMap> &maps) 
{
  /// Hard-coded values for advection map data
  const int num_maps = 80;
  
  int map_load_size = load_imgw_ * load_imgh * 2;
  
  /// Check to see if FlowSculpt is using advection data
  /// dimensions
  bool interpolate_map = false;
  if (load_imgw_ != imgw || load_imgh != imgh) 
  {
    interpolate_map = true;
  }
  
  for (int i = 0; i < num_maps; i++) 
  {
    assert(ifs->good());
    
    AdvectionMap am(map_load_size);
    ifs->read((char *)am.data(), map_load_size * sizeof(float));
    
    if (!interpolate_map) 
    {
      /// Use native advection map resolution
      IndexMap tm = AdvectionToIndex(am, imgw, imgh, diffusion_zone);
      maps.push_back(tm);
    } 
    else 
    {
      /// Interpolate new map resolution
      Q_Boundary q_bound = {0.0, 1.0, 0.0, ar};
      AdvectionMap am_interpolate = AdvectionInterpolate(am, load_imgw_, load_imgh, imgw, imgh, q_bound);
      IndexMap tm = AdvectionToIndex(am_interpolate, imgw, imgh, diffusion_zone);
      maps.push_back(tm);
    }
  }
}

void MapCache::LoadMaps(int imgw, int imgh) 
{
  // Size for maps to be loaded (nearly always: imgw=800)
  const double ar = double(imgh) / double(imgw);
  const int load_imgh = load_imgw_ * ar;

  std::vector<IndexMap> maps;
  
  /// Kep for image resolution
  auto reskey = std::make_pair(imgw, imgh);

  /// Diffusive blur cutoff data
  std::vector<int> diffusion_zone = LoadDiffusionZone(map_folder_, imgh, imgw, load_imgw_, load_imgh);

  // Load either entire full-pillar dataset, or the base-80
  std::stringstream ss;
  if (base_80_) 
  {
    ss << map_folder_ << "/adata_" << load_imgw_ << "x" << load_imgh << "_re" << re_num_ << ".bin.gz";
  } 
  else 
  {
    ss << map_folder_ << "/data_h" << std::setprecision(2) << ar << ".bin.gz";
  }
  zstr::ifstream ifs(ss.str(), std::ios::binary | std::ios::in);

  if (ifs.good()) 
  {
    LoadBase80Maps(&ifs, load_imgh, imgw, imgh, ar, diffusion_zone, maps);
    map_table_[reskey] = maps;
  } 
  else 
  {
    std::cerr << "Error. " << ss.str() << " could not be opened." << std::endl;
  }
}
