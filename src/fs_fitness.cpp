
#include "ocv_utilities.h"
#include "fs_flowsculptga.h"
#include "fs_fitness.h"

using namespace cv;
using namespace GA;

double PMR(const double * l1, const double * l2, int length) 
{
  double same = 0;
  for (int i = 0; i<length; i++) 
  {
    if (l1[i] == l2[i]) same++;
  }
  return (same / static_cast<double>(length));
}

double OCV_L2Norm(const cv::Mat A, const cv::Mat B) 
{
  if ( A.rows > 0 && A.rows == B.rows && A.cols > 0 && A.cols == B.cols ) 
  {
    // Calculate the L2 relative error between images.
    double errorL2 = cv::norm(A, B, CV_L2);
    // Convert to a reasonable scale, since L2 error is summed across all pixels of the image.
    double similarity = (1 - errorL2 / (double)( A.rows * A.cols ));
    return similarity;
  }
  else 
  {
    //Images have a different size
    return 0.0;  // Return a bad value
  }
}

double PMR(const double * l1, const double * l2, int material, int length) 
{
  double same = 0;
  for (int i = 0; i<length; i++) 
  {
    if (l1[i] == l2[i]) same++;
  }
  return (same / static_cast<double>(length));
}

double Corr2Squared(const double * l1, const double * l2, int length) 
{
  double avg_I1 = 0.0;
  double avg_I2 = 0.0;

  for (int i = 0; i < length; i++) 
  {
    avg_I1 += l1[i];
    avg_I2 += l2[i];
  }

  avg_I2 /= (double) length;
  avg_I1 /= (double) length;

  double sum = 0.0;
  double var21 = 0.0;
  double var22 = 0.0;

  for (int i = 0; i < length; i++) 
  {
    double l1i = l1[i];
    double l2i = l2[i];

    sum += (l1i - avg_I1) * (l2i - avg_I2);
    var21 += (l1i - avg_I1) * (l1i - avg_I1);
    var22 += (l2i - avg_I2) * (l2i - avg_I2);
  }

  //
  // N.B. Technically this should never happen since the GA is constrained to
  //      never produce zero inlets but in a multi-material search, some material
  //      might randomly not show up, causing zero-variance.
  //

  if (var21 * var22 == 0) 
  {
    return 0.0;
  }

  return (sum * sum) / (var21 * var22);
}


double Corr2(const double * l1, const double * l2, int material, int length) 
{

  double avg_I1 = 0.0;
  double avg_I2 = 0.0;

  for (int i = 0; i < length; i++) 
  {
    avg_I1 += (l1[i] == material? 1.0 : 0.0);
    avg_I2 += (l2[i] == material? 1.0 : 0.0);
  }

  avg_I2 /= (double) length;
  avg_I1 /= (double) length;

  double sum = 0.0;
  double var21 = 0.0;
  double var22 = 0.0;

  for (int i = 0; i < length; i++) 
  {
    double l1i = (l1[i] == material? 1.0 : 0.0);
    double l2i = (l2[i] == material? 1.0 : 0.0);

    sum += (l1i - avg_I1) * (l2i - avg_I2);
    var21 += (l1i - avg_I1) * (l1i - avg_I1);
    var22 += (l2i - avg_I2) * (l2i - avg_I2);
  }

  //
  // N.B. Technically this should never happen since the GA is constrained to
  //      never produce zero inlets but in a multi-material search, some material
  //      might randomly not show up, causing zero-variance.
  //

  if (var21 * var22 == 0) 
  {
    return 0.0;
  }

  return (sum) / sqrt(var21 * var22);
}


inline bool CheckMask(int c, int imgw, double mask) 
{
  double column = ((double) c) / ((double) imgw);
  return (column > mask) && (column < (1.0-mask));
}

double Corr2(const double * l1, const double * l2, int material,
    int imgw, int imgh, double mask) 
{
  double avg_I1 = 0.0;
  double avg_I2 = 0.0;
  int length = imgw * imgh;
  int mask_size = (int)(imgw*(1-2*mask))*imgh;

  for (int i = 0; i < length; i++) 
  {
    if (CheckMask(i % imgw, imgw, mask)) 
    {
      avg_I1 += (l1[i] == material ? 1.0 : 0.0);
      avg_I2 += (l2[i] == material ? 1.0 : 0.0);
    }
  }

  avg_I2 /= (double) mask_size;
  avg_I1 /= (double) mask_size;

  double sum = 0.0;
  double var21 = 0.0;
  double var22 = 0.0;

  for (int i = 0; i < length; i++) 
  {
    if (CheckMask(i % imgw, imgw, mask)) 
    {
      double l1i = (l1[i] == material? 1.0 : 0.0);
      double l2i = (l2[i] == material? 1.0 : 0.0);

      sum += (l1i - avg_I1) * (l2i - avg_I2);
      var21 += (l1i - avg_I1) * (l1i - avg_I1);
      var22 += (l2i - avg_I2) * (l2i - avg_I2);
    }
  }

  //
  // N.B. Technically this should never happen since the GA is constrained to
  //      never produce zero inlets but in a multi-material search, some material
  //      might randomly not show up, causing zero-variance.
  //

  if (var21 * var22 == 0) 
  {
    return 0.0;
  }

  return (sum) / sqrt(var21 * var22);
}

FITNESS_FUNCTION(Corr2Fitness) 
{
  /// Retrieve image dimensions
  int imgw = config.GetSetting("imgw").AsInt;
  int imgh = config.GetSetting("imgh").AsInt;

  ImageCache& imc = * (ImageCache *) config.GetSetting("ImageCache").AsPointer;
  const CrossSection& target = imc.GetTarget(imgw, imgh);

  //
  // N.B. This is required to ensure fitness without half_pillars only takes
  //      the top half of the image into account. The forward model will only
  //      permute the top half of the image, leaving the rest garbage.
  //

  bool half_pillars = config.GetSetting("half_pillars").AsBool;

  if (!half_pillars) 
  {
    imgh = imgh / 2;
  }

  int num_material = config.GetSetting("num_material").AsInt;
  double fitness = 0.0;
  double weight = 1.0 / num_material;

  if (num_material == 1) 
  {
    // Only one material -- complementary/inverse images are equivalent,
    // Corr2Squared
    fitness = Corr2Squared(outlet.data(), target.data(), imgw * imgh);
  } 
  else
  {
    /// Multi-material -- specified material location is important
    for (int material = 1; material<=num_material; material++) 
    {
      fitness += Corr2(outlet.data(), target.data(), material, imgw*imgh);
    }
  }

  return weight * fitness;
}

FITNESS_FUNCTION(OCV_L2Norm) 
{
  /// Retrieve image dimensions
  int imgw = config.GetSetting("imgw").AsInt;
  int imgh = config.GetSetting("imgh").AsInt;

  /// Retrieve target image
  ImageCache& imc = * (ImageCache *) config.GetSetting("ImageCache").AsPointer;
  const CrossSectionMAT& target_MAT = imc.GetTargetOCV(imgw, imgh);

  CrossSectionMAT outlet_MAT = ConvertToMAT(outlet, imgw, imgh);

  //  cv::imshow("target", outlet_MAT);
  //  cv::imshow("outlet_skel", outlet_MAT_skel);
  //  cv::waitKey();

  //
  // N.B. This is required to ensure fitness without half_pillars only takes
  //      the top half of the image into account. The forward model will only
  //      permute the top half of the image, leaving the rest garbage.
  //

  bool half_pillars = config.GetSetting("half_pillars").AsBool;

  if (!half_pillars) 
  {
    imgh = imgh / 2;
  }

  //  int num_material = config.GetSetting("num_material").AsInt;
  double fitness = 0.0;

  fitness = OCV_L2Norm(outlet_MAT, target_MAT);

  return fitness;
}


FITNESS_FUNCTION(PMRFitness) 
{
  int imgw = config.GetSetting("imgw").AsInt;
  int imgh = config.GetSetting("imgh").AsInt;

  ImageCache& imc = * (ImageCache *) config.GetSetting("ImageCache").AsPointer;
  const CrossSection& target = imc.GetTarget(imgw, imgh);

  //
  // N.B. This is required to ensure fitness without half_pillars only takes
  //      the top half of the image into account. The forward model will only
  //      permute the top half of the image, leaving the rest garbage.
  //

  bool half_pillars = config.GetSetting("half_pillars").AsBool;

  if (!half_pillars) 
  {
    imgh = imgh / 2;
  }

  int num_material = config.GetSetting("num_material").AsInt;
  double fitness = 0.0;
  double weight = 1.0 / num_material;

  for (int material = 1; material<=num_material; material++) 
  {
    fitness += PMR(outlet.data(), target.data(), material, imgw*imgh);
  }

  return weight * fitness;
}

FITNESS_FUNCTION(Corr2MaskFitness) 
{
  /// Retrieve image dimensions
  int imgw = config.GetSetting("imgw").AsInt;
  int imgh = config.GetSetting("imgh").AsInt;
  double mask = config.GetSetting("mask").AsDouble;

  ImageCache& imc = * (ImageCache *) config.GetSetting("ImageCache").AsPointer;
  const CrossSection& target = imc.GetTarget(imgw, imgh);

  //
  // N.B. This is required to ensure fitness without half_pillars only takes
  //      the top half of the image into account. The forward model will only
  //      permute the top half of the image, leaving the rest garbage.
  //

  bool half_pillars = config.GetSetting("half_pillars").AsBool;

  if (!half_pillars) 
  {
    imgh = imgh / 2;
  }

  int num_material = config.GetSetting("num_material").AsInt;
  double fitness = 0.0;
  double weight = 1.0 / num_material;

  for (int material = 0; material < num_material; material++) 
  {
    fitness += Corr2(outlet.data(), target.data(), material + 1,
        imgw, imgh, mask);
  }

  return weight * fitness;
}

FITNESS_FUNCTION(FFTCorr2Fitness) 
{
  /// Retrieve target image
  ImageCache& imc = * (ImageCache *) config.GetSetting("ImageCache").AsPointer;
  FFTPlanManager& pm = * (FFTPlanManager *) config.GetSetting("FFTPlanManager").AsPointer;

  /// Retrieve image dimensions
  int imgw = config.GetSetting("imgw").AsInt;
  int imgh = config.GetSetting("imgh").AsInt;
  int guid = (int)(size_t) thread_data;

  const CrossSection& target_ft = imc.GetTargetFFT(imgw, imgh);


  //
  // N.B. This is required to ensure fitness without half_pillars only takes
  //      the top half of the image into account. The forward model will only
  //      permute the top half of the image, leaving the rest garbage.
  //

  bool half_pillars = config.GetSetting("half_pillars").AsBool;

  if (!half_pillars) 
  {
    imgh = imgh / 2;
  }

  /// fft(outlet) has to be computed with care to half_pillars or not;
  /// The outlet images generated by the forward model only have data
  /// base on these dimensions, so the conversion has to take this
  /// into account
  CrossSection outlet_ft = pm.RunPlan(guid, imgw, imgh, outlet);

  double result = Corr2Squared(outlet_ft.data(), target_ft.data(), imgh * (imgw/2 + 1));

  //  std::cerr << "result = " << result << std::endl;

  return result;
}

