//
//  fs_flowsculptga.cpp
//  flowsculptga
//
//  Created by Dan Stoecklein on 1/23/19.
//

#include "fs_flowsculptga.h"
#include "fs_error.h"
#include "fs_mutation.h"

using namespace GA;

void FlowSculptGa::GaSweep() 
{
  /// Specify RNG seed if in settings
  if (global_config_.GetSetting("static_rng_seed").AsBool)
  {
    std::cerr << "Resetting RNG seed" << std::endl;
    Random::Reset((unsigned int) global_config_.GetSetting("rng_seed").AsInt);
  }
  
  /// Ensure target flow shape has some materials
  if (global_config_.GetSetting("num_material").AsInt == 0)
  {
    std::cerr << "Fatal error: no materials detected. Cannot run GA." << std::endl;
    exit(-1);
  }
  
  std::cerr << "Starting FlowSculpt search." << std::endl;
  
  /// Start internal clock
  auto t_start = std::chrono::high_resolution_clock::now();
  auto t_previous = t_start;
  
  /// Sweep through pillar sequence length
  for (num_pillars_ = min_pillars_; num_pillars_ <= max_pillars_; num_pillars_++)
  {
    /// Repeat GA search
    for (current_rep_ = 0; current_rep_ < reps_; current_rep_++)
    {
      global_config_.SetSetting("numPillars", num_pillars_);
      
      /// Create GA solver
      Solver s(global_config_);
      
      s.Setup(popsize_, FsChromosomeCrossover, FsChromosomeAllocate, global_config_);
      
      /// Run GA
      bool print_top = false; // flag to print best result from each GA generation
      s.Run(print_top);
      
      bool del = true; // flag to delete current top chromosome (deleted unless a new top score is set)
      
      auto t_current = std::chrono::high_resolution_clock::now();
      auto runtime = std::chrono::duration_cast<std::chrono::seconds>(t_current - t_previous).count();
      t_previous = t_current;
      
      ga_counter_++;
      
      /// Update performance data
      ga_runtime_.push_back(runtime);
      ga_pillars_.push_back(num_pillars_);
      
      /// Update and output  progress
      UpdateProgress(&s, del);
      PrintUpdate(&s, num_pillars_, current_rep_);
    }
  }
  auto t_current = std::chrono::high_resolution_clock::now();
  auto total_runtime = std::chrono::duration_cast<std::chrono::milliseconds>(t_current - t_start).count();
  std::cerr << "Total runtime (ms): " << total_runtime << std::endl;
}

void FlowSculptGa::CheckTop(FsChromosome* current_top, int pillar_index, bool& del) 
{
  /// See if new score replaces overall top score
  if (global_top_ == nullptr || global_top_->GetScore() < current_top->GetScore())
  {
    global_top_ = current_top;
    del = false;
  }
  
  /// Set per-piller top score
  if ((per_pillar_top_[pillar_index] == nullptr) ||
      (per_pillar_top_[pillar_index]->GetScore() < current_top->GetScore()))
  {
    per_pillar_top_[pillar_index] = current_top;
    del = false;
  }
}

void FlowSculptGa::UpdateProgress(GA::Solver* s, bool& del) 
{
  int pillar_index = num_pillars_ - min_pillars_; /// 0-index for current pillar
  
  /// Update top-performing chromosome data
  FsChromosome * current_top = (FsChromosome *)s->PopTop();
  top_scores_[pillar_index].push_back(current_top->GetScore());
  num_gen_[pillar_index].push_back(s->GetCurGen());
  
  /// Compare top-performers to current-best
  CheckTop(current_top, pillar_index, del);
  
  /// Store generational data
  if (path_data_.gen_file_prefix != "")
  {
    std::vector<std::vector<double>> ga_gen_data = s->GetGenerationData();
    SaveGenerationData(ga_gen_data, num_pillars_, current_rep_);
  }
  
  /// Update per-pillar best
  if (global_config_.GetSetting("per_pillar_results").AsBool)
  {
    std::stringstream ss;
    ss << path_data_.result_dir << "/per_pillar_" << num_pillars_ << "_result";
    if (path_data_.result_suffix.size() > 0)
    {
      ss << "_" << path_data_.result_suffix << ".png";
    }
    else
    {
      ss << ".png";
    }
    
    SaveResult(ss.str(), &global_config_, *per_pillar_top_[pillar_index]);
  }
  
  /// Save repetition result, if desired
  if (global_config_.GetSetting("save_repetition_data").AsBool)
  {
    SaveRepetitionData(s, current_top);
  }
  
  if (del) delete current_top;
}


void FlowSculptGa::PrepareGa(const char* filename) 
{
  int error;
  error = GetBaseConfiguration(filename, path_data_, global_config_, inlet_design_, image_cache_, seed_pillar_seq_);
  FSCHKERR(error);
  
  SetGASettings(global_config_);
  
  global_config_.SetSetting("CreateNewThreadDataMethod", (void *) &create_thread_data_);
  
  if (global_config_.GetSetting("fixed_inlet").AsBool)
  {
    global_config_.SetSetting("inlet_design", (void *) &inlet_design_);
  }
  
  map_cache_.SetMapCache(path_data_.map_folder,
                         global_config_.GetSetting("half_pillars").AsBool,
                         global_config_.GetSetting("renum").AsInt);
  plan_manager_.SetFFTPlanManager(global_config_.GetSetting("NumThreads").AsInt);
  
  if (global_config_.GetSetting("num_material").AsInt == 0)
  {
    std::cerr << "Setting num_material to " <<
    image_cache_.NumMaterial() << std::endl;
    global_config_.SetSetting("num_material", image_cache_.NumMaterial());
  }
  
  global_config_.SetSetting("inlet_generator", (void *) &inlet_generator_);
  global_config_.SetSetting("MapCache", (void *) &map_cache_);
  global_config_.SetSetting("ImageCache", (void *) &image_cache_);
  global_config_.SetSetting("FFTPlanManager", (void *) &plan_manager_);
  if (global_config_.GetSetting("use_seed_chromosome").AsBool)
  {
    global_config_.SetSetting("seed_pillar_seq", (void *) &seed_pillar_seq_);
  }
  
  min_pillars_ = global_config_.GetSetting("minpillars").AsInt;
  max_pillars_ = global_config_.GetSetting("maxpillars").AsInt;
  reps_ = global_config_.GetSetting("reps").AsInt;
  popsize_ = global_config_.GetSetting("PopulationSize").AsInt;
  
  ga_counter_ = 0;
  num_pillars_ = (max_pillars_ - min_pillars_ + 1);
  num_GAs_ = (num_pillars_) * reps_;
  
  global_top_ = nullptr;
  
  per_pillar_top_.resize(num_pillars_);
  top_scores_.resize(num_pillars_);
  num_gen_.resize(num_pillars_);
  
  if (global_config_.GetSetting("use_seed_chromosome").AsBool)
  {
    //    ChromosomeAllocator allocator = (ChromosomeAllocator) global_config_.GetSetting("ChromosomeAllocator").AsPointer;
    //    seed_ch_ = allocator();
    //    seed_ch_->SetConfiguration(&global_config_);
    //    seed_ch_->Generate();
    //    seed_ch_->SetPillars(seed_pillar_seq_);
  }
  
  if (global_config_.GetSetting("print_config").AsBool) {
    std::cerr << "-------------------- Template Configuration --------------------" << std::endl;
    global_config_.PrintConfiguration(true);
    std::cerr << std::dec;
    std::cerr << "----------------------------------------------------------------" << std::endl;
  }
}

void FlowSculptGa::PrintUpdate(Solver* s, int num_pillars, int rep) 
{
  std::string time_log = ComputeRemainingTime(num_pillars, rep);
  std::cerr << std::setprecision(3) <<  double(ga_counter_)/double(num_GAs_)*100  <<
  "% complete. Top fitness: " << global_top_->GetScore() << ". " << time_log << std::endl;
}

void FlowSculptGa::SaveGenerationData(std::vector<std::vector<double>> ga_gen_data,
                                      int num_pillars, int rep)
{
  std::ofstream gen_file;
  std::stringstream ss;
  ss << path_data_.result_dir << "/" << path_data_.gen_file_prefix <<
  "_pillar_" << num_pillars << "_ga_" << rep << ".txt";
  
  gen_file.open(ss.str());
  for (int i=0; i<ga_gen_data.size();i++)
  {
    for (int j=0; j<popsize_; j++)
    {
      gen_file << std::setprecision(3) << ga_gen_data[i][j] << ",";
    }
    gen_file << "\n";
  }
  gen_file.close();
}

void FlowSculptGa::SaveRepetitionData(Solver *s, FsChromosome *current_top) 
{
  /// Save GA result data
  std::stringstream ss;
  ss << path_data_.result_dir << "/" << path_data_.fitness_file_prefix << "_" << path_data_.result_suffix << ".txt";
  
  std::ofstream fitness_file;
  
  if ((num_pillars_ == min_pillars_) && (current_rep_ == 0))
  {
    // Write first data to file
    fitness_file.open(ss.str());
    fitness_file << "# pillars fitness generations fitness_evals pillar_sequence\n";
  }
  else
  {
    // Open file to append data
    fitness_file.open(ss.str(), std::ios_base::app);
  }
  
  fitness_file << num_pillars_ << " "
  << std::setprecision(3) << current_top->GetScore() << " "
  << s->GetNumGenerations() << " "
  << s->GetNumGenerations() * global_config_.GetSetting("PopulationSize").AsInt << " "
  << current_top->GetPillarSeqIdx();
  fitness_file.close();
  
  
  /// Save flow shape image
  if (global_config_.GetSetting("save_repetition_images").AsBool)
  {
    std::stringstream ss_rep;
    ss_rep << path_data_.result_dir << "/" << num_pillars_ << "_pillars_repetition_" << current_rep_;
    if (path_data_.result_suffix.size() > 0)
    {
      ss_rep << "_" << path_data_.result_suffix << ".png";
    }
    else
    {
      ss_rep << ".png";
    }
    
    SaveResult(ss_rep.str(), &global_config_, *current_top);
  }
}

void FlowSculptGa::PrintResults() 
{
  std::cout << "{" << std::endl;
  std::cout << "\"global_top\": ";
  
  if (path_data_.result_dir != "")
  {
    std::stringstream ss;
    ss << path_data_.result_dir << "/top_result";
    if (path_data_.result_suffix.size() > 0)
    {
      ss << "_" << path_data_.result_suffix << ".png";
    }
    
    SaveResult(ss.str(), &global_config_, *global_top_);
  }
  
  global_top_->PrintJson();
  std::cout << "," << std::endl;
  
  std::cout << "\"per_pillar\": [" << std::endl;
  for (int i = 0; i < per_pillar_top_.size(); i++)
  {
    per_pillar_top_[i]->PrintJson();
    if (i < (max_pillars_ - min_pillars_) - 1)
    {
      std::cout << "," << std::endl;
    }
    delete per_pillar_top_[i];
  }
  
  std::cout << "]}" << std::endl;
}

std::string FlowSculptGa::ComputeRemainingTime(int current_pillars, int current_rep) 
{
  // See if GA is finished, return empty string
  if ((current_pillars == max_pillars_) && (current_rep == (reps_ - 1)))
  {
    return "";
  }
  
  // Have at least 3 samples to estimate time
  if (ga_counter_ < 3)
  {
    return "Estimating remaining time...";
  }
  
  double remaining_time = 0;
  std::stringstream time_log;
  
  // Compute slope and intercept for linear regression
  double m;
  double b;
  
  // Linear regression needs two different x-values.  If they don't exist, we simulate them for now
  if (std::adjacent_find(ga_pillars_.begin(), ga_pillars_.end(), std::not_equal_to<int>() ) == ga_pillars_.end())
  {
    const double scaling = 1 + (ga_pillars_[0]+1 - ga_pillars_[0])/ga_pillars_[0];
    std::vector<double> x_temp = ga_pillars_;
    std::vector<double> y_temp = ga_runtime_;
    double y_mean = std::accumulate(ga_runtime_.begin(), ga_runtime_.end(), 0.0) / ga_runtime_.size();
    for (int i = 0; i<ga_pillars_.size(); i++)
    {
      x_temp.push_back(ga_pillars_[0]+1);
      y_temp.push_back(y_mean*scaling);
    }
    ComputeSlopeIntercept(x_temp, y_temp, m, b);
  }
  else
  {
    ComputeSlopeIntercept(ga_pillars_, ga_runtime_, m, b);
  }
  
  time_log << "Estimated time remaining: ";
  
  // Finish out current number of pillars
  if (current_rep < reps_ - 1)
  {
    remaining_time += (reps_ - current_rep)*(current_pillars  * m + b);
  }
  
  // Finish out rest of sweep
  for (int pillars = current_pillars + 1; pillars <= max_pillars_; pillars++)
  {
    remaining_time += (pillars  * m + b) * reps_;
  }
  
  time_log << RuntimeMod(remaining_time);
  
  return time_log.str();
}
