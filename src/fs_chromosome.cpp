#include <math.h>
#include <cmath>
#include <iomanip>

#include "fs_chromosome.h"
#include "fs_mutation.h"
#include "fs_crossover.h"
#include "fs_utilities.h"
#include "fs_fitness.h"
#include "fs_forward_model.h"

using namespace GA;

double FsChromosome::Evaluate() {
  FitnessFunction Fitness = ((FitnessFunction) config->GetSetting("fitnessFunction").AsPointer);
  CrossSection outlet = ForwardModel(config, inlet_, seq_, imgh_, imgw_);
  return Fitness(*config, thread_data, outlet);
}

inline double FsChromosome::NewPillarDw() {
  return GA::Random::GetRandom() * (dw_max_ - dw_min_) + dw_min_;
}

void FixInletMaterial(std::vector<int>& inlet, int num_material) {
  std::vector<int> inlet_out;
  int base_material = inlet[0];
  int num_switches = int(GA::Random::GetRandomInt(1, inlet.size()-1));
  
  std::vector<int> channels_to_switch;
  int counter = 0;
  while (counter < num_switches) {
    int channel = GA::Random::GetRandomInt(0, inlet.size()-1);
    if (!IsMember(channels_to_switch, channel)) {
      channels_to_switch.push_back(channel);
      counter++;
    }
  }
  
  counter = 0;
  while (num_switches > 0) {
    // Choose new material
    int random_material = GA::Random::GetRandomInt(0, num_material);
    if (random_material != base_material) {
      // Choose channel to swap
      inlet[channels_to_switch[counter]] = random_material;
      num_switches--;
      counter++;
    }
  }
}

RealPillar FsChromosome::GenerateRandomPillar(int pillar_index)
{
  bool half_pillars = config->GetSetting("half_pillars").AsBool;
  bool uniform_pillar_width = config->GetSetting("uniform_pillar_width").AsBool;
  
  float yw = (float)(GA::Random::GetRandom() * ((yw_max_) - (yw_min_)) + yw_min_);
  float dw = NewPillarDw();
  
  float hw = half_pillars ? GA::Random::GetRandom():1.0f;
  
  if (uniform_pillar_width && pillar_index > 0) {
    dw = seq_[0].dw;
  }
  
  RealPillar rp(yw, dw, hw);
  
  // TODO: double- and triple-check generation doesn't create an invalid pillar.
  if (!rp.IsValid()) {
    MakePillarValid(half_pillars, rp.hw, rp.dw);
  }
  
  return rp;
}

void FsChromosome::Generate(bool pre_computed_seed) {
  int numPillars = config->GetSetting("numPillars").AsInt;
  bool fixed_inlet = config->GetSetting("fixed_inlet").AsBool;
  bool half_pillars = config->GetSetting("half_pillars").AsBool;
  int num_material = config->GetSetting("num_material").AsInt;
  int num_inlets = config->GetSetting("num_inlets").AsInt;
  double min_cw = config->GetSetting("min_channel_width").AsDouble;
  double max_cw = 1.0 - (num_inlets - 1) * min_cw;

  imgh_ = config->GetSetting("imgh").AsInt;
  imgw_ = config->GetSetting("imgw").AsInt;
  
  SetPillarGeo(half_pillars);
  
  /// Set inlet flow pattern
  if (!fixed_inlet) {
    for (int i = 0; i < num_inlets; i++) {
      
      if (i < num_inlets - 1) {
        assert(max_cw >= min_cw);
        
        double channel_width = min_cw + (GA::Random::GetRandom() * (max_cw - min_cw));
        
        //
        // max_cw starts as the maximum width the current channel can take up. It starts
        // by guessing all channels are min_cw and allowing the current channel to take
        // as much as is left over.
        //
        // max_cw is then updated for each channel by removing the factor of min_cw for
        // the current channel and replacing it with the actuall channel_width. This
        // method ensures that all channels are guaranteed to be >= min_cw as long as
        // num_inlets * min_cw <= 1.0.
        //
        
        max_cw = max_cw + min_cw - channel_width;
        
        //
        // N.B. We only add entries to the channel width list for all but the last entry,
        //      which is assumed to have width = 1.0 - all_the_others.
        //
        
        inlet_.first.push_back(channel_width);
      }
      
      inlet_.second.push_back((int)(GA::Random::GetRandom() * (num_material + 1)));
    }
  }
  
  /// Ensure that inlet flow pattern is physically allowed
  FixInlet();
  
  /// Set pillar sequence
  if (pre_computed_seed && config->GetSetting("use_seed_chromosome").AsBool)
  {
    std::vector<RealPillar> &seed_pillar_seq = * (std::vector<RealPillar> *) config->GetSetting("seed_pillar_seq").AsPointer;
    for (int i = 0; i < numPillars; i++)
    {
      if (i < seed_pillar_seq.size())
      {
        seq_.push_back(seed_pillar_seq[i]);
      }
      else
      {
        // If sequence longer than seed, fill with random
        RealPillar rp = GenerateRandomPillar(i);
        seq_.push_back(rp);
      }
    }
    
  }
  else
  {
    for (int i = 0; i < numPillars; i++) {
      RealPillar rp = GenerateRandomPillar(i);
      seq_.push_back(rp);
    }
  }
}

void FsChromosome::FixInlet() {
  if(config->GetSetting("fixed_inlet").AsBool) {
    inlet_ = * (InletDesign *) config->GetSetting("inlet_design").AsPointer;
    return;
  }
  
  double min_cw = config->GetSetting("min_channel_width").AsDouble;
  
  assert(inlet_.first.size() == inlet_.second.size() - 1);
  
  double channel_sum = 0.0;
  
  for (int i = 0; i < inlet_.first.size(); i++) {
    channel_sum += inlet_.first[i];
  }
  
  for (int i = 0; i < inlet_.first.size(); i++) {
    if (channel_sum + min_cw > 1.0) {
      channel_sum -= inlet_.first[i];
      inlet_.first[i] = min_cw;
      channel_sum += min_cw;
    }
  }
  
  assert(channel_sum + min_cw <= 1.0);
  
  int num_material = config->GetSetting("num_material").AsInt;
  bool all_same = true;
  
  /// Check to see if anybody has mutated/crossovered too far
  for (int i = 0; i < inlet_.second.size(); i++) {
    if ((inlet_.second[i] < 0) || (inlet_.second[i] > num_material)) {
      inlet_.second[i] = (int)(GA::Random::GetRandom() * (num_material + 1));
    }
  }
  
  /// Check to see if all materials are the same
  for (int i = 1; i < inlet_.second.size(); i++) {
    if (inlet_.second[i] != inlet_.second[i-1]) {
      all_same = false;
    }
  }
  /// If they are all the same, fix it
  if (all_same) {
    FixInletMaterial(inlet_.second, num_material);
  }
}

GA::Chromosome * FsChromosomeAllocate() {
  return new FsChromosome();
}

void MakePillarValid(const bool half_pillars, const float& hw, float& dw) {
  // If this is a full-pillar and diameter is greater than 0.75,
  // either clamp the diameter or clamp the height. For now,
  // we just clamp the diameter to make a fix compatible with
  // full-pillar only searches
  // TODO: find more elegant method of doing this!
  if (hw >= 0.5) {
    if (dw >= DW_MAX_FP) {
      dw = DW_MAX_FP - E;
    }
  }
}

std::string FsChromosome::GetPillarSeqIdx() {
  std::stringstream seq_idx;
  bool first = true;
  seq_idx << "[";
  for (const RealPillar& rp : seq_)
  {
    if (!first) {
      seq_idx << ", ";
    }
    seq_idx << rp.Interpret();
    first = false;
  }
  seq_idx << "]" << std::endl;
  return seq_idx.str();
}

void FsChromosome::PrintData()
{
  PrintJson();
}

void FsChromosome::PrintJson() 
{
  bool first = true;
  std::cout << "{" << std::endl;
  double score = GetScore();
  if (std::isnan(score))
  {
    score = 0.0;
  }
  std::cout << "\"score\": " << GetScore() << "," << std::endl;
  std::cout << "\"num_pillars\": " << seq_.size() << "," << std::endl;
  std::cout << "\"inlet\": {" << std::endl;
  std::cout << "\"widths\" : [";
  double total_width = 0;
  for (double width : inlet_.first)
  {
    if (!first) {
      std::cout << ", ";
    }
    std::cout << std::setprecision(2) << width;
    total_width += width;
    first = false;
  }
  std::cout << ", " << std::setprecision(2) << floorf((1 - total_width)*100)/100;
  std::cout << "],";
  std::cout << "\"material\" : [";
  first = true;
  for (int material : inlet_.second)
  {
    if (!first) {
      std::cout << ", ";
    }
    std::cout << material;
    first = false;
  }
  std::cout << "]";
  std::cout << "}," << std::endl;
  
  first = true;
  std::cout << "\"seq\": [";
  for (const RealPillar& rp : seq_)
  {
    if (!first) {
      std::cout << ", ";
    }
    std::cout << std::setprecision(4) << rp.dw << ", " << rp.yw << ", " << rp.hw;
    first = false;
  }
  std::cout << "]," << std::endl;
  
  first = true;
  std::cout << "\"indices\": [";
  for (const RealPillar& rp : seq_)
  {
    if (!first) {
      std::cout << ", ";
    }
    std::cout << rp.Interpret();
    first = false;
  }
  std::cout << "]" << std::endl;
  std::cout << "}" << std::endl;
  
}
