//
//  fs_error.cpp
//  flowsculptga
//
//  Created by Dan Stoecklein on 4/10/19.
//

#include "fs_error.h"

static const std::vector<std::string> fs_error_list = {
  /* 1 */ "Cannot use Corr2+FFT with multimaterial target.",
  /* 2 */ "Image cache setup failed."
  /* 3 */
  /* 4 */
  /* 5 */
  /* 6 */
  /* 7 */
  /* 8 */
  /* 9 */
};

void FSCHKERR(int fs_error)
{
  if (fs_error != 0) {
    std::stringstream error_msg;
    error_msg << "Error: " << fs_error_list[fs_error-1];
    
    std::cerr << error_msg.str() << std::endl;
    
    exit(-1);
  }
}
