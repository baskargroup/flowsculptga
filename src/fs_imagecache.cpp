#include "fs_imagecache.h"
#include "ocv_utilities.h"

#include <iostream>
#include <vector>

int ImageCache::SetImageCache(std::string image_file, bool half_pillars,
                              GA::Configuration* config) 
{
  
  half_pillars_ = half_pillars;
  config_ = config;
  
  if (!LoadTarget(image_file, image_data_)) 
  {
    return 0;
  } 
  else 
  {
    return 1;
  }
}

const CrossSection& ImageCache::GetTarget(int imgw, int imgh) 
{
  std::unique_lock<std::mutex> lock(cache_lock_);
  
  auto reskey = std::make_pair(imgw, imgh);
  
  if (target_cache_.count(reskey) == 0) 
  {
    target_cache_[reskey] = Target2CrossSection(image_data_, config_, imgw, imgh);
  }
  
  return target_cache_[reskey];
}

CrossSection ImageCache::GenerateTargetFFT(int imgw, int imgh) 
{
  auto key = std::make_pair(imgw, imgh);
  
  CrossSection& target_outlet = target_cache_[key];
  
  CrossSection temp;
  if (!half_pillars_) 
  {
    imgh = imgh/2;
    for (int i = 0; i<imgw*imgh; i++) 
    {
      temp.push_back(target_outlet[i]);
    }
  } 
  else 
  {
    temp = target_outlet;
  }
  
  /// fftw requires imgw * (imgw/2+1) allocated for complex data
  fftw_complex * outlet_ft = (fftw_complex *) fftw_malloc(imgh * (imgw/2 + 1) * sizeof(fftw_complex));
  fftw_plan p = fftw_plan_dft_r2c_2d(imgh, imgw, temp.data(), outlet_ft, FFTW_ESTIMATE);
  fftw_execute(p);
  fftw_destroy_plan(p);
  
  CrossSection target_outlet_ft;
  for (int i = 0; i < imgh * (imgw/2 + 1); i++) 
  {
    target_outlet_ft.push_back(mag_squared(outlet_ft[i]));
  }
  
  fftw_free(outlet_ft);
  
  return target_outlet_ft;
}

const CrossSection& ImageCache::GetTargetFFT(int imgw, int imgh) {
  GetTarget(imgw, imgh);
  std::unique_lock<std::mutex> lock(cache_lock_);
  
  auto reskey = std::make_pair(imgw, imgh);
  
  if (target_fft_cache_.count(reskey) == 0) 
  {
    target_fft_cache_[reskey] = GenerateTargetFFT(imgw, imgh);
  }
  
  return target_fft_cache_[reskey];
}

CrossSectionMAT ImageCache::GenerateTargetOCV(int imgw, int imgh) 
{
  auto reskey = std::make_pair(imgw, imgh);
  CrossSection& target_outlet = target_cache_[reskey];
  
  CrossSectionMAT target_outlet_MAT = ConvertToMAT(target_outlet, imgw, imgh);

//  cv::imshow("test", ocv_target);
//  cv::waitKey();
  
  return target_outlet_MAT;
}

const CrossSectionMAT& ImageCache::GetTargetOCV(int imgw, int imgh) 
{
  //  GetTarget(imgw, imgh);
  std::unique_lock<std::mutex> lock(cache_lock_);
  
  auto reskey = std::make_pair(imgw, imgh);
  
  if (target_ocv_cache_.count(reskey) == 0) 
  {
    target_ocv_cache_[reskey] = GenerateTargetOCV(imgw, imgh);
  }
  
  return target_ocv_cache_[reskey];
}
