//
//  fs_utilities.cpp
//  flowsculptga
//
//  Created by Dan Stoecklein on 1/16/19.
//

#include "fs_utilities.h"

std::chrono::time_point<std::chrono::high_resolution_clock> FSTimer() {
  return std::chrono::high_resolution_clock::now();
}

int FSRuntime_ms(std::chrono::time_point<std::chrono::high_resolution_clock> t_1, std::chrono::time_point<std::chrono::high_resolution_clock> t_2) {
  return std::chrono::duration_cast<std::chrono::milliseconds>(t_2 - t_1).count();
}

void ComputeSlopeIntercept(const std::vector<double>& x, const std::vector<double>& y, double& slope, double& intercept) {
  // Compute slope
  const double n    = x.size();
  const double s_x  = std::accumulate(x.begin(), x.end(), 0.0);
  const double s_y  = std::accumulate(y.begin(), y.end(), 0.0);
  const double s_xx = std::inner_product(x.begin(), x.end(), x.begin(), 0.0);
  const double s_xy = std::inner_product(x.begin(), x.end(), y.begin(), 0.0);
  slope    = (n * s_xy - s_x * s_y) / (n * s_xx - s_x * s_x);
  
  const double x_mean = s_x / x.size();
  const double y_mean = s_y / y.size();
  intercept = y_mean - slope * x_mean;
  
  // Don't let things get unreasonable
  if (slope < 0) {
    slope = 0;
    intercept =  y_mean;
  }
}

std::string RuntimeMod(double time) {
  std::stringstream time_output;
  if (time < 60) {
    time_output << std::setprecision(2) << time << " seconds.";
  } else if (time < 3600) {
    time_output << std::setprecision(2) << time / 60 << " minutes.";
  } else if (time < 86400) {
    time_output << std::setprecision(2) << time / 3600 << " hours.";
  } else {
    time_output << std::setprecision(2) << time / 86400 << " days.";
  }
  return time_output.str();
}
