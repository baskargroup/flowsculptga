#include <sstream>
#include <iomanip>
#include <chrono>
#include <ctime>
#include <numeric>
#include <algorithm>

#include "fs_flowsculptga.h"

using namespace GA;


void usage() 
{
  std::cout << "USAGE: ./flowsculpt <config_file>" << std::endl;
  std::cout << "\tconfig_file: Configuration file for GA" << std::endl;
}

int main(int argc, char* argv[]) 
{
  if (argc != 2) 
  {
    usage();
    return 0;
  }
  try
  {
    
    FlowSculptGa fs;
    
    fs.PrepareGa(argv[1]);
    
    fs.GaSweep();
    
    fs.PrintResults();
    
  } 
  catch (std::runtime_error& e) 
  {
    std::cerr << "Fatal error!" << std::endl;
    std::cerr << e.what() << std::endl;
  }
}
