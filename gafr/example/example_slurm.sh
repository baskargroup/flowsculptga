#!/bin/bash
#SBATCH -J gridga_example
#SBATCH -o job.out
#SBATCH -e job.err
#SBATCH -n 1
#SBATCH -p normal
#SBATCH -t 00:05:00

A=$(cat input)
OUT=$(python -c "print $A*$A")
echo $OUT > output