#include "ga.h"

#include <algorithm>

namespace GA {

  struct ChromosomeComparer {
    std::vector<Chromosome *> * Chromosomes;
    bool operator()(int a, int b) { return ((*Chromosomes)[a]->GetScore() < (*Chromosomes)[b]->GetScore()); }

    ChromosomeComparer(std::vector<Chromosome *> * _Chromosomes) : Chromosomes(_Chromosomes) { }
  };

  Solver::Solver() : Chromosomes(), generations_(), config() {
    FillDefaultSettings();
  }

  Solver::Solver(const Configuration& templateConfig) : Chromosomes(), generations_(), config(templateConfig) {
    FillDefaultSettings();
  }

  void Solver::Setup(int populationSize, Crossover::CrossoverMethod crossover, ChromosomeAllocator allocator) {
    FillDefaultSettings();
    config.SetSetting("CrossoverMethod", (void *) crossover, true);
    config.SetSetting("ChromosomeAllocator", (void *) allocator, true);
    config.SetSetting("PopulationSize", populationSize, true);
  }

  void Solver::Setup(int populationSize, Crossover::CrossoverMethod crossover, ChromosomeAllocator allocator, const Configuration& templateConfig)  {
    FillDefaultSettings();
    config.SetSetting("CrossoverMethod", (void *) crossover, true);
    config.SetSetting("ChromosomeAllocator", (void *) allocator, true);
    config.SetSetting("PopulationSize", populationSize, true);
  }

  void Solver::Setup(int populationSize, 
      Crossover::CrossoverMethod crossover, 
      ChromosomeAllocator allocator, 
      const Configuration &templateConfig,
      Chromosome *seed_ch) {
    FillDefaultSettings();
    config.SetSetting("CrossoverMethod", (void *) crossover, true);
    config.SetSetting("ChromosomeAllocator", (void *) allocator, true);
    config.SetSetting("PopulationSize", populationSize, true);
    config.SetSetting("SeedChromosome", true, true);
    SetSeedChromosome(seed_ch);
  }

  void Solver::FillDefaultSettings() {
    config.SetSetting("PopulationSize", 100, false);
    config.SetSetting("MaxGenerations", 200, false);
    config.SetSetting("StallLimit", 50, false);
    config.SetSetting("MutationRate", 0.2, false);
    config.SetSetting("SelectionMethod", (void *) Selection::TournamentSelect, false);
    config.SetSetting("TournamentSize", 4, false);
    config.SetSetting("EvaluationMethod", (void *) Evaluation::SerialEvaluate, false);
    config.SetSetting("ElitePart", 0.05, false);
    config.SetSetting("GenerationNum", 0, false);
    config.SetSetting("SeedChromosome", false, false);
  }

  void Solver::GAStep(bool printTop = false) {
    // Read needed items from the configuration
    int PopSize = config.GetSetting("PopulationSize").AsInt;
    int GenerationNum = config.GetSetting("GenerationNum").AsInt;
    Selection::SelectionMethod select = (Selection::SelectionMethod) config.GetSetting("SelectionMethod").AsPointer;
    Crossover::CrossoverMethod crossover = (Crossover::CrossoverMethod) config.GetSetting("CrossoverMethod").AsPointer;
    Evaluation::EvaluationMethod evaluate = (Evaluation::EvaluationMethod) config.GetSetting("EvaluationMethod").AsPointer;
    ChromosomeAllocator allocator = (ChromosomeAllocator) config.GetSetting("ChromosomeAllocator").AsPointer;
    PostStepCallback step_callback = nullptr;
    if (config.HasSetting("StepCallback")) {
      step_callback = (PostStepCallback) config.GetSetting("StepCallback").AsPointer;
    }
    double ElitePart = config.GetSetting("ElitePart").AsDouble;
    double MutationRate = config.GetSetting("MutationRate").AsDouble;

    int NumElites = PopSize * ElitePart;
    int NumMutation = (PopSize - NumElites) * MutationRate;
    int NumCrossover = PopSize - NumMutation - NumElites;

    if (GenerationNum == 0) 
    {
      // Create initial population
      generations_.push_back(Generation());

      for (int i = 0; i < PopSize; i++) 
      {
        if ((i == 0) && (config.GetSetting("SeedChromosome").AsBool))
        {
          Chromosome * ch = allocator();
          ch->SetConfiguration(&config);
          ch->Generate(true);
          Chromosomes.push_back(ch);
          generations_[0].push_back(Chromosomes.size() - 1);
          ch->SetId(Chromosomes.size() - 1);
        }
        else
        {
          Chromosome * ch = allocator();
          ch->SetConfiguration(&config);
          ch->Generate(false);
          Chromosomes.push_back(ch);
          generations_[0].push_back(Chromosomes.size() - 1);
          ch->SetId(Chromosomes.size() - 1);
        }
      }
    }

//    if (GenerationNum == 0)
//    {
//      std::cout << " -------- GENERATION 0 -------- " << std::endl;
//      for (int i = 0; i < Chromosomes.size(); i++)
//      {
//        Chromosomes[i]->PrintData();
//      }
//      std::cout << " ------------------------------ " << std::endl;
//    }

    generations_.push_back(Generation());
    Generation& CurrentGeneration = generations_[generations_.size() - 2];
    Generation& NextGeneration = generations_[generations_.size() - 1];

    // Evaluate
    evaluate(config, Chromosomes, CurrentGeneration);

    // Store fitness data for this generation
    if (config.GetSetting("StoreGenerationFitness").AsBool) {
      std::vector<double> tmp_scores;
      for (int idx : CurrentGeneration) {
        tmp_scores.push_back(Chromosomes[idx]->GetScore());
      }
      generation_data_.push_back(tmp_scores);
    }

    // Sort previous population so we can retrieve top results
    ChromosomeComparer comparer = ChromosomeComparer(&Chromosomes);
    std::sort(CurrentGeneration.begin(), CurrentGeneration.end(), comparer);    

    if (printTop)
      std::cerr << "Top Score for generation " << GenerationNum <<
        " : " << Chromosomes[CurrentGeneration.back()]->GetScore() << std::endl;

    // Mutation Children
    for (int i = 0; i < NumMutation; i++) {
      Chromosome * ch = select(config, Chromosomes, CurrentGeneration, &ranked_fitness_)->Clone();
      ch->ClearScore();
      ch->Mutate();
      Chromosomes.push_back(ch);
      NextGeneration.push_back(Chromosomes.size() - 1);
      ch->SetId(Chromosomes.size() - 1);
    }

    // Crossover Children
    for (int i = 0; i < NumCrossover; i++) {
      Chromosome * ch = allocator();
      ch->SetConfiguration(&config);
      crossover(config, ch,
          select(config, Chromosomes, CurrentGeneration, &ranked_fitness_),
          select(config, Chromosomes, CurrentGeneration, &ranked_fitness_));
      Chromosomes.push_back(ch);
      NextGeneration.push_back(Chromosomes.size() - 1);
      ch->SetId(Chromosomes.size() - 1);
    }

    // Elite Children
    for (int i = PopSize - NumElites; i < PopSize; i++) {
      NextGeneration.push_back(CurrentGeneration[i]);
    }

    config.SetSetting("GenerationNum", GenerationNum + 1);

    if (step_callback) step_callback(config);
  }

  bool CalcStallGen(std::vector<double>& ga_best, int current_gen,
      int stall_limit, double tol) {
    bool terminate = false;

    // Generation containing oldest fitness to be considered
    int oldest_gen = current_gen - stall_limit;

    // Normalizing value for relative change in fitnesss
    double fitness_norm = stall_limit * (fabs(ga_best[current_gen]) > 1.0f ?
        fabs(ga_best[current_gen]) : 1.0f);

    double rel_avg_fitness = fabs(ga_best[current_gen] -
        ga_best[oldest_gen]) / fitness_norm;

    if (rel_avg_fitness < tol) {
      terminate = true;
    }

    return terminate;
  }

  void Solver::Run(bool printTop = false) {
    int MaxGenerations = config.GetSetting("MaxGenerations").AsInt;
    int StallLimit = config.GetSetting("StallLimit").AsInt;
    int GenerationNum = config.GetSetting("GenerationNum").AsInt;

    // Selection, Crossover, and Mutation
    ranked_fitness_.SetRankedFitness(config.GetSetting("PopulationSize").AsInt);

    // Termination variables
    double fitness_tolerance = 1E-6;
    std::vector<double> ga_best;
    int last_improved_gen = 0;

    while(GenerationNum < MaxGenerations) {
      GAStep(printTop);

      ga_best.push_back(GetTop()->GetScore());

      // Update last improved generation
      if (GetTop()->GetScore() > ga_best[last_improved_gen]) {
        last_improved_gen = GenerationNum;
      }

      // Check termination conditions
      if (GenerationNum > StallLimit) {
        // Check averaged relative change over stall generations
        if (CalcStallGen(ga_best, GenerationNum, StallLimit, fitness_tolerance)) {
          if (printTop) {
            std::cerr << "GA terminated: average relative fitness less than " <<
              fitness_tolerance << ". Exiting." << std::endl;
          }
          break;
          // Check if fitness has improved over stall generations
        } else if (GenerationNum - last_improved_gen > StallLimit) {
          if (printTop) {
            std::cerr << "GA terminated: Output unchanged for " <<
              StallLimit << " generations. Exiting." << std::endl;
          }
          break;
        }
      }

      GenerationNum = config.GetSetting("GenerationNum").AsInt;
    }
  }

}
