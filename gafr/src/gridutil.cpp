#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <mutex>

#if defined(_WIN32)
#define popen _popen
#define pclose _pclose
#endif

#define DEBUG

#include "gridutil.h"

namespace GRIDUTIL {

std::mutex grid_mutex;

std::string sh(const char * cmd) {
    char buffer[128];

    grid_mutex.lock();
#ifdef DEBUG
    std::cerr << "sh: " << cmd << std::endl;
#endif

    std::string result = "";
    std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
    if (!pipe) throw std::runtime_error("popen() failed!");
    while (!feof(pipe.get())) {
        if (fgets(buffer, 128, pipe.get()) != NULL)
            result += buffer;
    }


    grid_mutex.unlock();

    return result;
}


int PbsSubmitJob(std::string script) {
    std::stringstream cmd;
    cmd << "qsub " << script << " < /dev/null | awk -F. 'END{print $1}'";

    std::string output = sh(cmd.str().c_str());

    return std::atoi(output.c_str());
}

GA::GRID::GridStatus PbsJobState(int jobid) {
    std::stringstream cmd;
    cmd << "qstat -f " << jobid << " < /dev/null | grep job_state | awk '{print $3}'";

    std::string state = sh(cmd.str().c_str());

    if(state.c_str()[0] == 'Q')
        return GA::GRID::Queued;

    if(state.c_str()[0] == 'R')
        return GA::GRID::Running;

    if(state.c_str()[0] == 'C')
        return GA::GRID::Finished;

    return GA::GRID::NotSubmitted;
}

void PbsKillJob(int jobid) {
    std::stringstream cmd;
    cmd << "qdel " << jobid;
    sh(cmd.str().c_str());
}


int SlurmSubmitJob(std::string script, std::string dir) {
    std::stringstream cmd;

    if (dir.empty()) {
        cmd << "sbatch " << script << " < /dev/null | awk 'END{print $4}'";
    }
    else {
        cmd << "sbatch -D " << dir << " " << script << " < /dev/null | awk 'END{print $4}'";
    }

    std::string output = sh(cmd.str().c_str());

    return std::atoi(output.c_str());
}

GA::GRID::GridStatus SlurmJobState(int jobid) {
    std::stringstream cmd;
    cmd << "squeue -j " << jobid << " < /dev/null | awk 'END{print $5}'";

    std::string state = sh(cmd.str().c_str());

    if(state.find("PD") != std::string::npos)
        return GA::GRID::Queued;

    if(state.find("R") != std::string::npos)
        return GA::GRID::Running;

    if(state.find("CG") != std::string::npos)
        return GA::GRID::Finished;

    return GA::GRID::NotSubmitted;
}

void SlurmKillJob(int jobid) {
    std::stringstream cmd;
    cmd << "scancel " << jobid;
    sh(cmd.str().c_str());
}

}
