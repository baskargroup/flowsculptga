#include <cmath>
#include <limits>
#include <random>

#include "ga.h"

namespace GA {

  static std::mt19937 mt;
  static std::random_device rd;
  static std::uniform_real_distribution<double> dist(0.0, 1.0);

  Random::Random() : seed_(rd()) {
    mt.seed(seed_);
  }

  void Random::ResetInternal(unsigned int seed) {
    seed_ = seed;
    mt.seed(seed_);
  }

  double Random::InternalGetRandom() {
    return dist(mt);
  }
  
  int Random::InternalGetRandomInt(int a, int b) {
    std::uniform_int_distribution<int> dist_int(a, b);
    return dist_int(mt);
  }

  double Random::GetRandomGaussianInternal(double mu, double sigma) {
    const double epsilon = std::numeric_limits<double>::min();
    const double two_pi = 2.0*3.14159265358979323846;

    static double z0, z1;
    static bool generate = false;
    generate = !generate;

    if (!generate)
      return z1 * sigma + mu;

    double u1, u2;
    do
    {
      u1 = InternalGetRandom();
      u2 = InternalGetRandom();
    } while (u1 <= epsilon);

    z0 = sqrt(-2.0 * log(u1)) * cos(two_pi * u2);
    z1 = sqrt(-2.0 * log(u1)) * sin(two_pi * u2);
    return z0 * sigma + mu;
  }

}
