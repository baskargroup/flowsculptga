#include "ga.h"

namespace GA {
  RankedFitness::RankedFitness(int pop_size) {
    SetRankedFitness(pop_size);
  }
  void RankedFitness::SetRankedFitness(int pop_size) {
    
//    std::cout << "making a new one" << std::endl;
    int nParents = pop_size*2;
    
    std::vector<double> expectation;
    
    double sum = 0;
    for (double i=1; i<=pop_size; i++) {
      double increment = 1.0 / sqrt(i);
      expectation.push_back(increment);
      sum += increment;
    }
    
    for (int i=expectation.size()-1; i>=0; i--) {
      ranked_fitness.push_back(nParents * expectation[i] / sum);
    }  
    
//    ranked_sum = 0;
//    double sum_part = 0;
//
//    /// Linear scaling
//    for (int i=0; i<pop_size; i++) {
//      ranked_sum += 1 / pop_size;
//    }
//    double pop_scaling = pop_size / ranked_sum;
//    for (int i=0; i<pop_size; i++) {
//      sum_part += pop_scaling * (1 / pop_size);
//      ranked_fitness.push_back(sum_part);
//    }
    
    /// Nonlinear scaling
//        for (int i=0; i<pop_size; i++) {
//          ranked_sum += 1 / sqrt(pop_size - i);
//        }
//        double pop_scaling = pop_size / ranked_sum;
//        for (int i=0; i<pop_size; i++) {
//          sum_part += pop_scaling * (1 / sqrt(pop_size - i));
//          ranked_fitness.push_back(sum_part);
//        }
  }
  std::vector<double> RankedFitness::GetRankedFitness() {
    return ranked_fitness;
  }
  double RankedFitness::GetRankedSum(){
    return ranked_sum;
  }
  
  
  namespace Selection {
    
    Chromosome * TournamentSelect(const Configuration& config,
                                  const std::vector<Chromosome *>& Chromosomes,
                                  Generation& CurrentGeneration,
                                  RankedFitness* ranked_fitness) {
      
      bool ranked = false;
      int population_size = CurrentGeneration.size();
      int k = config.GetSetting("TournamentSize").AsInt;
      
      int champion, contestant;
      
      if (config.HasSetting("RankedFitness")) {
        ranked = config.GetSetting("RankedFitness").AsBool;
      }
      
      if (ranked) {
        champion = int(population_size * Random::GetRandom());
        
        for (int i = 0; i < k; i++) {
          contestant = int(population_size * Random::GetRandom());
          if (ranked_fitness->GetRankedFitness()[contestant] > ranked_fitness->GetRankedFitness()[champion]) {
            champion = contestant;
          }
        }
      } else {
        champion = int(population_size * Random::GetRandom());
        
        for (int i = 0; i < k; i++) {
          contestant = int(population_size * Random::GetRandom());
          if(Chromosomes[CurrentGeneration[contestant]]->GetScore() >
             Chromosomes[CurrentGeneration[champion]]->GetScore()) {
            champion = contestant;
          }
        }
      }
      return Chromosomes[CurrentGeneration[champion]];;
    }
    
    Chromosome * RouletteSelect(const Configuration& config,
                                const std::vector<Chromosome *>& Chromosomes,
                                Generation& CurrentGeneration,
                                RankedFitness* ranked_fitness) {
      bool ranked = false;
      double sum = 0;
      double sum_part = 0;
      int popSize = CurrentGeneration.size();
      
      if (config.HasSetting("RankedFitness")) {
        ranked = config.GetSetting("RankedFitness").AsBool;
      }
      // Find sum of scores or ranks.
      int i; // Selected parent index
      if (ranked) {
        double selection = Random::GetRandom() * popSize;
        
        // Select chromosome
        for (i=0; i<popSize; i++) {
          if (ranked_fitness->GetRankedFitness()[i] > selection) {
            break;
          }
        }
      }
      else {
        /* Using fitness for roulette wheel proporations */
        for (int idx : CurrentGeneration) {
          sum += Chromosomes[idx]->GetScore();
        }
        double random = Random::GetRandom() * sum;
        for (i=0; i<popSize; i++) {
          sum_part += Chromosomes[CurrentGeneration[i]]->GetScore();
          if (sum_part > random) {
            break;
          }
        }
      }
      if (i >= popSize) {
        i = popSize - 1;
      }
      
      return Chromosomes[CurrentGeneration[i]];
    }
  }
}
