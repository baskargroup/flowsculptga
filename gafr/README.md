GAFr
===
A cross-platform C++ framework for genetic algorithm optimization

## What does this do?
GAFr is a GA solver. It provides a framework for building a GA application. GAFr comes with a solver, and a chromosome interface. The intent is that a user can implement their own representation of a chromosome, and supply it to this framework and run optimization on it.

## Features

* Standard GA solver
* Flexible chromosome interface
* Highly modifiable with dynamic configuration
* Supplies a random number generator based on [SFMT](http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/SFMT/)

## Getting started
### Dependancies
GAFr requires no dependancies other than standard C++ libraries and wither make (linux) or nmake (windows) to build it.

## Cloning the repository
The first step is to clone this repository off BitBucket:
~~~
$ git clone https://bitbucket.org/baskargroup/gpu-cpu-code-for-ga.git
~~~

## Building GAFr
There are two make files for GAFr, one for linux/unix environments with GNU Make, and one for Windows environments with NMake.
### Linux
To build with GNU Make, simply execute
~~~
$ make -f linux.mk
~~~
### Windows
`Note:` Windows take a little bit of messing with to get NMake set up to work from PowerShell. If you do not have that setup or don't want to, you can open Visual Studio and run `Tools->Visual Studio Command Prompt` and navigate to the directory you cloned this repository. All following command for Windows should work.

To build with NMake, execute
~~~
PS> nmake /f windows.mk
~~~
### Output
The output is a static library named libgafr.\[a|lib\] in the bin/ folder.

## Debugging GAFr
The makefiles supplied with GAFr come with debug configurations. To build GAFr for debug targets, type (for Linux):
~~~
$ make -f linux.mk debug
~~~

or on Windows, type:
~~~
PS> nmake /f windows.mk debug
~~~

## GAFr Example Application
Supplied with GAFr is a sample program that uses the GA to minimize the function f(x)=x^2. To build and run on,

Linux:
~~~
$ make -f linux.mk example
$ ./bin/example
~~~

Windows:
~~~
PS> nmake /f windows.mk example
PS> .\bin\example.exe
~~~


The output should look something like this:
~~~
Example started
Population Size: 40
Top Score for generation 0 : 0
Top Score for generation 1 : 7.1115e-54
Top Score for generation 2 : 7.1115e-54
Top Score for generation 3 : 7.1115e-54
Top Score for generation 4 : 3.24081e-48
Top Score for generation 5 : 0.998718
...
Top Score for generation 56 : 0.998718
Warning: Output unchanged for 50 generations. Exiting.
Completed GA.
Top score: 0.998718 at x = -0.0358147
~~~

## Documentation
GAFr is commented with Doxygen style comments. The included Doxyfile allows users to generate html documentation of the code. To do this run,
~~~
$ doxygen
~~~
or,
~~~
PS> Doxygen
~~~
This will build the documentation into the html/ directory. 

`Note`: Generating documentation requires that Doxygen be installed on your system, as well as GraphViz for optional class / call hierarchy generation.