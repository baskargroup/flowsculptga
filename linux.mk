# The following lines can be modified if using a custom install of thirdparty libs:
INC_SEARCH_DIRS= # -I/path/to/dir
LIB_SEARCH_DIRS= # -L/path/to/dir

# Do not modify below this line
CXX=icpc
LINKER=icpc
OBJ=obj/fs_image.o obj/main.o obj/fs_chromosome.o obj/fs_fft.o obj/fs_multires.o obj/fs_inletgenerator.o obj/fs_setup.o obj/fs_fitness.o obj/fs_imagecache.o obj/fs_mapcache.o obj/fs_utilities obj/QuiverInterpolate.o

THIRD_PARTY_INC=$(addprefix -I, $(wildcard thirdparty/libs/*/include))
INC_DIRS=-Iinclude -Igafr/include $(THIRD_PARTY_INC) $(INC_SEARCH_DIRS)

ZLIB_DIR=-L./thirdparty/libs/zlib-1.2.11/lib

THIRD_PARTY_LIB=$(addprefix -L, $(wildcard thirdparty/libs/*/lib)) $(addprefix -L, $(wildcard thirdparty/libs/*/lib64))
LIB_DIRS=-Lgafr/bin $(THIRD_PARTY_LIB) $(LIB_SEARCH_DIRS) $(ZLIB_DIR)
LIBS=-pthread -lgafr -lfftw3 -lz -lopencv_shape -lopencv_stitching -lopencv_objdetect -lopencv_superres -lopencv_videostab -lopencv_calib3d -lopencv_features2d -lopencv_highgui -lopencv_videoio -lopencv_imgcodecs -lopencv_video -lopencv_photo -lopencv_ml -lopencv_imgproc -lopencv_flann -lopencv_core

CFLAGS=-std=c++14 $(OFLAGS) $(DFLAGS) $(INC_DIRS)
LDFLAGS=$(LIB_DIRS)

OCV_LIBS=$(wildcard thirdparty/libs/*opencv*/lib*/libopencv_*.so*) $(wildcard thirdparty/libs/*opencv*/lib*/libopencv_*.dylib)


.PHONY: clean gafr release debug pre-build

all: default
default: release

pre-build:
	@mkdir -p bin
	@mkdir -p obj
#	@cp $(OCV_LIBS) bin

release: pre-build
	@$(MAKE) --no-print-directory -f linux.mk bin/flowsculptga OFLAGS="-O3 -funroll-loops"

debug: pre-build
	@$(MAKE) --no-print-directory -f linux.mk bin/flowsculptga DFLAGS="-D DEBUG -g -Wall" DEBUG=debug

gafr:
	@$(MAKE) --no-print-directory -C gafr -f linux.mk $(DEBUG) 

bin/flowsculptga: gafr $(OBJ)
	@echo "    [LD] $@"
	@$(LINKER) $(LDFLAGS) $(OBJ) $(LIBS) -o $@

obj/%.o: src/%.cpp
	@echo "    [CXX] $<"
	@$(CXX) $(CFLAGS) -c $< -o $@

dSFMT/%.o: dSFMT/%.c
	@echo "    [CC] $<"
	@$(CXX) $(CFLAGS) -c $< -o $@

clean:
	@$(MAKE) --no-print-directory -C gafr -f linux.mk clean
	@\rm -rf bin
	@\rm -rf obj

