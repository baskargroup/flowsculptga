import scipy.misc as misc
import numpy as np
import sys

img = misc.imread(sys.argv[1])
fh = (img.shape[0] - 1) * 2 + 1

if len(img.shape) == 3:
    img = img[:,:,0]

img_full = np.zeros((fh, img.shape[1]))

img_full[:img.shape[0],:] = img
img_full[img.shape[0]-1:,:] = np.flipud(img)

misc.imsave(sys.argv[2], img_full)