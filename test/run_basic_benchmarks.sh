#!/bin/bash

for f in benchmark_targets/single_inlet_targets/* ; do

    fb=$(basename $f)
    echo $fb

echo "
half_pillars: false
mbundle_folder: ../mbundles

imgw: 801
imgh: 201
renum: 20
target: $f

minpillars: 5
maxpillars: 8
reps: 1
num_inlets: 5

#fixed_inlet: 00100

translation_invariance: false
PopulationSize: 50
MaxGenerations: 200
MutationRate: 0.05
numpar: 4
result_directory: .
result_suffix: $(basename $f)
per_pillar_results: false

" > ga.config

    echo "$(pwd)/../bin:$LD_LIBRARY_PATH"

    LD_LIBRARY_PATH="$(pwd)/../bin:$LD_LIBRARY_PATH" ../bin/flowsculptga ga.config > out/$fb.out.json

done