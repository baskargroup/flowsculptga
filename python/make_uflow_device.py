#!/usr/bin/env python

import numpy as np
import scipy as sp
import gzip

try:
    import cPickle
except:
    import _pickle as cPickle


default_colors = [ (0, 0, 0, 1),(0, 1, 0, 1),  (0, 0, 0, 1), (1, 0, 0, 1),(0, 1, 1, 1), (1, 0, 0, 1),
    (0, 1, 1, 1), (1, 0, 0, 1),(0, 1, 1, 1), (1, 0, 0, 1),]


def data_32_to_D_y(seq32):
    # Convert integer proxy (1-32) valued pillar sequences to (D,y) pillar
    # Sequences

    seq_D_y_h = np.zeros((3,1))

    seq_D_y_h[0] = 0.250 + 0.125 * np.ceil(seq32/8.0)

    seq_D_y_h[1] = (seq32 - (np.ceil(seq32/8.0)-1)*8.0)*0.125 - 0.5

    seq_D_y_h[2] = 1.0

    return seq_D_y_h


def form_inlet(channels, Ny, Nz):
    bins = len(channels)
    inlet = np.zeros(Ny*Nz)
    count = 0
    for j in range(0, Nz):
        for i in range(0,Ny):
            col = int(np.floor((i / float(Ny)) * bins))
            if channels[col] == 1:
                inlet[count] = 1
                count += 1
            else:
                inlet[count] = 0
                count += 1
    inlet[0] = 0

    return inlet


def translate_FlowSculpt_to_uFlow(seq32, inlet, colors=default_colors):
    Np = len(seq32)
    Ns = len(inlet)

    # Translate pillars
    device_seq = []
    for n in range(0,Np):
        if seq32[n] > 32:
            print("Error. Cannot export half-pillars to uFlow")

        dy_pillar = data_32_to_D_y(seq32[n]+1)
        pillar_dict = dict(y=dy_pillar[1][0], d=dy_pillar[0][0])
        pillar_entry = ('Circular', pillar_dict)
        device_seq.append(pillar_entry)

    device_sliders = np.zeros(Ns+1)
    # Translate sliders (inlet)
    # print(inlet)
    device_sliders[0] = inlet[0]
    for n in range(1,Ns):
        device_sliders[n] = device_sliders[n-1] + inlet[n]
    device_sliders[Ns] = 1.0

    device_sliders = map(lambda x : round(x*1000)/1000, device_sliders)

    if isinstance((colors[0]), int):
        device_colors = species_to_color(colors)
    else:
        device_colors = colors

    device_version = (0, 2)

    device_dev = dict(pillars=device_seq,sliders=device_sliders,colors=device_colors,version=device_version)

    return device_dev


def species_to_color(all_species):
    all_colors = []
    for species in all_species:
        if species == 0:
            all_colors.append(uflow_colors('white'))
        elif species == 1:
            all_colors.append(uflow_colors('blue'))
        elif species == 2:
            all_colors.append(uflow_colors('red'))
        elif species == 3:
            all_colors.append(uflow_colors('green'))
        elif species == 4:
            all_colors.append(uflow_colors('orange'))

    return all_colors


def uflow_colors(color):
    color_dict = {'white': (1, 1, 1, 1), 'black': (0, 0, 0, 1), 'green': (0, 1, 0, 1), 'orange': (1, 0.5, 0, 1),
                  'red': (1, 0, 0, 1), 'blue': (0, 0, 1, 1)}
    return color_dict[color]


def make_dumbbell_device():
    inlet_design = [0.77, 0.06, 0.02, 0.08]
    inlet_colors = [uflow_colors('black'), uflow_colors('green'), uflow_colors('orange'), uflow_colors('green'), uflow_colors('black')]
    dumbbell_device = [[20, 5, 27, 20, 28, 20, 30, 30, 30, 22, 31, 22], inlet_design, inlet_colors,'dumbbell_01.dev']
    make_device(dumbbell_device)

def make_temp_device():
    inlet_design = [0.776755, 0.0766594, 0.0323257, 0.017194]
    inlet_colors = [0, 1, 2, 3, 0]
    sequence = [29, 10, 20, 28, 20, 25, 31, 31, 14, 21, 20]

    device_design = translate_FlowSculpt_to_uFlow(sequence, inlet_design, inlet_colors)

    device_name = 'Slide02_11_pillars.dev'
    f = open(device_name, "wb")
    cPickle.dump(device_design, f)
    f.close()



def print_device(design):
    for element in design:
        print(element)

def make_device(design):
    # print "Making %s"%(design[3])

    device_design = translate_FlowSculpt_to_uFlow(design[0], design[1], design[2])

    f = open(design[3], "wb")
    cPickle.dump(device_design, f)
    f.close()

def main():
    # make_dumbbell_device()
    make_temp_device()


if __name__ == '__main__':
    main()
