#!/usr/bin/env python

import os
import shutil
import subprocess
import sys

try:
    from Queue import Queue
except:
    from queue import Queue

from threading import Thread

CODES = os.environ['CODES']
flowsculpt_dir = '{}/flowsculptga'.format(CODES)
python_dir = '{}/python'.format(flowsculpt_dir)
sys.path.insert(0, python_dir)

from ga_campaign import *

num_workers = 4

def main():

    run_dir = os.getcwd()
    target_dir = 'h025_multi_species_targets'
    result_dir = 'h025_multi_species_campaign5_results'

    ga_params = {}
    ga_params['half_pillars'] = 'false'
    ga_params['mbundle_folder'] = '{}/mbundles'.format(flowsculpt_dir)
    ga_params['imgw'] = 401
    ga_params['imgh'] = 101
    ga_params['renum'] = 20
    ga_params['minpillars'] = 1
    ga_params['maxpillars'] = 15
    ga_params['reps'] = 30
    ga_params['num_inlets'] = 5
    ga_params['PopulationSize'] = 100
    ga_params['MaxGenerations'] = 500
    ga_params['MutationRate'] = 0.1
    ga_params['numpar'] = 8
    ga_params['result_directory'] = '.'
    ga_params['result_suffix'] = 0
    ga_params['per_pillar_results'] = 'true'

    campaign_params = {}
    campaign_params['ga_params'] = ga_params
    campaign_params['target_dir'] = '{}/{}'.format(run_dir, target_dir)
    campaign_params['result_dir'] = '{}/{}'.format(run_dir, result_dir)
    campaign_params['num_workers'] = num_workers
     
    multi_species_campaign_1 = GACampaign(campaign_params)
    #multi_species_campaign_1.run_jobs()
    multi_species_campaign_1.parse_jobs()



if __name__ == '__main__':
    main()
