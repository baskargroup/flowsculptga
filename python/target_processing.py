#!/usr/bin/env python

import csv as csv
import numpy as np
from PIL import Image
import os
import sys
from fractions import Fraction
import math
import shutil


min_allowed_fraction = 0.001


def distance_bmp(c1, c2):
    (r1,g1,b1) = c1
    (r2,g2,b2) = c2
    return math.sqrt((r1 - r2)**2 + (g1 - g2) ** 2 + (b1 - b2) **2)

def distance_png(c1, c2):
    (r1,g1,b1,a1) = c1
    (r2,g2,b2,a2) = c2
    return math.sqrt((r1 - r2)**2 + (g1 - g2) ** 2 + (b1 - b2) **2)


def target_color_count(target, imgw, imgh):
    '''
    Scans target to detect number of colors and the
    minimum volume fraction
    '''
    try:
        result = image_color_count(target, imgw, imgh)
        return result
    except:
        result = csv_color_count(target)
        return result


def csv_color_count(target):
    '''
    Assuming csv is correctly formatted with integer values for colors
    '''
    readdata = csv.reader(open(target))

    all_data = []
    for row in readdata:
        all_data.append(row)
    all_Data = np.array(all_data)

    unique, counts = np.unique(all_data, return_counts=True)
    all_counts = dict(zip(unique, counts))

    num_colors = len(all_counts) - 1  # Remove background color
    num_inlets = None
    final_color_bank = None

    return [num_colors, num_inlets, final_color_bank]

def compute_asymmetry(target, imgw, imgh):
    im = np.array(Image.open(target))
    [ys, xs, zs] = im.shape
    print('ys = {}, xs = {}'.format(ys, xs))

    for j in range(int(ys/2)-1):
        for i in range(int(xs)):
            if im[j,i].all() != im[ys-(j+1),i].all():
                return False
    return True


def image_color_count(target, imgw, imgh):
    img_file = Image.open(target)
    [xs, ys] = img_file.size
    if (xs != imgw or ys != imgh):
        print("Image size of {}x{} doesn't match ga_params of {}x{}".format(
            xs, ys, imgw, imgh))
        print("Resizing.")
        img_file = img_file.resize((imgw, imgh), Image.NEAREST)
        xs = imgw
        ys = imgh

    print("new size: {}x{}".format(img_file.size[0], img_file.size[1]))

    total_volume = float(xs * ys)

    color_bank = []
    color_bank_count = []
    for pixel in img_file.getdata():
        if (pixel not in color_bank) and (pixel != (255, 255, 255)):
            color_bank.append(pixel)
            color_bank_count.append(1)
        else:
            for i, color in enumerate(color_bank):
                if pixel == color:
                    color_bank_count[i] += 1

    num_colors = 0
    min_fraction = 1.0
    final_color_bank = []
    for i, count in enumerate(color_bank_count):
        volume_fraction = float(count) / total_volume
        if volume_fraction > min_allowed_fraction:
            final_color_bank.append(color_bank[i])
            min_fraction = volume_fraction
            num_colors += 1

    #num_colors -= 1  # Remove background color
    res = Fraction(min_fraction).limit_denominator(1000)
    num_inlets = res.denominator

    print(final_color_bank)


    return [num_colors, num_inlets, final_color_bank]


def target_to_csv(image_fname, single_species, csv_fname, final_color_bank, imgw, imgh):
    target_ext = os.path.splitext(image_fname)[1]
    if target_ext == '.csv':
        shutil.copyfile(image_fname, csv_fname)
    else:
        img_file = Image.open(image_fname)
        img = img_file.load()

        [xs, ys] = img_file.size
        if (xs != imgw or ys != imgh):
            img_file = img_file.resize((imgw, imgh), Image.NEAREST)
            xs = imgw
            ys = imgh

        csv_img = np.zeros((ys, xs))
        print("xs x ys = {}x{}".format(xs, ys))

        for x in range(xs):
            for y in range(ys):
                pixel = img_file.getpixel((x, y))
                if (img_file.format == 'BMP') or (target_ext == '.bmp'):
                    closest_color = sorted(final_color_bank, key=lambda color: distance_bmp(color, pixel))[0]
                elif img_file.format == 'PNG':
                    closest_color = sorted(final_color_bank, key=lambda color: distance_png(color, pixel))[0]
                else:
                    print('Unrecognized image format')
                if pixel == (255, 255, 255):
                    csv_img[y][x] = 0
                elif single_species:
                    csv_img[y][x] = 1.0
                else:
                    csv_img[y][x] = final_color_bank.index(closest_color) + 1

        np.savetxt(csv_fname, csv_img, delimiter=",", fmt='%d')



def main():
    imgw = 801
    imgh = 201
    for n in range(1, len(sys.argv)):
        target_fname = sys.argv[n]
        csv_fname = '{}.csv'.format(os.path.splitext(target_fname)[0])

        [num_colors, num_inlets, final_color_bank] = image_color_count(target_fname, imgw, imgh)
        target_to_csv(target_fname, csv_fname, final_color_bank, imgw, imgh)

if __name__ == '__main__':
    main()
