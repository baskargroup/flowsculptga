#!/usr/bin/env python
import re
import ast
import json
import os
import subprocess
import numpy as np
import platform
from PIL import Image
import sys

import matplotlib as mpl
if platform.system() == 'Darwin':
    mpl.use('Agg')
import matplotlib.pyplot as plt

CODES = os.environ['CODES']
flowsculpt_dir = '{}/flowsculptga'.format(CODES)
python_dir = '{}/python'.format(flowsculpt_dir)
sys.path.insert(0, python_dir)
import make_uflow_device as mud

class ReportWriter:
    def __init__(self, parser):
        self.parser = parser
        self.job_name = parser.job_name
        self.result_dir = parser.result_dir
        self.report_dir = parser.report_dir
        self.report_fname = '{}/{}_report.tex'.format(parser.report_dir, self.job_name)

    def write_report(self):
        print('Writing report')
        with open(self.report_fname, 'w') as f:
            self.write_preamble(f)
            self.write_main_result_text(self.parser, f)
            self.write_per_pillar_results_test(f)
            self.write_end_text(f)

        self.compile_latex(self.report_dir, self.report_fname)

    def compile_latex(self, report_dir, report_fname):
        latex_cmd = 'pdflatex -shell-escape -output-directory {} {}'.format(
            report_dir, report_fname)
        FNULL = open(os.devnull, 'w')
        subprocess.call(latex_cmd, stdout=FNULL, stderr=subprocess.STDOUT, shell=True)


    def write_preamble(self, f):
        f.write('\\documentclass[11pt, oneside]{article}\n')
        f.write('\\usepackage[margin=0.5in]{geometry}\n')
        f.write('\\geometry{letterpaper}\n')
        f.write('\\usepackage{graphicx}\n')
        f.write('\\usepackage{amssymb}\n')
        f.write('\\usepackage[hidelinks]{hyperref}\n')
        f.write('\\usepackage{verbatim}\n')
        f.write('\\usepackage{cprotect}\n')
        f.write('\\begin{document}\n')

    def write_figure(self, f, location, fname, caption=None, width=0.9):
        f.write('\\begin{figure}[h]\n')
        f.write('\centering\n')
        f.write('\t\\frame{{\\includegraphics[width={}\\textwidth]{{{}/{}}}}}\n'.format(
            width, location, fname))
        if (caption is not None):
            f.write('\t\caption{{{}}}\n'.format(caption))
        f.write('\\end{figure}\n')

    def create_result_caption(self, result):
        caption = 'Pillar indices={}, inlet flow pattern = {}, inlet species pattern = {}'\
            .format(result['"indices"'], result['"widths"'].strip(','), result["species"])
        return caption

    def write_main_result_text(self, parser, f):
        # Write in target
        f.write('\\cprotect\\section{{Target: \\verb|{}|}}\n'.format(self.job_name))
        self.write_figure(f, self.report_dir, 'target.png')

        # Write in top result
        f.write('\\subsection{Top result}\n')
        caption = self.create_result_caption(parser.top_result)
        self.write_figure(f, self.result_dir, 'top_result_0.png', caption=caption)

        # Write in statistics
        f.write('\\subsection{Statistics}\n')
        self.write_figure(f, self.report_dir, 'statistics.eps')
        f.write('\clearpage\n')

    def write_per_pillar_results_test(self,f):
        # Write in best per-pillar results
        f.write('\\section{Per-pillar results}\n')
        for n in range(self.parser.num_pillars):
            pillar_number = self.parser.pillars_start + n
            pillar_caption = '{} pillars'.format(pillar_number)
            self.write_figure(f, self.result_dir, 'per_pillar_{}_result_0.png'.format(pillar_number),
                    caption=pillar_caption, width = 1.0)

    def write_end_text(self,f):
        f.write('\\end{document}\n')



class GAParser:
    def __init__(self, job_name, result_dir):
        self.job_name = job_name
        self.result_dir = result_dir
        self.report_dir = '{}/Report'.format(self.result_dir)
        if not os.path.isdir(self.report_dir):
            os.makedirs(self.report_dir)
        self.output_fname = '{}/{}'.format(self.result_dir, 'std.out')
        self.config_fname = '{}/{}'.format(self.result_dir, 'ga.config')

        self.ParseConfiguration()

        self.top_result = {}
        self.per_pillar_results = []
        self.all_ga_scores = np.zeros((self.num_pillars, self.ga_reps))
        self.all_ga_gen = np.zeros((self.num_pillars, self.ga_reps))

        self.ParseAllScores()
        self.ParseTopResult()

        self.CreateTopDevice()

        self.ParsePerPillarResults()

        self.compute_stats()

    def compute_stats(self):
        self.gen_mean = self.all_ga_gen.mean()
        self.gen_max = self.all_ga_gen.max()
        self.gen_min = self.all_ga_gen.min()

        # plt.hist(self.all_ga_gen.flatten(), 25, normed=1)
        # plt.savefig('{}/generation_hist.png'.format(self.report_dir))
        # plt.close()

        # plt.figure()
        # x_values = np.linspace(1, self.num_pillars, self.num_pillars)
        # x_pillars = np.linspace(self.pillars_start, self.pillars_stop, self.num_pillars, dtype=int)
        # plt.boxplot(self.all_ga_scores.T)
        # plt.xticks(x_values, x_pillars)
        # axes = plt.gca()
        # # axes.set_ylim([0, 1])
        # plt.ylabel('GA Fitness ')
        # plt.xlabel('Number of pillars in sequence')
        # plt.savefig('{}/fitness_plot.png'.format(self.report_dir))
        # plt.close()

        fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(11,4))

        x_values = np.linspace(1, self.num_pillars, self.num_pillars)
        x_pillars = np.linspace(self.pillars_start, self.pillars_stop, self.num_pillars, dtype=int)

        axes[0].boxplot(self.all_ga_gen.T)
        axes[0].set_xticks(x_values)
        axes[0].set_xticklabels(x_pillars)
        axes[0].set_ylabel('Generations in GA run')
        axes[0].set_xlabel('Number of pillars in sequence')

        axes[1].boxplot(self.all_ga_scores.T)
        axes[1].set_xticks(x_values)
        axes[1].set_xticklabels(x_pillars)
        axes[1].set_ylabel('GA Fitness')
        axes[1].set_xlabel('Number of pillars in sequence')

        title = 'Population={}, Repetitions={}\nMax generations={}, Mutation rate={}'.format(
                self.popsize, self.ga_reps, self.maxgen, self.mutation_rate)

        plt.suptitle(title)
        plt.tight_layout()
        plt.subplots_adjust(top=0.85)
        plt.savefig('{}/statistics.eps'.format(self.report_dir))
        plt.close()


    def ParseConfiguration(self):
        f = open(self.config_fname)
        line = f.readline()
        while line:
            parsed_line = line.strip('\n').split(":")
            #print line
            if parsed_line[0] == 'minpillars':
                self.pillars_start = int(parsed_line[1])
            elif parsed_line[0] == 'maxpillars':
                self.pillars_stop = int(parsed_line[1])
            elif parsed_line[0] == 'reps':
                self.ga_reps = int(parsed_line[1])
            elif parsed_line[0] == 'imgw':
                self.Ny = int(parsed_line[1])
            elif parsed_line[0] == 'imgh':
                self.Nz = int(parsed_line[1])
            elif parsed_line[0] == 'PopulationSize':
                self.popsize = int(parsed_line[1])
            elif parsed_line[0] == 'MaxGenerations':
                self.maxgen = int(parsed_line[1])
            elif parsed_line[0] == 'MutationRate':
                self.mutation_rate = float(parsed_line[1])
            elif parsed_line[0] == 'half_pillars':
                if parsed_line[1] == ' true':
                    self.use_FC = True
            line = f.readline()
        f.close()
        self.num_pillars = self.pillars_stop - self.pillars_start + 1
        assert self.num_pillars != 0

    def ParseResult(self, f):
        Result = {}
        for n in range(6):
            line = f.readline()
            # print(line)
            parsed_line = line.strip('\n').split(':')
            # print(parsed_line[0])
            # print("that was it\n")
            if parsed_line[0] == '"widths" ':
                # print("we doing it")
                Result[parsed_line[0].strip()] = parsed_line[1].split(',"')[0].strip()
                # print('parsedline species?')
                # print(parsed_line[2].split(',}')[0])
                Result["species"] = parsed_line[2].split(',}')[0].strip()
            else:
                Result[parsed_line[0].strip()] = parsed_line[1].strip()

        # print(Result)
        return Result


    def ParseTopResult(self):
        try:
            img = Image.open('{}/target.bmp'.format(self.result_dir))
            img.save('{}/target.png'.format(self.report_dir))
        except:
            A = np.genfromtxt('{}/target.csv'.format(self.result_dir),delimiter=',')
            fig = plt.imshow(A, extent=[0, 1, 0, 1], aspect=0.25)
            plt.axis('off')
            fig.axes.get_xaxis().set_visible(False)
            fig.axes.get_yaxis().set_visible(False)
            plt.savefig('{}/target.png'.format(self.report_dir), bbox_inches='tight', pad_inches=0)

        with open(self.output_fname) as f:
            line = f.readline()
            while line:
                if line.startswith('\"global_top\"'):
                    self.top_result = self.ParseResult(f)
                    return
                else:
                    line = f.readline()

    def CreateTopDevice(self):
        # print(self.top_result)
        sequence = ast.literal_eval(self.top_result['"indices"'])
        inlet_design = ast.literal_eval(self.top_result['"widths"'])
        inlet_colors = ast.literal_eval(self.top_result["species"])
        device_name = '{}/{}.dev'.format(self.result_dir, self.job_name)
        device = [sequence, inlet_design, inlet_colors, device_name]
        # mud.print_device(device)
        mud.make_device(device)

    def ParsePerPillarResults(self):
        with open(self.output_fname) as f:
            line = f.readline()
            while line:
                if line.startswith('\"per_pillar\"'):
                    f.readline()
                    for n in range(self.num_pillars):
                        self.per_pillar_results.append(self.ParseResult(f))
                        f.readline()
                        f.readline()
                        f.readline()
                    return
                else:
                    line = f.readline()


    def ParseAllScores(self):
        # Parse stderr for all GA scores
        f = open(self.output_fname)
        line = f.readline()
        while line:
            if line.startswith('Starting'):
                for i in range(self.num_pillars):
                    for j in range(self.ga_reps):
                        line = f.readline().strip('\n')
                        parsed_line = re.split('[ :;?]+', line)
                        self.all_ga_scores[i][j] = float(parsed_line[7])
                        self.all_ga_gen[i][j] = int(parsed_line[11])

                break
            else:
                line = f.readline()
        f.close()


def main():
    parser = GAParser(os.getcwd())
    writer = ReportWriter(parser)


if __name__ == '__main__':
    main()
