import os
import sys
import shutil
import subprocess
import csv as csv
from PIL import Image
import math
import socket
import shutil
try:
    from Queue import Queue
except:
    from queue import Queue

from threading import Thread

CODES = os.environ['CODES']
flowsculpt_dir = '{}/flowsculptga'.format(CODES)
python_dir = '{}/python'.format(flowsculpt_dir)

sys.path.insert(0, python_dir)

from ga_reporter import *
from target_processing import *


min_allowed_fraction = 0.001


class GAJob:
    def __init__(self, job_name, job_dir, flowsculpt, ga_params):
        self.job_name = job_name
        self.job_dir = job_dir
        self.ga_params = ga_params

        if not os.path.exists(self.job_dir):
            os.makedirs(self.job_dir)

        self.detect_build()
        self.set_host()

    def detect_build(self):
        if os.path.isfile('{}/bin/flowsculptga'.format(flowsculpt_dir)):
            self.run_cmd = '{}/bin/flowsculptga {}/ga.config'.format(flowsculpt_dir,
                    self.job_dir)
        elif os.path.isfile('{}/build/flowsculptga'.format(flowsculpt_dir)):
            self.run_cmd = '{}/build/flowsculptga {}/ga.config'.format(flowsculpt_dir,
                    self.job_dir)
        else:
            print('Flowsculpt not detected; exiting.')
            exit()

    def set_host(self):
        hostname = socket.gethostname()
        if hostname.startswith('condo'):
            self.host = 'hpc'
        else:
            self.host = 'local'

    def write_config(self):
        with open('{}/ga.config'.format(self.job_dir), 'w') as f:
            for k, v in self.ga_params.items():
                f.write('{}: {}\n'.format(k, v))
        with open('{}/job.sh'.format(self.job_dir), 'w') as f:
            f.write('#!/bin/bash\n')
            f.write("#SBATCH --time=92:00:00\n")
            f.write('#SBATCH --nodes=1\n')
            f.write('#SBATCH --ntasks-per-node=16\n')
            f.write('#SBATCH --job-name={}\n'.format(self.job_name))
            f.write('#SBATCH --output="std.out"\n')
            f.write('cd $SLURM_SUBMIT_DIR\n')
            f.write('date\n')
            f.write('{}\n'.format(self.run_cmd))


    def set_affinity(self, q):
        num_cores = self.ga_params['numpar']
        cpu_list = range(q*num_cores, (q+1)*num_cores)
        self.cpu_affinity = ', '.join(str(a) for a in cpu_list)

    def run_job(self):
        if self.host == 'local':
            self.run_local()
        elif self.host == 'hpc':
            self.run_hpc()
        else:
            print('No host type set; exiting')
            exit()

    def run_local(self):
        job_stdout = '{}/std.out'.format(self.job_dir)
        print('Running {}'.format(self.job_name))
        with open(job_stdout, 'w') as outputFile:
            subprocess.call(self.run_cmd, stdout=outputFile, stderr=subprocess.STDOUT, shell=True)

    def run_hpc(self):
        print('Running hpc')
        hpc_cmd = 'cd {}; sbatch job.sh; cd ..;'.format(self.job_dir)
        print(hpc_cmd)
        subprocess.call(hpc_cmd, shell=True)

    def parse_job(self):
        print('Parsing job {}'.format(self.job_name))
        self.parser = GAParser(self.job_name, self.job_dir)
        self.writer = ReportWriter(self.parser)
        self.writer.write_report()

    def write_campaign_section(self, f):
        if self.parser is None:
            self.parser = GAParser(self.job_name, self.job_dir)
            self.writer = ReportWriter(self.parser)

        self.writer.write_main_result_text(self.parser,f)


class GACampaign:
    def __init__(self, campaign_params):
        # Set up directories
        self.target_dir = campaign_params['target_dir']
        self.result_dir = campaign_params['result_dir']
        self.num_workers = campaign_params['num_workers']
        self.flowsculptga = campaign_params['flowsculptga']
        self.allow_symmetry = campaign_params['allow_symmetry']
        self.detect_colors = campaign_params['allow_color_detect']

        # Set up run parameters
        self.ga_params = campaign_params['ga_params']
        self.get_targets()

        self.run_queue = Queue()
        self.parse_queue = Queue()

    def get_targets(self):
        self.targets = []
        self.target_fnames = []
        for file in os.listdir(self.target_dir):
            self.targets.append(file)
        self.targets.sort()

    def csv_color_count(self, target):
        '''
        Assuming csv is correctly formatted with integer values for colors
        '''
        readdata = csv.reader(open(target))

        all_data = []
        for row in readdata:
            all_data.append(row)
        all_Data = np.array(all_data)

        unique, counts = np.unique(all_data, return_counts=True)
        all_counts = dict(zip(unique, counts))

        num_colors = len(all_counts) - 1 # Remove background color
        num_inlets = None
        final_color_bank = None

        return [num_colors, num_inlets, final_color_bank]
        
    def image_color_count(self, target):
        img_file = Image.open(target)
        [xs, ys] = img_file.size
        if (xs != self.ga_params['imgw'] or ys != self.ga_params['imgh']):
            print("Image size of {}x{} doesn't match ga_params of {}x{}".format(
                xs, ys, self.ga_params['imgw'], self.ga_params['imgh']))
            print("Resizing.")
            img_file = img_file.resize((self.ga_params['imgw'], self.ga_params['imgh']), Image.NEAREST)
            xs = self.ga_params['imgw']
            ys = self.ga_params['imgh']

        print("new size: {}x{}".format(img_file.size[0], img_file.size[1]))

        total_volume = float(xs * ys)

        color_bank = []
        color_bank_count = []
        for pixel in img_file.getdata():
            if (pixel not in color_bank) and (pixel != (255,255,255)):
                color_bank.append(pixel)
                color_bank_count.append(1)
            else:
                for i, color in enumerate(color_bank):
                    if pixel == color:
                        color_bank_count[i] += 1

        num_colors = 0
        min_fraction = 1.0
        final_color_bank = []
        for i, count in enumerate(color_bank_count):
            volume_fraction = float(count) / total_volume
            if volume_fraction > min_allowed_fraction:
                final_color_bank.append(color_bank[i])
                min_fraction = volume_fraction
                num_colors += 1

        res = Fraction(min_fraction).limit_denominator(1000)
        num_inlets = res.denominator

        for color in final_color_bank:
            print(color)

        return [num_colors, num_inlets, final_color_bank]

    def target_to_csv(self, image_fname, csv_fname, final_color_bank):
        target_ext = os.path.splitext(image_fname)[1]
        if target_ext == '.csv':
            shutil.copyfile(image_fname, csv_fname)
        else:
            img_file = Image.open(image_fname)
            img = img_file.load()

            [xs, ys] = img_file.size
            if (xs != self.ga_params['imgw'] or ys != self.ga_params['imgh']):
                print("Image size of {}x{} STILL doesn't match ga_params of {}x{}".format(
                    xs, ys, self.ga_params['imgw'], self.ga_params['imgh']))
                print("Resizing.")
                img_file = img_file.resize((self.ga_params['imgw'], self.ga_params['imgh']), Image.NEAREST)
                xs = self.ga_params['imgw']
                ys = self.ga_params['imgh']
            
            csv_img = np.zeros((ys, xs))
            print("xs x ys = {}x{}".format(xs, ys))

            for x in range(xs):
                for y in range(ys):
                    pixel = img_file.getpixel((x, y))
                    if (img_file.format == 'BMP') or (target_ext == '.bmp'):
                        closest_color = sorted(final_color_bank, key = lambda color: distance_bmp(color, pixel))[0]
                    elif img_file.format == 'PNG':
                        closest_color = sorted(final_color_bank, key = lambda color: distance_png(color, pixel))[0]
                    else:
                        print('Unrecognized image format')
                    if pixel == (255, 255, 255):
                        csv_img[y][x] = 0
                    else:
                        csv_img[y][x] = final_color_bank.index(closest_color)+1

            np.savetxt(csv_fname, csv_img, delimiter=",", fmt='%d')

    def prepare_jobs_for_campaign(self):
        for target in self.targets:
            job_name = target.split('.')[0]
            target_ext = target.split('.')[1]
            job_dir = '{}/{}'.format(self.result_dir, job_name)

            target_full = '{}/{}'.format(self.target_dir, target)

            ga_params = self.ga_params
            ga_params['result_directory'] = job_dir

            # Analyze image
            imgw = int(self.ga_params['imgw'])
            imgh = int(self.ga_params['imgh'])
            [num_colors, num_inlets, final_color_bank] = target_color_count(target_full, imgw, imgh)
            ga_params['num_species'] = num_colors
            #ga_params['num_inlets'] = num_inlets
            if self.allow_symmetry:
                symmetry = compute_symmetry(target_full, imgw, imgh)
                if symmetry:
                    ga_params['half_pillars'] = 'false'

            if self.detect_colors:
                [num_colors, num_inlets, final_color_bank] = target_color_count(target_full, imgw, imgh)
                ga_params['num_species'] = num_colors
            else:
                final_color_bank = [(0, 0, 0)]
            
            symmetric_jobs = ['Slide04', 'Slide08', 'Slide09', 'Slide11', 'Slide12', 'Slide14']
            if job_name in symmetric_jobs:
                ga_params['half_pillars'] = 'false'
            else:
                ga_params['half_pillars'] = 'true'

            # Set csv target
            self.csv_fname = '{}/target.csv'.format(job_dir)
            ga_params['target'] = self.csv_fname

            job = GAJob(job_name, job_dir, self.flowsculptga, ga_params)
            job.write_config()
            target_to_csv(target_full, self.csv_fname, final_color_bank, imgw, imgh)

            target_tmp_name = '{}/target.{}'.format(job_dir, target_ext)
            shutil.copyfile(target_full, target_tmp_name)

            self.run_queue.put(job)


    def parse_jobs(self):
        self.campaign_report_dir = '{}/Report'.format(self.result_dir)
        if not os.path.isdir(self.campaign_report_dir):
            os.makedirs(self.campaign_report_dir)
        self.campaign_report_fname = '{}/campaign_report.tex'.format(self.campaign_report_dir)
        campaign_report = open(self.campaign_report_fname, 'w')

        for i, target in enumerate(self.targets):
            job_name = target.split('.')[0]
            target_ext = target.split('.')[1]
            job_dir = '{}/{}'.format(self.result_dir, job_name)
            #print("job_dir = {}".format(job_dir))

            target_full = '{}/{}'.format(self.target_dir, target)

            ga_params = self.ga_params
            ga_params['target'] = target_full
            ga_params['result_directory'] = job_dir

            job = GAJob(job_name, job_dir, self.flowsculptga, ga_params)
            job.parse_job()
            if i == 0:
                job.writer.write_preamble(campaign_report)
            job.write_campaign_section(campaign_report)
        job.writer.write_end_text(campaign_report)
        campaign_report.close()
        job.writer.compile_latex(self.campaign_report_dir,self.campaign_report_fname)

    def run_jobs(self):
        self.prepare_jobs_for_campaign()
        def worker(q):
            while True:
                job = self.run_queue.get()
                job.set_affinity(q)
                job.run_job()
                self.run_queue.task_done()
        try:
            for i in range(self.num_workers):
                t = Thread(target=worker, args=(i,))
                t.daemon = True
                t.start()

            self.run_queue.join()
        except:
            while not self.run_queue.empty():
                worker(1)

           


if __name__ == '__main__':
    print('Nothing is going to happen.')
