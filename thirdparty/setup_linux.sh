#!/bin/bash
echo "Helper script for setting up third-party libs for FlowSculptGA"
echo "Note: This will download and build various libraries which could take quite a bit of time and will use a lot of CPU power. If this is NOT OK, press Ctrl+C now. Otherwise, press ENTER."

if [ "$1" != "unattend" ] ; then
    read
fi

PIDS=""

mkdir -p libs
cd libs

for f in ../scripts/linux/*.sh ; do 
    bash $f &
    PIDS="$PIDS $!"
done

echo "Waiting for $PIDS"
wait $PIDS
cd ..