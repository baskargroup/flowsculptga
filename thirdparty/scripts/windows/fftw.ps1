wget ftp://ftp.fftw.org/pub/fftw/fftw-3.3.5-dll64.zip -OutFile fftw.zip

Add-Type -AssemblyName System.IO.Compression.FileSystem
function unzip
{
    param([string]$zipfile, [string]$outpath)

    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}

$wd=[string]$(pwd)

unzip "$wd\fftw.zip" "$wd\fftw"
del fftw.zip

cd fftw
lib /machine:x64 /def:libfftw3f-3.def
lib /machine:x64 /def:libfftw3-3.def