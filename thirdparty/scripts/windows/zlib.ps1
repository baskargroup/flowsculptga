wget https://www.zlib.net/zlib1211.zip -OutFile libz.zip

Add-Type -AssemblyName System.IO.Compression.FileSystem
function unzip
{
    param([string]$zipfile, [string]$outpath)

    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}

$wd=[string]$(pwd)

unzip "$wd\libz.zip" "$wd\libz"
del libz.zip

cd libz
mv */* .
nmake /f win32/Makefile.msc