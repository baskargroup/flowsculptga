FFTW_URL="http://fftw.org/fftw-3.3.6-pl2.tar.gz"
NP=2

if [ $(ls -1 . | grep fftw | wc -l) == 1 ] ; then
    echo "FFTW Installed."
    exit
fi

echo "Downloading FFTW..."
curl -L -o fftw.tar.gz $FFTW_URL > /dev/null 2>&1
echo "Extracting FFTW..."
tf=fftw.tar.gz
tar xf $tf > /dev/null 2>&1
rm $tf 
dir=$(ls -1 . | grep fftw)
cd $dir
echo "Configuring FFTW..."
./configure --prefix=$(pwd) > /dev/null 2>&1
echo "Building FFTW..."
make -j $NP > /dev/null 2>&1
make install > /dev/null 2>&1
echo "FFTW Setup Complete"