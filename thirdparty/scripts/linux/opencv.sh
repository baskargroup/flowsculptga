OCV_URL="https://github.com/opencv/opencv/archive/3.2.0.zip"
NP=$(grep -c ^processor /proc/cpuinfo)

if [ $(ls -1 . | grep opencv | wc -l) == 1 ] ; then
    echo "OpenCV Installed."
    exit
fi

echo "Downloading OpenCV..."
wget $OCV_URL -O opencv.zip > /dev/null 2>&1
echo "Extracting OpenCV..."
tf=opencv.zip
unzip -qq $tf > /dev/null 2>&1
rm $tf
dir=$(ls -1 . | grep opencv)
cd $dir
echo "Configuring OpenCV..."
mkdir build
cd build 
cmake -DCMAKE_INSTALL_PREFIX:PATH=$(pwd)/.. .. > /dev/null 2>&1
echo "Building OpenCV..."
make -j $NP install > /dev/null 2>&1

echo "OpenCV Setup Complete"