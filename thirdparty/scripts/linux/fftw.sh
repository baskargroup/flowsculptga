FFTW_URL="http://fftw.org/fftw-3.3.6-pl2.tar.gz"
NP=$(grep -c ^processor /proc/cpuinfo)

if [ $(ls -1 . | grep fftw | wc -l) == 1 ] ; then
    echo "FFTW Installed."
    exit
fi

echo "Downloading FFTW..."
wget $FFTW_URL > /dev/null 2>&1
echo "Extracting FFTW..."
tf=$(ls -1 . | grep fftw)
tar xf $tf > /dev/null 2>&1
rm $tf 
dir=$(ls -1 . | grep fftw)
cd $dir
echo "Configuring FFTW..."
./configure --prefix=$(pwd) > /dev/null 2>&1
echo "Building FFTW..."
make -j $NP > /dev/null 2>&1
make install > /dev/null 2>&1
echo "FFTW Setup Complete"