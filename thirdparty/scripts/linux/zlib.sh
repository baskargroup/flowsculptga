ZLIB_URL="https://www.zlib.net/zlib-1.2.11.tar.gz"
NP=$(grep -c ^processor /proc/cpuinfo)

if [ $(ls -1 . | grep zlib | wc -l) == 1 ] ; then
    echo "zlib Installed."
    exit
fi

echo "Downloading zlib..."
wget $ZLIB_URL > /dev/null 2>&1
echo "Extracting zlib..."
tf=$(ls -1 . | grep zlib)
tar xf $tf > /dev/null 2>&1
rm $tf 
dir=$(ls -1 . | grep zlib)
cd $dir
echo "Configuring zlib..."
./configure --prefix=$(pwd) > /dev/null 2>&1
echo "Building zlib..."
make -j $NP > /dev/null 2>&1
make install > /dev/null 2>&1
echo "zlib Setup Complete"